<?php

/**
 * 后台用户模型
 */

namespace chb_deploy\server_file\model;

use chb_lib\common\BaseModel;

class ServerFileModel extends BaseModel
{

    protected $name = 'server_file';
    protected $pk = 'server_file_id';

    protected $likeList = ["keyword" => "resources_name"]; //设置模糊搜索映射的字段 alias|value

}
