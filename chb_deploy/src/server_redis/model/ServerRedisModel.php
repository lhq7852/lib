<?php

/**
 * 后台用户模型
 */

namespace chb_deploy\server_redis\model;

use chb_lib\common\BaseModel;

class ServerRedisModel extends BaseModel
{

    protected $name = 'server_redis';
    protected $pk = 'server_redis_id';

    protected $likeList = ["keyword" => "resources_name"]; //设置模糊搜索映射的字段 alias|value

}
