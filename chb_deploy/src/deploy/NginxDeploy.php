<?php

/**
 * nginx环境部署
 */
namespace chb_deploy\deploy;

class NginxDeploy extends CodeDeploy
{

    public $nginxDir = "/www/server/panel/vhost/nginx"; //nginx配置地址

    public $nginxTemp = "nginx/nginx.conf"; //nginx 原型

    public $bt_token = "uwHlsiS14QhZ060UJ4h0UEdkXdqSySRJ";

    /**
     * 开始安装nginx
     *
     * @return void
     */
    public function startInstall()
    {
        $file = root_path() . $this->nginxTemp;
        $nginxString = file_get_contents($file);
        $listen = "listen 80;"; //监听端口
        $server_name = ""; //网站域名
        print_r($nginxString);exit;
        return true;
    }

    /**
     * 重启nginx
     *
     * @param [type] $dir
     * @param [type] $packger
     * @return void
     */
    private function restartNginx($account, $host)
    {
        $code = 'ssh %s@%s "service nginx restart"';
        $code = sprintf($code, $account, $host);
        echo $code . PHP_EOL;
        return shell_exec($code);
    }

    /**
     * 推送nginx配置到远程服务器
     *
     * @param [type] $dir
     * @return void
     */
    private function pushNginx($account, $host, $nginxDir, $nginxFile)
    {
        //scp /www/deploy/test/website/system.caihubang.com/think root@47.106.115.99:/www/server/panel/vhost/nginx
        $code = 'scp %s %s@%s:%s';
        $code = sprintf($code, $nginxFile, $account, $host, $nginxDir);
        echo $code . PHP_EOL;
        return shell_exec($code);
    }

}
