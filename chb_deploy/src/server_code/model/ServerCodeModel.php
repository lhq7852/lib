<?php

/**
 * 后台用户模型
 */

namespace chb_deploy\server_code\model;

use chb_lib\common\BaseModel;

class ServerCodeModel extends BaseModel
{

    protected $name = 'server_code`';
    protected $pk = 'server_code_id';

    protected $likeList = ["keyword" => "server_name"]; //设置模糊搜索映射的字段 alias|value

}
