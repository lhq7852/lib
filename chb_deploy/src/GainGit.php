<?php

namespace chb_deploy;

class GainGit
{

    public $checkout = "/mnt/www/deploy/test"; // 宿主机检出目录
    public $git = "git@gitee.com:swiftchina/chb-platform.git"; //代码仓库

    public $account = "www"; //目标服务器账号
    public $host = "47.106.115.99"; // 目标服务器              47.106.115.99     39.108.247.113

    public $deploy = "/www/deploy/test"; // 目标服务器 部署位置  /www/deploy/test  /date/deploy/test
    public $website = "website"; // 目标服务器 站点目录 软连接
    public $version = "releases"; //版本目录  代码真实存放位置

    public $packger = "system.caihubang.com"; //包名
    public $packger_suffix = ".tar.gz"; //包后缀
    public $packger_url = ''; //打包后的完整包 物理地址

    public $nginx_url = "/www/server/panel/vhost/nginx"; //nginx配置地址

    public function getGitCode()
    {
        echo 'code';
    }

    /**
     * 推送安装
     *
     * @return bool
     */
    public function pushInstall()
    {
        $this->checkout .= "/" . $this->packger; //宿主检出目录
        $this->website = $this->deploy . "/" . $this->website; //站点目录
        $this->version = $this->deploy . "/" . $this->version . "/" . date("YmdHis"); //版本地址
        $this->batchMkdir($this->checkout);
        $arrDir = scandir($this->checkout);
        //拉git代码
        if (count($arrDir) <= 2) {
            $this->gainGit($this->checkout, $this->git, "master");
        } else {
            $this->pullGit($this->checkout);
        }
        //打包
        $this->createTargz($this->checkout, $this->packger . $this->packger_suffix);
        //创建远程文件夹
        $this->longRangeCheckDir($this->account, $this->host, $this->deploy);
        $this->longRangeCheckDir($this->account, $this->host, $this->website); //站点地址创建站点地址
        $this->longRangeCheckDir($this->account, $this->host, $this->version); //源文件版本地址
        //推送代码
        $this->pushProject($this->account, $this->host, $this->deploy, $this->packger_url);
        //删除本地压缩包
        $this->deleteTargz($this->checkout, $this->packger . $this->packger_suffix);
        //解压远程包&删除
        $this->openTargz($this->account, $this->host, $this->deploy, $this->packger . $this->packger_suffix, $this->version);
        $this->createLn($this->account, $this->host, $this->version . '/' . $this->packger, $this->website);
        //nginx 配置

        return true;
    }

    /**
     * 创建软连接
     *
     * @param [type] $realDir 真实文件地址[源文件地址]
     * @param [type] $lnDir  软连接地址
     * @return void
     */
    private function createLn($account, $host, $realDir, $lnDir)
    {
        $code = 'ssh %s@%s "ln -snf %s %s"';
        $code = sprintf($code, $account, $host, $realDir, $lnDir);
        echo $code . PHP_EOL;
        return shell_exec($code);
    }

    /**
     * 创建压缩包
     *
     * @param [type] $dir
     * @param string $name
     * @return void
     */
    private function createTargz($dir, $name)
    {
        $arrDir = explode("/", $dir);
        $packerDir = array_pop($arrDir);
        $dir = implode("/", $arrDir);
        $this->packger_url = $dir . '/' . $name;
        $code = "cd %s && tar -zcvf %s --exclude=.git %s";
        $code = sprintf($code, $dir, $name, $packerDir);
        echo $code . PHP_EOL;
        return shell_exec($code);
    }

    /**
     * 删除压缩包
     *
     * @param [type] $dir
     * @param [type] $packger
     * @return void
     */
    private function deleteTargz($dir, $packger)
    {
        $arrDir = explode("/", $dir);
        $packerDir = array_pop($arrDir);
        $dir = implode("/", $arrDir);

        $code = "cd %s && rm -rf %s";
        $code = sprintf($code, $dir, trim($packger));
        echo $code . PHP_EOL;
        // return shell_exec($code);
    }

    /**
     * 解压压缩包 & 删除压缩包
     *
     * @param [type] $dir
     * @param [type] $packger
     * @return void
     */
    private function openTargz($account, $host, $dir, $packger, $version)
    {
        // $code = 'ssh %s@%s "cd %s && tar -xzvf %s && rm -r %s"';
        $code = 'ssh %s@%s "cd %s && tar -xzvf %s -C %s"';
        $code = sprintf($code, $account, $host, $dir, $packger, $version);
        echo $code . PHP_EOL;
        return shell_exec($code);
    }

    /**
     * 拉起仓库代码
     *
     * @return void
     */
    private function gainGit($dir, $git, $master = 'master')
    {
        $code = "cd %s && git clone -b %s %s %s";
        $code = sprintf($code, $dir, $master, $git, $dir);
        echo $code . PHP_EOL;
        return shell_exec($code);
    }

    /**
     * 更新仓库代码
     *
     * @return void
     */
    private function pullGit($dir)
    {
        $code = "cd %s && git pull";
        $code = sprintf($code, $dir);
        echo $code . PHP_EOL;
        return shell_exec($code);
    }

    /**
     * 推送项目到远程
     *
     * @param [type] $dir
     * @return void
     */
    private function pushProject($account, $host, $dir, $packger)
    {
        $code = 'scp %s %s@%s:%s';
        $code = sprintf($code, $packger, $account, $host, $dir);
        echo $code . PHP_EOL;
        return shell_exec($code);
    }

    /**
     * 远程创建目录
     *
     * @param [type] $account
     * @param [type] $host
     * @param [type] $dir
     * @return void
     */
    private function longRangeCheckDir($account, $host, $dir)
    {
        $is_dir = <<<DATA
# /bin/bash
if [ -d "{$dir}" ];then
echo yes
else
echo no
fi
DATA
        ;
        $check = shell_exec($is_dir);
        if (trim($check) == 'yes') {
            return true;
        }
        $code = 'ssh %s@%s "mkdir -p -m=775 %s"';
        $code = sprintf($code, $account, $host, $dir);
        echo $code . PHP_EOL;
        return shell_exec($code);
    }

    /**
     * 创建目录
     *
     * @param [type] $dir
     * @return bool
     */
    private function batchMkdir($dir)
    {
        if (is_dir($dir)) {
            return true;
        }
        $arrExplode = explode("/", $this->checkout);
        $str = "/";
        foreach ($arrExplode as $v) {
            if (empty($v)) {
                continue;
            }
            $str .= $v . "/";
            if (is_dir($str)) {
                continue;
            }
            mkdir($str, 0777, true);
        }
        return true;
    }
}
