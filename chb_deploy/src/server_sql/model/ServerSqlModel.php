<?php

/**
 * 后台用户模型
 */

namespace chb_deploy\server_sql\model;

use chb_lib\common\BaseModel;

class ServerSqlModel extends BaseModel
{

    protected $name = 'server_sql';
    protected $pk = 'server_sql_id';

    protected $likeList = ["keyword" => "resources_name"]; //设置模糊搜索映射的字段 alias|value

}
