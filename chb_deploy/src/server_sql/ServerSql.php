<?php

namespace chb_deploy\server_sql;

use chb_lib\common\ServiceTrait;
use chb_lib\common\RedisToolTrait;

class ServerSql
{

    use RedisToolTrait, ServiceTrait;


    /**
     * 格式化详情
     *
     * @param [type] $info
     * @return array
     */
    public function formatInfo(&$info)
    {
        if (!empty($info['config'])) {
            $info['config'] = json_decode($info['config'], true);
            foreach ($info['config'] as $k => $v) {
                $info['config_' . $k] = $v;
            }
        }
        return $info;
    }

    /**
     * 设置参数
     *
     * @param [type] $param
     * @return object
     */
    protected function setFieldParam(&$param, $mode = "add")
    {
        foreach ($param as $k => $v) {
            if (strchr(strtolower($k), "config_")) {
                $arr = explode("_", $k);
                if (count($arr) != 2) {
                    continue;
                }
                $param['config'][$arr[1]] = $v;
            }
        }
        if (!empty($param['config'])) {
            $param['config'] = json_encode($param['config']);
        }
        return $this;
    }
}
