<?php

/**
 * 后台用户模型
 */

namespace chb_deploy\website\model;

use chb_lib\common\BaseModel;

class WebsiteModel extends BaseModel
{

    protected $name = 'website';
    protected $pk = 'website_id';

    protected $likeList = ["keyword" => "server_name"]; //设置模糊搜索映射的字段 alias|value

}
