<?php

namespace chb_deploy\website;

use chb_deploy\deploy\CodeDeploy;
use chb_lib\common\RedisToolTrait;
use chb_lib\common\ServiceTrait;
use chb_rpc\external_api\BtRpc;

class Website
{

    use RedisToolTrait, ServiceTrait;

    public $nginxTemp = "nginx/nginx.conf"; //nginx 原型

    /**
     * 设置入库的参数,可重写
     *
     * @param [type] $param
     * @return object
     */
    protected function setFieldParam(&$param = [], $mode = "add")
    {
        if (empty($param['app_id']) && $mode == 'add') {
            $param['app_id'] = "open" . $this->getNonceStr();
        }
        return $this;
    }

    /**
     * 新增站点监听
     *
     * @param array $param
     * @return void
     */
    protected function listenAdd()
    {
        $ret = BtRpc::_addWebsite([
            "webname" => $this->dataInfo['server_name'] ?? '',
            "path" => $this->dataInfo['path'] ?? '',
            "port" => '80',
            "ps" => $this->dataInfo['ps'] ?? '',
            'bt_token' => request()->param('bt_token', ''),
        ]);
        if (!empty($ret) && !empty($ret['siteId']) && $ret['siteStatus'] == 1) {
            $this->getModel()->editData(['website_id' => $this->dataInfo['website_id'], 'bt_id' => $ret['siteId'], 'state' => 1]);
        }
        return true;
    }

    /**
     * 更新站点配置
     *
     * @return void
     */
    public function updateWebsiteConfig($param = [])
    {
        $file = root_path() . $this->nginxTemp;
        $nginxString = file_get_contents($file);
        $info = $this->getInfo(['bt_id' => $param['bt_id']]);
        $client_pk = $info['client_pk'] == 'open' ? 'open' : $info['client_pk'];
        $listen = $info['port'] == '80' ? '' : "listen {$info['port']};";
        $nginxString = $this->content_replace($nginxString, [
            'server_name' => $info['server_name'],
            "root_dir" => $info['path'],
            "listen" => $listen,
            "website_type_config" => <<<WEB
location /{$client_pk}/ {
        proxy_pass http://127.0.0.1:{$info['port']}/{$client_pk}/;
    }
WEB
            ,
        ],"#{{","}}");
        //打包
        $file = (new CodeDeploy)->packProject();
        //上传
        $ret = BtRpc::_uploadFile([
            'file' => $file,
            'f_path' => "/mnt/www/deploy",
        ]);
        //解压
        $ret = BtRpc::_unzip([
            'sfile' => "/mnt/www/deploy/system.caihubang.com.tar.gz",
            'dfile' => "/mnt/www/deploy",
        ]);
        sleep(3);
        //删除压缩文件
        $ret = BtRpc::_deleteFile([
            'path' => "/mnt/www/deploy/system.caihubang.com.tar.gz",
        ]);
        //保存网站nginx配置
        $ret = BtRpc::_saveNginxFileBody([
            'data' => $nginxString,
            "path" => "/www/server/panel/vhost/nginx/" . $info['server_name'] . ".conf",
        ]);
        //保存网站根目录
        // $ret = BtRpc::_setWebsitePath([
        //     'path' => "/mnt/www/deploy/system.caihubang.com/public",
        //     'id' => $param['bt_id'],
        // ]);
        print_r($file);
        echo PHP_EOL;
        print_R($ret);
        exit;
    }

}
