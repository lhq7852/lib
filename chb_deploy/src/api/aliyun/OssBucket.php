<?php

/**
 * OSS存储空间管理
 */

namespace chb_resources\api\aliyun;

use OSS\OssClient;

class OssBucket
{
    public $accessKeyId = '';
    public $accessKeySecret = '';

    public function __construct(AliyunConfig $objConfig)
    {
        $this->accessKeyId = $objConfig->accessKeyId;
        $this->accessKeySecret = $objConfig->accessKeySecret;
    }

    /**
     * 创建存储空间
     *
     * @return bool
     */
    public function createBucket()
    {
        // Endpoint以杭州为例，其它Region请按实际情况填写。
        $endpoint = "http://oss-cn-hangzhou.aliyuncs.com";
        // 存储空间名称。
        $bucket = "<yourBucketName>";
        try {
            $ossClient = new OssClient($this->accessKeyId, $this->accessKeySecret, $endpoint);
            // 设置存储空间的存储类型为低频访问类型，默认是标准类型。
            $options = array(
                OssClient::OSS_STORAGE => OssClient::OSS_STORAGE_IA
            );
            // 设置存储空间的权限为公共读，默认是私有读写。
            $ossClient->createBucket($bucket, OssClient::OSS_ACL_TYPE_PUBLIC_READ, $options);
        } catch (\Exception $e) {
            printf(__FUNCTION__ . ": FAILED\n");
            printf($e->getMessage() . "\n");
            return;
        }
    }

    /**
     * 存储空间列表
     *
     * @return array
     */
    public function listBuckets()
    {
        // Endpoint以杭州为例，其它Region请按实际情况填写。
        $endpoint = "http://oss-cn-hangzhou.aliyuncs.com";
        try {
            $ossClient = new OssClient($this->accessKeyId, $this->accessKeySecret, $endpoint);
            $bucketListInfo = $ossClient->listBuckets();
            $bucketList = $bucketListInfo->getBucketList();
            print_r($bucketList);exit;
            foreach ($bucketList as $bucket) {
                print($bucket->getLocation() . "\t" . $bucket->getName() . "\t" . $bucket->getCreatedate() . "\n");
            }
        } catch (\Exception $e) {
            printf(__FUNCTION__ . ": FAILED\n");
            printf($e->getMessage() . "\n");
            return;
        }
    }
}
