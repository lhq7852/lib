<?php

/**
 * 域名解析管理
 */

namespace chb_resources\api\aliyun;

use AlibabaCloud\Alidns\Alidns;
use AlibabaCloud\Client\AlibabaCloud;

class DomainRecord
{
    public $accessKeyId = '';
    public $accessKeySecret = '';

    public function __construct(AliyunConfig $objConfig)
    {
        $this->accessKeyId = $objConfig->accessKeyId;
        $this->accessKeySecret = $objConfig->accessKeySecret;
    }

    /**
     * 查看解析记录
     *
     * @return array
     */
    public function DescribeDomainRecords()
    {
        AlibabaCloud::accessKeyClient($this->accessKeyId, $this->accessKeySecret)
            ->regionId('cn-shenzhen')
            ->asDefaultClient();

        try {
            $request = Alidns::v20150109()->describeDomainRecords();
            $result = $request
                ->withDomainName("6ms.cn")
                ->connectTimeout(1) // Throw an exception when Connection timeout
                ->timeout(1) // Throw an exception when timeout 
                ->request();

            print_r($result->toArray());
            exit;
        } catch (\Exception $exception) {
            echo $exception->getMessage() . PHP_EOL;
        }
    }

    /**
     * 添加解析
     *
     * @return bool
     */
    public function addDomainRecord()
    {
        $a = AlibabaCloud::accessKeyClient($this->accessKeyId, $this->accessKeySecret)
            ->regionId('cn-shenzhen')
            ->asDefaultClient();
        try {
            $request = Alidns::v20150109()->addDomainRecord();
            $result = $request
                ->withDomainName("6ms.cn")
                ->withType("A")
                ->withRR("lstest")
                ->withValue("47.106.115.99")
                ->connectTimeout(1) // Throw an exception when Connection timeout
                ->timeout(1) // Throw an exception when timeout 
                ->request();
            print_r($result->toArray());
        } catch (\Exception $exception) {
            echo $exception->getMessage() . PHP_EOL;
        }
    }


    /**
     * 修改解析
     *
     * @return bool
     */
    public function UpdateDomainRecord()
    {
        AlibabaCloud::accessKeyClient($this->accessKeyId, $this->accessKeySecret)
            ->regionId('cn-shenzhen')
            ->asDefaultClient();
        try {
            $request = Alidns::v20150109()->UpdateDomainRecord();
            $result = $request
                ->withType("A")
                ->withRR("lsstest")
                ->withValue("47.106.115.99")
                ->withRecordId("727926426927303680")
                ->connectTimeout(1) // Throw an exception when Connection timeout
                ->timeout(1) // Throw an exception when timeout 
                ->request();
            print_r($result->toArray());
        } catch (\Exception $exception) {
            echo $exception->getMessage() . PHP_EOL;
        }
    }

    /**
     * 删除解析
     *
     * @return bool
     */
    public function DeleteDomainRecord()
    {
        AlibabaCloud::accessKeyClient($this->accessKeyId, $this->accessKeySecret)
            ->regionId('cn-shenzhen')
            ->asDefaultClient();
        try {
            $request = Alidns::v20150109()->DeleteDomainRecord();
            $result = $request
                ->withRecordId("727926426927303680")
                ->connectTimeout(1) // Throw an exception when Connection timeout
                ->timeout(1) // Throw an exception when timeout 
                ->request();
            print_r($result->toArray());
        } catch (\Exception $exception) {
            echo $exception->getMessage() . PHP_EOL;
        }
    }
}
