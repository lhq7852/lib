<?php

/**
 * 阿里云配置
 */

namespace chb_resources\api\aliyun;

use chb_lib\common\BaseSystem;

class AliyunConfig
{
    public $accessKeyId = '';
    public $accessKeySecret = '';

    public function __construct()
    {
        $access = (BaseSystem::_getObject("SystemConfig"))::_getKeyConfig("aliyun_6ms");
        $this->accessKeyId = $access['accessKeyId'];
        $this->accessKeySecret = $access['accessKeySecret'];
    }
}
