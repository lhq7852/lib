<?php

/**
 * 数据库实例
 */

namespace chb_resources\api\aliyun;

use AlibabaCloud\Alidns\Alidns;
use AlibabaCloud\Client\AlibabaCloud;

class DBInstances
{
    public $accessKeyId = '';
    public $accessKeySecret = '';

    public function __construct(AliyunConfig $objConfig)
    {
        $this->accessKeyId = $objConfig->accessKeyId;
        $this->accessKeySecret = $objConfig->accessKeySecret;
    }

    /**
     * 查看实例记录
     *
     * @return array
     */
    public function DescribeDBInstances()
    {
        AlibabaCloud::accessKeyClient($this->accessKeyId, $this->accessKeySecret)
            ->regionId('cn-shenzhen')
            ->asDefaultClient();

        try {
            $request = Alidns::v20150109()->DescribeDBInstances();
            $result = $request
                ->connectTimeout(1) // Throw an exception when Connection timeout
                ->timeout(1) // Throw an exception when timeout 
                ->request();

            print_r($result->toArray());
            exit;
        } catch (\Exception $exception) {
            echo $exception->getMessage() . PHP_EOL;
        }
    }
}
