<?php

namespace chb_message\template;

use chb_lib\common\RedisToolTrait;
use chb_lib\common\ServiceTrait;
use chb_message\template\model\TemplateGroupRelationModel;

class TemplateGroup
{

    use RedisToolTrait, ServiceTrait;

    protected $relationModel;
    public $type = [
        'sms_template_id',
        'mail_template_id',
        'wechat_template_id',
        'email_template_id',
    ];

    /**
     * 格式化
     *
     * @param [type] $info
     * @return array
     */
    public function formatInfo(&$info)
    {
        $info['relation'] = $this->relationModel()->getDataList(['group_id' => $info['group_id']], true);
        return $info;
    }

    /**
     * 关联模型
     *
     * @return object
     */
    public function relationModel()
    {
        if (!empty($this->relationModel)) {
            return $this->relationModel;
        }
        $this->relationModel = new TemplateGroupRelationModel();
        return $this->relationModel;
    }

    /**
     * 监听添加
     *
     * @return void|bool
     */
    public function listenAdd()
    {
        if (empty($this->dataInfo['group_id'])) {
            return true;
        }
        $data = [];
        foreach ($this->type as $type) {
            if (!empty($this->param[$type])) {
                $data[] = [
                    'group_id' => $this->dataInfo['group_id'],
                    'template_id' => $this->param[$type],
                    'template_type' => $type,
                ];
            }
        }
        if (!empty($data)) {
            $this->relationModel()->addData($data);
        }
        return true;
    }

    /**
     * 设置必须更新
     *
     * @return object
     */
    protected function setIsEdit()
    {
        $this->isEdit = true;
        return $this;
    }

    /**
     * 监听编辑
     *
     * @return void|bool
     */
    public function listenEdit()
    {
        if (empty($this->dataInfo['group_id'])) {
            return true;
        }
        $data = [];
        foreach ($this->type as $type) {
            if (!empty($this->param[$type])) {
                $data[] = [
                    'group_id' => $this->dataInfo['group_id'],
                    'template_id' => $this->param[$type],
                    'template_type' => $type,
                ];
            }
        }
        $this->relationModel()->deleteData(['group_id' => $this->dataInfo['group_id']]);
        if (!empty($data)) {
            $this->relationModel()->addData($data);
        }
        return true;
    }
}
