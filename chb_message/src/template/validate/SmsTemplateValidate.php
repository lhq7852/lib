<?php

/**
 * 验证
 */

namespace chb_message\template\validate;

use chb_lib\common\ValidateTrait;
use think\Validate;

class SmsTemplateValidate extends Validate
{
    use ValidateTrait;

    protected $rule = [
        'template_name' => 'require|max:30',
        'type' => 'require|between:0,3',
        "content" => 'require|max:500',
        "remark" => 'require|max:500',
    ];
}
