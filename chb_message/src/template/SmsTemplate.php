<?php

namespace chb_message\template;

use chb_lib\common\exception\Err;
use chb_lib\common\RedisToolTrait;
use chb_lib\common\ServiceTrait;
use chb_rpc\external_api\SmsRpc;
use think\facade\Queue;

class SmsTemplate
{

    use RedisToolTrait, ServiceTrait;

    /**
     * 设置入库的参数,可重写
     *
     * @param [type] $param
     * @return object
     */
    protected function setFieldParam(&$param = [], $mode = "add")
    {
        if ($mode == 'edit') {
            $info = $this->getModel()->getInfo(['sms_template_id' => $param['sms_template_id']], false);
            if (!empty($info) && $info['state'] == 0) {
                Err::errorMsg(871);
            }
            if (!empty($info) && $info['state'] == 1) {
                Err::errorMsg(873);
            }
            $param['submit_state'] = 0;
        } else {
            $param['state'] = 0;
        }
        return $this;
    }

    /**
     * 检查数据权限
     *
     * @return object
     */
    public function checkDataLevel($param = [])
    {
        $info = $this->getModel()->getInfo(['sms_template_id' => $param['sms_template_id']], false);
        if (!empty($info) && $info['submit_state'] == 1) { //审核中,无法删除
            Err::errorMsg(872);
        }
        return $this;
    }

    /**
     * 监听添加
     */
    public function listenAdd()
    {
        Queue::push("chb_lib\common\job\SmsTemplateJob@addSmsTemplate", $this->dataInfo, 'smsTemplate:add_queue');
    }

    /**
     * 监听删除
     */
    public function listenDel()
    {
        Queue::push("chb_lib\common\job\SmsTemplateJob@deleteSmsTemplate", $this->dataInfo, 'smsTemplate:delete_queue');
    }

    /**
     * 监听编辑
     */
    public function listenEdit()
    {
        Queue::push("chb_lib\common\job\SmsTemplateJob@editSmsTemplate", $this->dataInfo, 'smsTemplate:edit_queue');
    }

    /**
     * 处理队列
     */
    public function handleAddQueue($param = [])
    {
        $info = $this->getModel()->getInfo(['sms_template_id' => $param['sms_template_id']], false);
        if (empty($info) || !empty($info['template_code'])) {
            return true;
        }
        $ret = SmsRpc::_addSmsTemplate([
            "templateType" => $param['type'] ?? '',
            "templateName" => $param['template_name'] ?? '',
            "templateContent" => $param['content'] ?? '',
            "remark" => $param['remark'] ?? '',
        ]);
        $this->getModel()->editData(['sms_template_id' => $param['sms_template_id'], 'submit_state' => 1, 'template_code' => $ret['templateCode']]);
        return true;
    }

    /**
     * 处理队列
     */
    public function handleDeleteQueue($param = [])
    {
        $info = $this->getModel()->setIsDeleteTime()->getInfo(['sms_template_id' => $param['sms_template_id']], false);
        if (empty($info) || (!empty($info) && $info['state'] != 1)) {
            return true;
        }
        $ret = SmsRpc::_deleteSmsTemplate(['templateCode' => $info['template_code']]);
        if (empty($info['delete_time'])) {
            $this->getModel()->deleteData(['sms_template_id' => $param['sms_template_id']]);
        }
        return true;
    }

    /**
     * 处理队列
     */
    public function handleEditQueue($param = [])
    {
        $info = $this->getModel()->getInfo(['sms_template_id' => $param['sms_template_id']], false);
        if (empty($info) || (!empty($info) && $info['state'] != 2)) {
            return true;
        }
        $info = SmsRpc::_querySmsTemplate(['templateCode' => $info['template_code']]);
        if (!empty($info['code']) && $info['code'] == 'OK') {
            if ($info['templateStatus'] == 0) {
                return true;
            }
            if ($info['templateStatus'] == 1) {
                return true;
            }
        }
        SmsRpc::_modifySmsTemplate([
            "templateType" => $param['type'] ?? '',
            "templateName" => $param['template_name'] ?? '',
            "templateContent" => $param['content'] ?? '',
            "remark" => $param['remark'] ?? '',
            "templateCode" => $param['template_code'] ?? '',
        ]);
        $this->getModel()->editData(['sms_template_id' => $param['sms_template_id'], 'submit_state' => 1, 'state' => 0]);
        return true;
    }

    /**
     * 定时任务拉取审核结果
     */
    public function timeGetResult()
    {
        $list = $this->getDataList(['submit_state' => 1]);
        if (empty($list)) {
            $this->repeatAddQueue();
            return true;
        }
        foreach ($list as $v) {
            echo '查看状态:' . $v['template_code'] . PHP_EOL;
            $info = SmsRpc::_querySmsTemplate(['templateCode' => $v['template_code']]);
            if (!empty($info['code']) && $info['code'] == 'OK' && $info['templateStatus'] > 0) {
                $this->getModel()->editData([
                    'sms_template_id' => $v['sms_template_id'],
                    'state' => $info['templateStatus'],
                    'reason' => $info['reason'],
                    'submit_state' => 2,
                ]);
            }
        }
        $this->repeatAddQueue();
        return true;
    }

    /**
     * 没有成功加入队列的,重复加入队列处理
     */
    protected function repeatAddQueue()
    {
        $list = $this->getDataList(['submit_state' => 0]);
        if (empty($list)) {
            return true;
        }
        foreach ($list as $v) {
            if ($v['submit_state'] == 0 && empty($v['template_code'])) { //重新加入添加队列
                echo "重新加入添加队列" . $v['template_code'] . PHP_EOL;
                Queue::push("chb_lib\common\job\SmsTemplateJob@addSmsTemplate", $v, 'smsTemplate:add_queue');
                $this->getModel()->editData(['sms_template_id' => $v['sms_template_id'], 'submit_state' => 1]);
            }
            if ($v['submit_state'] == 0 && !empty($v['template_code'])) { //审核失败,重新加入编辑队列
                echo "重新加入编辑队列" . $v['template_code'] . PHP_EOL;
                Queue::push("chb_lib\common\job\SmsTemplateJob@editSmsTemplate", $v, 'smsTemplate:edit_queue');
                $this->getModel()->editData(['sms_template_id' => $v['sms_template_id'], 'submit_state' => 1, 'state' => 0]);
            }
        }
        return true;
    }
}
