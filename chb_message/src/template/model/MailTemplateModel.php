<?php

/**
 * 模型
 */

namespace chb_message\template\model;

use chb_lib\common\BaseModel;

class MailTemplateModel extends BaseModel
{

    protected $name = 'template_mail';
    protected $pk = 'mail_template_id';

    protected $hidden = ['delete_time'];
    protected $likeList = ["keyword" => "template_name|template_alias"]; //设置模糊搜索映射的字段 alias|value

}
