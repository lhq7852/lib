<?php

/**
 * 模型
 */

namespace chb_message\template\model;

use chb_lib\common\BaseModel;

class TemplateGroupRelationModel extends BaseModel
{

    protected $name = 'template_group_relation';
    protected $pk = 'group_template_relation_id';

    protected $hidden = ['create_user_no', 'create_time', 'group_template_relation_id'];

}
