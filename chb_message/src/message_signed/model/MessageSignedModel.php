<?php

/**
 * 模型
 */

namespace chb_message\message_signed\model;

use chb_lib\common\BaseModel;

class MessageSignedModel extends BaseModel
{

    protected $name = 'message_signed';
    protected $pk = 'sms_signed_id';

    protected $likeList = ["keyword" => "签名"]; //设置模糊搜索映射的字段 alias|value

    /**
     * 字段-1
     */
    public static function field_1()
    {
        return "a.sms_signed_id, a.organization_no, a.signed, a.business_ype, a.sign_source, a.remark, a.state, a.reason, a.create_time";
    }
}
