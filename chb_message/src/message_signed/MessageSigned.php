<?php

namespace chb_message\message_signed;

use chb_lib\common\exception\Err;
use chb_lib\common\RedisToolTrait;
use chb_lib\common\ServiceTrait;
use chb_message\message_signed\model\MessageSignedModel;
use chb_rpc\external_api\SmsRpc;
use think\facade\Queue;

class MessageSigned
{

    use RedisToolTrait, ServiceTrait;

    /**
     * 设置入库的参数,可重写
     *
     * @param [type] $param
     * @return object
     */
    protected function setFieldParam(&$param = [], $mode = "add")
    {
        if ($mode == 'edit') {
            $info = SmsRpc::_querySmsSign(['signName' => $this->oldDataInfo['signed']]);
            if (!empty($info['code']) && $info['code'] == 'OK') {
                if ($info['signStatus'] == 0) {
                    Err::errorMsg(870);
                }
                if ($info['signStatus'] == 1) {
                    Err::errorMsg(871);
                }
            }
            $param['submit_state'] = 0;
        } else {
            $param['state'] = 0;
        }
        return $this;
    }

    /**
     * 自定义过滤字段
     *
     * @param array $param
     * @param [type] $method
     * @return object
     */
    protected function setFilterWhere(&$param = [], $method = null)
    {
        if (strchr(strtolower($method), "list")) {
            $param['field'] = MessageSignedModel::field_1();
        }
        return $this;
    }

    /**
     * 格式化
     *
     * @param [type] $info
     * @return array
     */
    public function formatInfo(&$info)
    {
        if (!empty($info['certificate'])) {
            $info['certificate'] = json_decode($info['certificate'], true);
        }
        return $info;
    }

    /**
     * 监听添加
     */
    public function listenAdd()
    {
        Queue::push("chb_lib\common\job\MessageSignedJob@addSmsSign", $this->dataInfo, 'smsSign:add_queue');
    }

    /**
     * 监听删除
     */
    public function listenDel()
    {
        //审核中不允许删除
        $info = SmsRpc::_querySmsSign(['signName' => $this->dataInfo['signed']]);
        if (!empty($info['code']) && $info['code'] == 'OK') {
            if ($info['signStatus'] == 0) {
                Err::errorMsg(872);
            }
        }
        Queue::push("chb_lib\common\job\MessageSignedJob@deleteSmsSign", $this->dataInfo, 'smsSign:delete_queue');
    }

    /**
     * 监听编辑
     */
    public function listenEdit()
    {
        Queue::push("chb_lib\common\job\MessageSignedJob@editSmsSign", $this->dataInfo, 'smsSign:edit_queue');
    }

    /**
     * 添加-队列处理
     */
    public function handleAddQueue($param = [])
    {
        $info = $this->getModel()->getInfo(['sms_signed_id' => $param['sms_signed_id']], false);
        if (empty($info) || !empty($info['submit_state'])) {
            return true;
        }
        //查询该签名是否已存在,防止重复提交
        $info = SmsRpc::_querySmsSign(['signName' => $param['signed']]);
        if (!empty($info['code']) && $info['code'] == 'OK' && $info['signStatus'] > 0) {
            $this->getModel()->editData([
                'sms_signed_id' => $param['sms_signed_id'],
                'state' => $info['signStatus'],
                'reason' => $info['reason'],
                'submit_state' => 2,
            ]);
            return true;
        }

        $image[] = [
            'fileContents' => $this->getImageBase64($param['authorization']),
            'fileSuffix' => $this->getImageType($param['authorization']),
        ];
        $arrCertificate = is_array($param['certificate']) ? $param['certificate'] : json_decode($param['certificate'], true);

        foreach ($arrCertificate as $v) {
            if (!empty($v['url'])) {
                $image[] = [
                    'fileContents' => $this->getImageBase64($v['url']),
                    'fileSuffix' => $this->getImageType($v['url']),
                ];
            }
        }
        $data = [
            'signName' => $param['signed'],
            'signSource' => $param['sign_source'],
            'remark' => $param['remark'],
            'signFileList' => $image,
        ];
        SmsRpc::_addSmsSign($data);
        $this->getModel()->editData(['sms_signed_id' => $param['sms_signed_id'], 'submit_state' => 1]);
        return true;
    }

    /**
     * 删除-队列处理
     */
    public function handleDeleteQueue($param = [])
    {
        $info = $this->getModel()->setIsDeleteTime()->getInfo(['sms_signed_id' => $param['sms_signed_id']], false);
        if (empty($info)) {
            return true;
        }
        //审核中不允许删除
        $info = SmsRpc::_querySmsSign(['signName' => $info['signed']]);
        if (!empty($info['code']) && $info['code'] == 'OK') {
            if ($info['signStatus'] == 0) {
                return true;
            }
        }
        $ret = SmsRpc::_deleteSmsSign(['signName' => $info['signed']]);
        if (empty($info['delete_time'])) {
            $this->getModel()->deleteData(['sms_signed_id' => $param['sms_signed_id']]);
        }
        return true;
    }

    /**
     * 编辑-队列处理
     */
    public function handleEditQueue($param = [])
    {
        $info = $this->getModel()->getInfo(['sms_signed_id' => $param['sms_signed_id']], false);
        if (empty($info) || (!empty($info) && $info['state'] != 2)) {
            return true;
        }
        $info = SmsRpc::_querySmsSign(['signName' => $info['signed']]);
        if (!empty($info['code']) && $info['code'] == 'OK') {
            if ($info['signStatus'] == 0) {
                return true;
            }
            if ($info['signStatus'] == 1) {
                return true;
            }
        }
        $image[] = [
            'fileContents' => $this->getImageBase64($param['authorization']),
            'fileSuffix' => $this->getImageType($param['authorization']),
        ];
        $arrCertificate = is_array($param['certificate']) ? $param['certificate'] : json_decode($param['certificate'], true);

        foreach ($arrCertificate as $v) {
            if (!empty($v['url'])) {
                $image[] = [
                    'fileContents' => $this->getImageBase64($v['url']),
                    'fileSuffix' => $this->getImageType($v['url']),
                ];
            }
        }
        $data = [
            'signName' => $param['signed'],
            'signSource' => $param['sign_source'],
            'remark' => $param['remark'],
            'signFileList' => $image,
        ];
        SmsRpc::_modifySmsSign($data);
        $this->getModel()->editData(['sms_signed_id' => $param['sms_signed_id'], 'submit_state' => 1]);
        return true;
    }

    /**
     * 定时任务拉取审核结果
     */
    public function timeGetResult()
    {
        $list = $this->getDataList(['submit_state' => 1]);
        if (empty($list)) {
            $this->repeatAddQueue();
            return true;
        }
        foreach ($list as $v) {
            echo '查看状态:' . $v['signed'] . PHP_EOL;
            $info = SmsRpc::_querySmsSign(['signName' => $v['signed']]);
            if (!empty($info['code']) && $info['code'] == 'OK' && $info['signStatus'] > 0) {
                $this->getModel()->editData([
                    'sms_signed_id' => $v['sms_signed_id'],
                    'state' => $info['signStatus'],
                    'reason' => $info['reason'],
                    'submit_state' => 2,
                ]);
            }
        }
        $this->repeatAddQueue();
        return true;
    }

    /**
     * 没有成功加入队列的,重复加入队列处理
     */
    protected function repeatAddQueue()
    {
        $list = $this->getDataList(['submit_state' => 0]);
        if (empty($list)) {
            return true;
        }
        foreach ($list as $v) {
            echo "重新加入队列" . $v['signed'] . PHP_EOL;
            if ($v['state'] == 0 && $v['submit_state'] == 0) { //重新加入添加队列
                Queue::push("chb_lib\common\job\MessageSignedJob@addSmsSign", $v, 'smsSign:add_queue');
                $this->getModel()->editData(['sms_signed_id' => $v['sms_signed_id'], 'submit_state' => 1]);
            }
            if ($v['state'] == 2 && $v['submit_state'] == 0) { //审核失败,重新加入编辑队列
                Queue::push("chb_lib\common\job\MessageSignedJob@editSmsSign", $v, 'smsSign:edit_queue');
                $this->getModel()->editData(['sms_signed_id' => $v['sms_signed_id'], 'submit_state' => 1, 'state' => 0]);
            }
        }
        return true;
    }
}
