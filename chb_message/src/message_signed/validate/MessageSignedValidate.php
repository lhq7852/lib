<?php

/**
 * 验证
 */

namespace chb_message\message_signed\validate;

use chb_lib\common\ValidateTrait;
use think\Validate;

class MessageSignedValidate extends Validate
{
    use ValidateTrait;

    protected $being = "signed"; //唯一字段检查

    protected $rule = [
        'signed' => 'require|max:200|checkBeing',
        'sign_source' => 'require|between:0,5',
        "certificate" => 'require|checkJson|checkImageSize:2048,url',
        "authorization" => 'require|checkImageSize:2048',
    ];
}
