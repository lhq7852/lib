<?php

/**
 * 模型
 */

namespace chb_message\message\model;

use chb_lib\common\BaseModel;

class SmsMessageModel extends BaseModel
{

    protected $name = 'log_sms';
    protected $pk = 'sms_log_id';

    protected $hidden = ['delete_time'];
    protected $likeList = ["keyword" => "message_name"]; //设置模糊搜索映射的字段 alias|value

}
