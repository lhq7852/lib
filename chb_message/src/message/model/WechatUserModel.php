<?php

/**
 * 模型
 */

namespace chb_message\message\model;

use chb_lib\common\BaseModel;

class WechatUserModel extends BaseModel
{

    protected $name = 'a_log_wechat_user';
    protected $pk = 'wechat_user_id';

    protected $hidden = ['delete_time'];
    protected $likeList = ["keyword" => "wechat_open_id"]; //设置模糊搜索映射的字段 alias|value

}
