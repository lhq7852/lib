<?php

/**
 * 模型
 */

namespace chb_message\message\model;

use chb_lib\common\BaseModel;

class SmsUserModel extends BaseModel
{

    protected $name = 'log_sms_user';
    protected $pk = 'sms_user_id';

    protected $hidden = ['delete_time'];
    protected $likeList = ["keyword" => "is_read"]; //设置模糊搜索映射的字段 alias|value

}
