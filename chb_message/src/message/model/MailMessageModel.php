<?php

/**
 * 模型
 */

namespace chb_message\message\model;

use chb_lib\common\BaseModel;

class MailMessageModel extends BaseModel
{

    protected $name = 'log_mail';
    protected $pk = 'mail_log_id';

    protected $hidden = ['delete_time'];
    protected $likeList = ["keyword" => "message_name"]; //设置模糊搜索映射的字段 alias|value

}
