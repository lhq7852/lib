<?php

/**
 * 模型
 */

namespace chb_message\message\model;

use chb_lib\common\BaseModel;

class EmailUserModel extends BaseModel
{

    protected $name = 'log_email_user';
    protected $pk = 'email_user_id';

    protected $hidden = ['delete_time'];
    protected $likeList = ["keyword" => "email"]; //设置模糊搜索映射的字段 alias|value

}
