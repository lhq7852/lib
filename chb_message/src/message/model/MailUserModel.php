<?php

/**
 * 模型
 */

namespace chb_message\message\model;

use chb_lib\common\BaseModel;

class MailUserModel extends BaseModel
{

    protected $name = 'log_mail_user';
    protected $pk = 'mail_user_id';

    protected $hidden = ['delete_time'];
    protected $likeList = ["keyword" => "is_read"]; //设置模糊搜索映射的字段 alias|value

}
