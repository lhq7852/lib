<?php
/**
 * 短信
 */
namespace chb_message\message;

use chb_lib\common\RedisToolTrait;
use chb_lib\common\ServiceTrait;
use chb_message\message\model\SmsUserModel;
use chb_rpc\external_api\SmsRpc;

class SmsMessage
{

    use RedisToolTrait, ServiceTrait;

    public $accessKeyId = '';
    protected $accessKeySecret = '';
    protected $maxSms = 10; //每天上限发送短信次数
    protected $durationTime = 900; //有效时长-持续时间
    protected $intervalTime = 60; //休息时长-间隔时长
    protected $arrCheck = [];
    protected $templateParam = ''; //发送的内容
    protected $phone;
    protected $template = [
        "signName" => "",
        "templateCode" => "",
    ]; //配置模板

    public function __construct()
    {
        $this->time = time();
        $this->redisKey = "SmsModel:";
    }

    /**
     * 监听添加
     */
    public function listenAdd()
    {
        if (!empty($this->dataInfo['sms_log_id']) && !empty($this->param['to_phone'])) {
            foreach ($this->param['to_phone'] as $to_phone) {
                $data[] = [
                    'sms_log_id' => $this->dataInfo['sms_log_id'],
                    'phone' => $to_phone,
                ];
            }
            if (!empty($data)) {
                (new SmsUserModel)->addData($data);
            }
        }
        return true;
    }

    /**
     * 配置阿里云模板
     *
     * @param array $template
     * @return object
     */
    public function setTemplate($template = [])
    {
        if (!isset($template['accessKeySecret']) || empty($template['accessKeySecret']) || !isset($template['accessKeyId']) || empty($template['accessKeyId'])) {
            throw new \Exception("请先配置阿里云短信");
        }
        $this->template = $template;
        $this->accessKeyId = $template['accessKeyId'] ?? $this->accessKeyId;
        $this->accessKeySecret = $template['accessKeySecret'] ?? $this->accessKeySecret;

        $this->maxSms = $template['maxMobile'] ?? $this->maxSms;
        $this->durationTime = $template['durationTime'] ?? $this->durationTime;
        $this->intervalTime = $template['intervalTime'] ?? $this->intervalTime;
        return $this;
    }

    /**
     * 发送短信的手机号
     *
     * @param string $phone
     * @return object
     */
    public function setPhone($phone = '')
    {
        if (!$this->mobileVerify($phone)) {
            throw new \Exception("请输入正确的手机号码", 500);
        }
        $this->phone = $phone;
        $this->redisKeyName = $this->redisKey . date("Ymd") . ":" . $phone;
        $this->arrCheck = $this->setRedisKey($this->redisKeyName)->getRedisArray();
        return $this;
    }

    /**
     * 设置发送内容
     *
     * @return object
     */
    public function setContent($param = [])
    {
        return $this;
    }

    /**
     * 成功回调
     *
     * @return bool
     */
    public function successBack()
    {
        return true;
    }

    /**
     * 发送消息-对接外部资源发送消息
     *
     * @return bool
     */
    public function send($default = false)
    {
        $ret = SmsRpc::_sendSms([
            'phone' => $this->phone,
            "signName" => $this->template['signName'],
            "templateCode" => $this->template['templateCode'],
            "templateParam" => $this->templateParam,
            "accessKeyId" => $this->accessKeyId,
            "accessKeySecret" => $this->accessKeySecret,
        ]);
        $this->successBack();
        if (!$default) {
            return true;
        }
        return $ret;
    }
}
