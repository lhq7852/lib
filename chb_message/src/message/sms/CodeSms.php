<?php

namespace chb_message\message\sms;

use chb_lib\common\SingleTrait;
use chb_message\message\SmsMessage;

class CodeSms extends SmsMessage
{
    use SingleTrait;

    /**
     * 设置发送内容
     *
     * @return void
     */
    public function setContent($param = [])
    {
        return $this->checkSms()->createCode();
    }

    /**
     * 检查当天发送短信上限次数
     *
     * @return object
     */
    public function checkSms()
    {
        $ret = $this->arrCheck ?? [];
        if (empty($ret)) {
            return $this;
        }
        if ($ret['num'] >= $this->maxSms) {
            throw new \Exception("当天发送短信已上限");
        }
        $s = $this->time - $ret['lastTime'];
        if ($s <= $this->intervalTime) {
            throw new \Exception("请" . ($this->intervalTime - $s) . "秒后重新发送");
        }
        return $this;
    }

    /**
     * 验证短信验证码
     *
     * @param string $code
     * @return bool
     */
    public function verifyCode($code = '')
    {
        if ($this->arrCheck['code'] != $code) {
            throw new \Exception("验证码错误");
        }
        $s = $this->time - $this->arrCheck['lastTime'];
        if ($s > $this->durationTime) {
            throw new \Exception("短信验证码已过期");
        }
        return true;
    }

    /**
     * 生成短信验证码
     *
     * @return void
     */
    public function createCode()
    {
        $this->arrCheck['code'] = rand(10000, 99999);
        $this->templateParam = "{\"code\":\"" . $this->arrCheck['code'] . "\"}";
        return $this;
    }

    /**
     * 发送成功处理
     *
     * @return bool
     */
    public function successBack()
    {
        $this->arrCheck['num'] = empty($this->arrCheck['num']) ? 1 : $this->arrCheck['num'] + 1; //当天发送次数
        $this->arrCheck['lastTime'] = $this->time; //上一次发送短信时间
        $endTime = strtotime(date("Y-m-d", strtotime("+1 day"))); //第二天0时
        $this->setRedisKey($this->redisKeyName)->setRedisTime($endTime - $this->time)->setRedisValue($this->arrCheck);
        return true;
    }
}
