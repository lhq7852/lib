<?php

namespace chb_message\message\sms;

use chb_message\message\SmsMessage;

class TemplateSms extends SmsMessage
{

    /**
     * 设置发送内容
     *
     * @return void
     */
    public function setContent($param = [])
    {
        // $this->arrCheck['content'] = json_encode($param, JSON_UNESCAPED_UNICODE);
        $this->templateParam = json_encode($param, JSON_UNESCAPED_UNICODE);
        return $this;
    }

    /**
     * 发送成功处理
     *
     * @return bool
     */
    public function successBack()
    {
        // $this->arrCheck['num'] = empty($this->arrCheck['num']) ? 1 : $this->arrCheck['num'] + 1; //当天发送次数
        // $this->arrCheck['lastTime'] = $this->time; //上一次发送短信时间
        // $endTime = strtotime(date("Y-m-d", strtotime("+1 day"))); //第二天0时
        // $this->setRedisKey($this->redisKey)->setRedisTime($endTime - $this->time)->setRedisValue($this->arrCheck);
        return true;
    }
}
