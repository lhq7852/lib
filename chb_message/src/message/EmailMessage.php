<?php
/**
 * 邮件
 */
namespace chb_message\message;

use chb_lib\common\RedisToolTrait;
use chb_lib\common\RpcServerTrait;
use chb_lib\common\ServiceTrait;
use chb_message\message\model\EmailUserModel;
use chb_rpc\external_api\SmsRpc;

class EmailMessage
{

    use RedisToolTrait, ServiceTrait, RpcServerTrait;

    public $accessKeyId = '';
    protected $accessKeySecret = '';
    protected $maxSms = 10; //每天上限发送次数
    protected $durationTime = 900; //有效时长-持续时间
    protected $intervalTime = 60; //休息时长-间隔时长
    protected $arrCheck = [];
    protected $templateParam = ''; //发送的内容
    protected $email;
    protected $template = [
        "signName" => "",
        "templateCode" => "",
    ]; //配置模板

    public function __construct()
    {
        $this->time = time();
        $this->redisKey = "EmailMessage:";
    }

    /**
     * 监听添加
     */
    public function listenAdd()
    {
        if (!empty($this->dataInfo['email_log_id']) && !empty($this->param['to_email'])) {
            foreach ($this->param['to_email'] as $to_email) {
                $data[] = [
                    'email_log_id' => $this->dataInfo['email_log_id'],
                    'email' => $to_email,
                ];
            }
            if (!empty($data)) {
                (new EmailUserModel)->addData($data);
            }
        }
        return true;
    }

    /**
     * 配置阿里云模板
     *
     * @param array $template
     * @return object
     */
    public function setTemplate($template = [])
    {
        // if (!isset($template['accessKeySecret']) || empty($template['accessKeySecret']) || !isset($template['accessKeyId']) || empty($template['accessKeyId'])) {
        //     throw new \Exception("请先配置阿里云邮件");
        // }
        // $this->template = $template;
        // $this->accessKeyId = $template['accessKeyId'] ?? $this->accessKeyId;
        // $this->accessKeySecret = $template['accessKeySecret'] ?? $this->accessKeySecret;

        $this->maxSms = $template['maxMobile'] ?? $this->maxSms;
        $this->durationTime = $template['durationTime'] ?? $this->durationTime;
        $this->intervalTime = $template['intervalTime'] ?? $this->intervalTime;
        return $this;
    }

    /**
     * 发送短信的邮箱
     *
     * @param string $email
     * @return object
     */
    public function setEmail($email = '')
    {
        if (!$this->emailVerify($email)) {
            throw new \Exception("请输入正确的邮箱", 500);
        }
        $this->email = $email;
        $this->redisKeyName = $this->redisKey . date("Ymd") . ":" . $email;
        $this->arrCheck = $this->setRedisKey($this->redisKeyName)->getRedisArray();
        return $this;
    }

    /**
     * 设置发送内容
     *
     * @return void
     */
    public function setContent($param = [])
    {
        return $this->checkCode()->createCode();
    }

    /**
     * 检查当天发送短信上限次数
     *
     * @return object
     */
    public function checkCode()
    {
        $ret = $this->arrCheck ?? [];
        if (empty($ret)) {
            return $this;
        }
        if ($ret['num'] >= $this->maxSms) {
            throw new \Exception("当天发送次数已上限");
        }
        $s = $this->time - $ret['lastTime'];
        if ($s <= $this->intervalTime) {
            throw new \Exception("请" . ($this->intervalTime - $s) . "秒后重新发送");
        }
        return $this;
    }

    /**
     * 验证短信验证码
     *
     * @param string $code
     * @return bool
     */
    public function verifyCode($code = '')
    {
        if ($this->arrCheck['code'] != $code) {
            throw new \Exception("验证码错误");
        }
        $s = $this->time - $this->arrCheck['lastTime'];
        if ($s > $this->durationTime) {
            throw new \Exception("验证码已过期");
        }
        return true;
    }

    /**
     * 生成短信验证码
     *
     * @return void
     */
    public function createCode()
    {
        $this->arrCheck['code'] = rand(10000, 99999);
        return $this;
    }

    /**
     * 发送邮件
     * toAddress
     * subject
     * htmlBody
     * textBody
     */
    public function send()
    {
        SmsRpc::_sendEmail([
            'toAddress' => $this->email,
            'subject' => "注册",
            'htmlBody' => $this->arrCheck['code'],
            'textBody' => $this->arrCheck['code'],
        ]);
        return $this->successBack();
    }

    /**
     * 发送成功处理
     *
     * @return bool
     */
    public function successBack()
    {
        $this->arrCheck['num'] = empty($this->arrCheck['num']) ? 1 : $this->arrCheck['num'] + 1; //当天发送次数
        $this->arrCheck['lastTime'] = $this->time; //上一次发送短信时间
        $endTime = strtotime(date("Y-m-d", strtotime("+1 day"))); //第二天0时
        $this->setRedisKey($this->redisKeyName)->setRedisTime($endTime - $this->time)->setRedisValue($this->arrCheck);
        return true;
    }
}
