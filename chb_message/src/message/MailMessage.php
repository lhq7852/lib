<?php
/**
 * 站内信
 */
namespace chb_message\message;

use chb_lib\common\RedisToolTrait;
use chb_lib\common\ServiceTrait;
use chb_message\message\model\MailUserModel;

class MailMessage
{

    use RedisToolTrait, ServiceTrait;

    /**
     * 监听添加
     */
    public function listenAdd()
    {
        if (!empty($this->dataInfo['mail_log_id']) && !empty($this->param['to_user_no'])) {
            foreach ($this->param['to_user_no'] as $user_no) {
                $data[] = [
                    'mail_log_id' => $this->dataInfo['mail_log_id'],
                    'to_user_no' => $user_no,
                    'is_read' => 0,
                    'read_time' => null,
                ];
            }
            if (!empty($data)) {
                (new MailUserModel)->addData($data);
            }
        }
        return true;
    }

    
}
