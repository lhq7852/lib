<?php

/**
 * 消息服务
 */
namespace chb_message\message;

use chb_lib\common\BaseSystem;
use chb_lib\common\exception\Err;
use chb_lib\common\SingleTrait;
use chb_lib\common\UtilsTrait;
use chb_message\message\sms\TemplateSms;
use chb_message\template\EmailTemplate;
use chb_message\template\MailTemplate;
use chb_message\template\SmsTemplate;
use chb_message\template\TemplateGroup;
use chb_message\template\WechatTemplate;
use chb_rpc\external_api\SmsRpc;

class MessageService
{
    use SingleTrait, UtilsTrait;

    protected $type = [
        'mail_template_id' => 'sendMail',
        'sms_template_id' => 'sendSms',
        'wechat_template_id' => 'sendWechat',
        'email_template_id' => 'sendEmail',
    ];

    /**
     * 加入发送消息队列
     */
    public function send($param = [])
    {
        $info = TemplateGroup::_getInfo(['group_alias' => $param['alias']]);
        if (empty($info['relation'])) {
            Err::errorMsg(880);
        }
        return true;
    }

    /**
     * 发送消息
     */
    protected function sendMail($param = [])
    {
        $info = MailTemplate::_getInfo(['mail_template_id' => $param['template_id']]);
        if (empty($info)) {
            return true;
        }
        $content = $this->content_replace($info['content'], $param, "${", "}");
        $data = [
            'group_alias' => $param['alias'],
            'template_id' => $param['template_id'],
            'message_name' => $info['template_name'],
            'template_alias' => $info['template_alias'],
            'type' => $info['type'],
            'icon' => $info['icon'],
            'color' => $info['color'],
            'class_name' => $info['class_name'],
            'content' => $content,
            'state' => 1,
            'callback_url' => $param['callback_url'] ?? '',
            'to_user_no' => $this->explodeData(",", $param['to_user_no']),
        ];
        MailMessage::_setParam($data)->addData($data);
        return true;
    }

    /**
     * 发送消息
     */
    protected function sendSms($param = [])
    {
        if (empty($param['to_phone'])) {
            return false;
        }
        $info = SmsTemplate::_getInfo(['sms_template_id' => $param['template_id']]);
        if (empty($info)) {
            return true;
        }
        $ret = $this->content_replace($info['content'], $param, "${", "}", true);
        $data = [
            'group_alias' => $param['alias'],
            'template_id' => $param['template_id'],
            'message_name' => $info['template_name'],
            'template_alias' => $info['template_alias'],
            'type' => $info['type'],
            'content' => $ret['content'] ?? '',
            'state' => 1,
            'to_phone' => $this->explodeData(",", $param['to_phone']),
        ];
        $sms_token = (BaseSystem::_getObject("SystemConfig"))::_getKeyConfig("sms_token");
        $ret = TemplateSms::_setPhone(implode(",", $data['to_phone']))->setTemplate($sms_token)->setContent($ret['field'])->send(true);

        $data['state'] = !empty($ret['code']) && $ret['code'] == 'OK' ? 1 : 2;
        $data['reason'] = json_encode($ret);

        SmsMessage::_setParam($data)->addData($data);
        return true;
    }

    /**
     * 发送消息
     */
    protected function sendWechat($param = [])
    {
        $info = WechatTemplate::_getInfo(['wechat_template_id' => $param['template_id']]);
        if (empty($info)) {
            return true;
        }
        $content = $this->content_replace($info['content'], $param, "${", "}");
        $data = [
            'group_alias' => $param['alias'],
            'template_id' => $param['template_id'],
            'message_name' => $info['template_name'],
            'template_alias' => $info['template_alias'],
            'type' => $info['type'],
            'icon' => $info['icon'],
            'color' => $info['color'],
            'class_name' => $info['class_name'],
            'content' => $content,
            'state' => 1,
            'callback_url' => $param['callback_url'] ?? '',
            'to_user_no' => $this->explodeData(",", $param['to_user_no']),
        ];
        MailMessage::_setParam($data)->addData($data);
        return true;
    }

    /**
     * 发送消息
     */
    protected function sendEmail($param = [])
    {
        if (empty($param['to_email'])) {
            return false;
        }
        $info = EmailTemplate::_getInfo(['email_template_id' => $param['template_id']]);
        if (empty($info)) {
            return true;
        }
        $content = $this->content_replace($info['content'], $param, "${", "}");
        $data = [
            'group_alias' => $param['alias'],
            'template_id' => $param['template_id'],
            'message_name' => $info['template_name'],
            'template_alias' => $info['template_alias'],
            'type' => $info['type'],
            'content' => $content,
            'state' => 1,
            'to_email' => $this->explodeData(",", $param['to_email']),
        ];
        $ret = SmsRpc::_sendEmail([
            'toAddress' => implode(",", $data['to_email']),
            'subject' => $data['message_name'],
            'htmlBody' => $content,
            'textBody' => strip_tags($content),
        ]);
        $data['env_id'] = !empty($ret['data']['EnvId']) ? $ret['data']['EnvId'] : '';
        $data['state'] = !empty($ret['data']['EnvId']) ? 1 : 2;
        $data['reason'] = json_encode($ret);

        EmailMessage::_setParam($data)->addData($data);
        return true;
    }

    /**
     * 处理发送消息
     *
     * @param array $param
     * @return void|bool
     */
    public function handleSend($param = [])
    {
        $info = TemplateGroup::_getInfo(['group_alias' => $param['alias']]);
        if (empty($info['relation'])) {
            return false;
        }
        foreach ($info['relation'] as $v) {
            if (!empty($v['template_id']) && !empty($this->type[$v['template_type']]) && method_exists($this, $this->type[$v['template_type']])) {
                $method = $this->type[$v['template_type']];
                $this->$method(array_merge($param, [
                    'template_id' => $v['template_id'],
                ]));
            }
        }
        return true;
    }

}
