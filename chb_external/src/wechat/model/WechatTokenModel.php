<?php

/**
 * 模型
 */

namespace chb_external\wechat\model;

use chb_lib\common\BaseModel;

class WechatTokenModel extends BaseModel
{

    protected $name = 'wechat_token';
    protected $pk = 'id';

    protected $hidden = ['delete_time'];
    protected $likeList = ["keyword" => "type_pk"]; //设置模糊搜索映射的字段 alias|value

}
