<?php

/**
 * 模型
 */

namespace chb_external\wechat\model;

use chb_lib\common\BaseModel;

class WechatAppModel extends BaseModel
{

    protected $name = 'wechat_app';
    protected $pk = 'id';

    protected $hidden = ['delete_time'];
    protected $likeList = ["keyword" => "authorizer_appid"]; //设置模糊搜索映射的字段 alias|value

}
