<?php

/**
 * 模型
 */

namespace chb_external\wechat\model;

use chb_lib\common\BaseModel;

class WechatVerifyTicketModel extends BaseModel
{

    protected $name = 'wechat_verify_ticket';
    protected $pk = 'id';

    protected $hidden = ['delete_time'];
    protected $likeList = ["keyword" => "token"]; //设置模糊搜索映射的字段 alias|value

}
