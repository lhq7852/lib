<?php
/**
 * 微信第三方平台服务
 */
namespace chb_external\wechat;

use chb_external\request_service\ApiConfig;
use chb_external\request_service\RequestService;
use chb_external\wechat\model\WechatAppModel;
use chb_external\wechat\model\WechatTokenModel;
use chb_lib\common\exception\Err;
use chb_lib\common\RedisToolTrait;
use chb_lib\common\SingleTrait;
use chb_lib\common\UtilsTrait;

class WechatLoginService
{
    use RedisToolTrait, SingleTrait, UtilsTrait;

    protected $passCallback = "/external/wechat/login_pass_callback";
    protected $appModel;

    public function __construct()
    {
        $this->redisKey = "Wechat:WechatLoginService:";
        $this->appModel = new WechatAppModel();
        $this->tokenModel = new WechatTokenModel();
    }

    /**
     *
     * 请求code
     * 所属用户的系统应用标识 client_pk
     *
     * scope 应用授权作用域，snsapi_base （不弹出授权页面，直接跳转，只能获取用户openid），snsapi_userinfo （弹出授权页面，可通过openid拿到昵称、性别、所在地。并且， 即使在未关注的情况下，只要用户授权，也能获取其信息 ）
     * @return void
     *
     */
    public function requestCode($param = [])
    {
        $check = $this->appModel->getInfo(['client_pk' => $param['param_config']['client_pk']]);
        if (empty($check['authorizer_appid'])) {
            Err::errorMsg(920);
        }
        $token = $this->createUid();
        $this->tokenModel->addData([
            'token' => $token,
            "value" => $param['param_config']['state'],
        ]);
        $url = sprintf($param['request_url'], $check['authorizer_appid'], config("chb.platform_host") . $this->passCallback, $param['param_config']['scope'], $token, $param['param_config']['component_appid']);
        return [
            'url' => $url,
        ];
    }

    /**
     * 用code 换 access_token
     *
     */
    public function getAccessToken($param = [])
    {
        $check = $this->appModel->getInfo(['client_pk' => $param['param_config']['client_pk']]);
        if (empty($check['authorizer_appid'])) {
            Err::errorMsg(911);
        }
        $param['param_config']['appid'] = $check['authorizer_appid'];
        $param['param_config']['component_access_token'] = ApiConfig::_startGetData(['method' => "getComponentAccessToken", "alias" => "wechat_component_token"]);

        $param['request_url'] = sprintf($param['request_url'], $param['param_config']['appid'], $param['param_config']['code'], $param['param_config']['component_appid'], $param['param_config']['component_access_token']);
        $data = $this->sendRuqest($param);
        return $data;
    }

    /**
     * 只有授权的code 换的access_token 可以 换 用户信息-- 有头像,有昵称
     *
     */
    public function getUserInfo($param = [])
    {
        $param['request_url'] = sprintf($param['request_url'], $param['param_config']['access_token'], $param['param_config']['openid']);

        $data = $this->sendRuqest($param);
        return $data;
    }

    /**
     * 用access_token 换 用户信息-公众号-基本信息-无头像,无昵称
     *
     */
    public function getUserBasicInfo($param = [])
    {
        $param['param_config']['access_token'] = ApiConfig::_startGetData(['method' => "getAuthorizerToken", "alias" => "wechat_authorizer_token", "client_pk" => $param['param_config']['client_pk']]);
        $param['request_url'] = sprintf($param['request_url'], $param['param_config']['access_token'], $param['param_config']['openid']);
        $data = $this->sendRuqest($param);
        return $data;
    }

    /**
     * 根据appid 和密钥获取access_token
     */
    public function getAccessTokenByAppId($param = [])
    {
        $param['request_url'] = sprintf($param['request_url'], $param['param_config']['appid'], $param['param_config']['secret']);
        $data = $this->sendRuqest($param);
        if (!empty($data['result']['access_token'])) {
            return $data['result']['access_token'];
        }
        return '';
    }

    /**
     * 微信场景二维码
     * action_info: {"scene": {"scene_str": "test"}}
     * action_name : QR_STR_SCENE 临时二维码 QR_LIMIT_STR_SCENE 永久二维码
     */
    public function getSceneQrcode($param = [])
    {
        $param['param_config']['access_token'] = ApiConfig::_startGetData(['method' => "getAuthorizerToken", "alias" => "wechat_authorizer_token", "client_pk" => $param['param_config']['client_pk']]);
        $param['request_url'] = sprintf($param['request_url'], $param['param_config']['access_token']);

        $token = $this->createUid();
        $this->tokenModel->addData([
            'token' => $token,
            "value" => $param['param_config']['scene_str'],
        ]);
        $param['param_config']['action_info']['scene']['scene_str'] = $token;
        $data = $this->sendRuqest($param);
        return $data;
    }

    /**
     * 发送请求
     *
     * @param array $param
     * @return void|array
     */
    public function sendRuqest($param = [])
    {
        $data = RequestService::_setUrl($param['request_url'])->setMethod($param['mode'])
            ->setHeaderConfig(array_merge($param['header_config'] ?? [], []))
            ->setParamConfig($param['param_config'])
            ->setReturnField($param['return_field'])
            ->request();
        return [
            'code' => !empty($data['errcode']) ? "500" : '200',
            'msg' => !empty($data['errcode']) ? "fail" : 'success',
            "result" => $data,
        ];
    }

}
