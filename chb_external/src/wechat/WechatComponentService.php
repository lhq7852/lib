<?php
/**
 * 微信第三方平台服务
 */
namespace chb_external\wechat;

use chb_external\request_service\ApiConfig;
use chb_external\request_service\RequestService;
use chb_external\wechat\crypt\WXBizMsgCrypt;
use chb_external\wechat\model\WechatAppModel;
use chb_external\wechat\model\WechatTokenModel;
use chb_external\wechat\model\WechatVerifyTicketModel;
use chb_lib\common\exception\Err;
use chb_lib\common\RedisToolTrait;
use chb_lib\common\SingleTrait;
use chb_lib\common\UtilsTrait;

class WechatComponentService
{
    use RedisToolTrait, SingleTrait, UtilsTrait;

    protected $passCallback = "/external/wechat/pass_callback/client_pk/";
    protected $appId = 'wxfa156cfce4760293';
    protected $appSecret = 'f15a4e39ae56f80b96f1664c74ccbf3d';
    protected $encodingAesKey = 'QWERTYUIOPASDFGHJKLZXC00NMSDFGHJKERTYUIDFGH'; //消息加解密Key
    protected $token = 'FGHJKLERTYU10VBNM'; //FGHJKLERTYU10VBNM
    protected $appModel;
    protected $arrScene = ['login' => "登录成功"];

    public function __construct()
    {
        $this->redisKey = "Wechat:ComponentService:";
        $this->model = new WechatVerifyTicketModel();
        $this->appModel = new WechatAppModel();
        $this->tokenModel = new WechatTokenModel();
    }

    /**
     * 保存微信- 验证票据
     *
     * @return string|bool
     */
    public function saveComponentVerifyTicket($param = [])
    {
        trace(['param' => $param], "chb_error");

        $timeStamp = $param['timestamp'] ?? '';
        $nonce = $param['nonce'] ?? '';
        $msg_sign = $param['msg_signature'] ?? '';
        $encryptMsg = $param['encryptMsg'] ?? '';

        if (empty($timeStamp) || empty($nonce) || empty($msg_sign) || empty($encryptMsg)) {
            return true;
        }

        $token = "FGHJKLERTYU10VBNM";
        $encodingAesKey = "QWERTYUIOPASDFGHJKLZXC00NMSDFGHJKERTYUIDFGH";
        $appId = "wxfa156cfce4760293";

        $pc = new WXBizMsgCrypt($token, $encodingAesKey, $appId);
        $xml_tree = new \DOMDocument();
        $xml_tree->loadXML($encryptMsg);
        $array_e = $xml_tree->getElementsByTagName('Encrypt');
        $encrypt = $array_e->item(0)->nodeValue;

        $format = "<xml><ToUserName><![CDATA[ToUser]]></ToUserName><Encrypt><![CDATA[%s]]></Encrypt></xml>";
        $from_xml = sprintf($format, $encrypt);

        $msg = '';
        $errCode = $pc->decryptMsg($msg_sign, $timeStamp, $nonce, $from_xml, $msg);
        if ($errCode > 0) {
            trace("微信消息解析失败", "chb_error");
            return true;
        }
        $msg_tree = new \DOMDocument();
        $msg_tree->loadXML($msg);
        $array_AppId = $msg_tree->getElementsByTagName('AppId');
        $array_CreateTime = $msg_tree->getElementsByTagName('CreateTime');
        $array_InfoType = $msg_tree->getElementsByTagName('InfoType');
        $array_ComponentVerifyTicket = $msg_tree->getElementsByTagName('ComponentVerifyTicket');

        $data['AppId'] = $array_AppId->item(0)->nodeValue;
        $data['CreateTime'] = $array_CreateTime->item(0)->nodeValue;
        $data['InfoType'] = $array_InfoType->item(0)->nodeValue;
        $data['ComponentVerifyTicket'] = $array_ComponentVerifyTicket->item(0)->nodeValue;

        $this->setRedisKey($this->redisKey . "ComponentVerifyTicket")->setRedisTime(60 * 60 * 11)->setRedisValue($data);

        $this->model->addData([
            'info_type' => $data['InfoType'],
            'platform_app_id' => $data['AppId'],
            'deadline' => date("Y-m-d H:i:s", $data['CreateTime'] + 60 * 60 * 12),
            'component_verify_ticket' => $data['ComponentVerifyTicket'],
            'create_time' => date("Y-m-d H:i:s", $data['CreateTime']),
        ]);
        return true;
    }

    /**
     * 保存微信- 验证票据
     *
     * @return string|bool
     */
    public function messageDecrypt($param = [])
    {
        if (empty($param['timestamp']) || empty($param['nonce']) || empty($param['msg_signature']) || empty($param['message'])) {
            return true;
        }

        $token = "FGHJKLERTYU10VBNM";
        $encodingAesKey = "QWERTYUIOPASDFGHJKLZXC00NMSDFGHJKERTYUIDFGH";
        $appId = "wxfa156cfce4760293";

        $pc = new WXBizMsgCrypt($token, $encodingAesKey, $appId);
        $xml_tree = new \DOMDocument();
        $xml_tree->loadXML($param['message']);
        $array_e = $xml_tree->getElementsByTagName('Encrypt');
        $encrypt = $array_e->item(0)->nodeValue;

        $format = "<xml><ToUserName><![CDATA[ToUser]]></ToUserName><Encrypt><![CDATA[%s]]></Encrypt></xml>";
        $from_xml = sprintf($format, $encrypt);

        $msg = '';
        $errCode = $pc->decryptMsg($param['msg_signature'], $param['timestamp'], $param['nonce'], $from_xml, $msg);

        if ($errCode > 0) {
            trace("微信消息解析失败", "chb_error");
            return true;
        }
        $msg_tree = new \DOMDocument();
        $msg_tree->loadXML($msg);
        $EventKey = $msg_tree->getElementsByTagName('EventKey');
        $FromUserName = $msg_tree->getElementsByTagName('FromUserName');
        $ToUserName = $msg_tree->getElementsByTagName('ToUserName');

        $scene = $EventKey->item(0)->nodeValue;
        $data['openid'] = $FromUserName->item(0)->nodeValue;
        $ToUserName = $ToUserName->item(0)->nodeValue;

        trace($msg, "chb_error");
        $info = $this->tokenModel->getInfo(['token' => str_replace("qrscene_", "", $scene)], false);
        $arrScene = explode(config("chb.connect_str"), $info['value']);

        list($sceneType, $data['host'], $data['session_id']) = $arrScene;

        $content = $this->arrScene[$sceneType] ?? 'success';
        $timestamp = time();
        $nonce = $this->createUid();
        $data['txt'] = "<xml><ToUserName><![CDATA[" . $data['openid'] . "]]></ToUserName>
        <FromUserName><![CDATA[" . $ToUserName . "]]></FromUserName>
        <CreateTime>$timestamp</CreateTime>
        <MsgType><![CDATA[text]]</MsgType>
        <Content><![CDATA[" . $content . "]]</Content>
        <FuncFlag>0</FuncFlag>
        </xml>";

        // $pc = new WXBizMsgCrypt($token, $encodingAesKey, "wxdf9532ed991cb304");
        $pc->encryptMsg($data['txt'], $timestamp, $nonce, $data['msg']);

        // $xml_tree = new \DOMDocument();
        // $xml_tree->loadXML($data['msg']);
        // $array_e = $xml_tree->getElementsByTagName('Encrypt');
        // $array_s = $xml_tree->getElementsByTagName('MsgSignature');
        // $encrypt = $array_e->item(0)->nodeValue;
        // $msg_sign = $array_s->item(0)->nodeValue;
        // $format = "<xml><ToUserName><![CDATA[ToUser]]></ToUserName><Encrypt><![CDATA[%s]]></Encrypt></xml>";
        // $from_xml = sprintf($format, $encrypt);
        // $pc->decryptMsg($msg_sign, $timestamp, $nonce, $from_xml, $msg2);
        // print_r($data);
        // print_r($msg2);exit;
        return $data;
    }

    /**
     * 获取验证票据
     */
    public function getVerifyTicket($param = [])
    {
        $info = $this->model->getInfo(['platform_app_id' => $param['app_id']], '');
        if (empty($info['component_verify_ticket'])) {
            return '';
        }
        return $info['component_verify_ticket'];
    }

    /**
     * 获取令牌
     *
     * @return string|void
     */
    public function getComponentAccessToken($param = [])
    {
        $data = $this->setRedisKey($this->redisKey . "getComponentAccessToken")->getRedisArray();
        if (!empty($data)) {
            return $data;
        }
        $componentVerifyTicket = $this->getVerifyTicket(['app_id' => $param['param_config']['component_appid']]);
        $param['param_config'] = array_merge($param['param_config'], ['component_verify_ticket' => $componentVerifyTicket]);

        $data = $this->sendRuqest($param);

        if (empty($data['result']['component_access_token'])) {
            trace("getComponentAccessToken=error", "chb_error");
            trace($data, "chb_error");
            return '';
        }
        $this->setRedisKey($this->redisKey . "getComponentAccessToken")->setRedisTime(60 * 5)->setRedisValue($data['result']['component_access_token']);

        return $data['result']['component_access_token'] ?? '';

    }

    /**
     * 获取预授权码
     */
    public function getPreAuthCode($param = [])
    {
        $component_access_token = ApiConfig::_startGetData(['method' => "getComponentAccessToken", "alias" => "wechat_component_token"]);
        $param['param_config']['component_access_token'] = $component_access_token;

        $param['request_url'] = sprintf($param['request_url'], $param['param_config']['component_access_token']);
        $data = $this->sendRuqest($param);
        if (empty($data['result']['pre_auth_code'])) {
            trace("getPreAuthCode=error", "chb_error");
            trace($data, "chb_error");
            return '';
        }
        return $data['result']['pre_auth_code'];
    }

    /**
     * pc 授权链接拼接
     *
     * @return string
     */
    public function getPcPass($param = [])
    {
        $check = $this->appModel->getInfo(['client_pk' => $param['client_pk']]);
        if (!empty($check['authorizer_appid'])) {
            Err::errorMsg(910);
        }
        $redirect_uri = urlencode(config("chb.platform_host") . $this->passCallback . $param['client_pk']);
        $pre_auth_code = ApiConfig::_startGetData(['method' => "getPreAuthCode", "alias" => "wechat_pre_auth_code"]);
        $url = "https://mp.weixin.qq.com/cgi-bin/componentloginpage?component_appid=%s&pre_auth_code=%s&redirect_uri=%s&auth_type=3";
        $url = sprintf($url, $this->appId, $pre_auth_code, $redirect_uri);

        if (!empty($check)) {
            //发起授权记录
            $this->appModel->where(['client_pk' => $param['client_pk']])->update([
                'state' => 0,
                'host' => $param['host'],
            ]);
        } else {
            //发起授权记录
            $this->appModel->addData([
                'client_pk' => $param['client_pk'],
                'host' => $param['host'],
                'func_info' => '',
                'state' => 0,
            ]);
        }
        if (!empty($param['redirect_uri'])) {
            $this->setRedisKey($this->redisKey . "getPcPass:" . $param['client_pk'])->setRedisTime(60 * 15)->setRedisValue($param['redirect_uri']);
        }
        return $url;
    }

    /**
     * 获取授权信息
     */
    public function getQueryAuth($param = [])
    {
        $client_pk = request()->param('client_pk', '');
        if (empty($client_pk)) {
            Err::errorMsg(900);
        }
        $component_access_token = ApiConfig::_startGetData(['method' => "getComponentAccessToken", "alias" => "wechat_component_token"]);
        $param['param_config']['component_access_token'] = $component_access_token;

        $param['request_url'] = sprintf($param['request_url'], $param['param_config']['component_access_token']);
        $data = $this->sendRuqest($param);
        if ($data['code'] == 500) {
            $this->appModel->where(['client_pk' => $client_pk])->update([
                'state' => 2,
                'reason' => json_encode($data),
            ]);
            return false;
        }
        $check = $this->appModel->getInfo(['authorizer_appid' => $data['result']['authorization_info']['authorizer_appid']]);
        if (!empty($check) && $check['client_pk'] != $client_pk) {
            $this->appModel->where(['client_pk' => $client_pk])->update([
                'state' => 2,
                'reason' => "该微信已绑定其他账号",
            ]);
            return false;
        }
        if (!empty($client_pk)) {
            $this->appModel->where(['client_pk' => $client_pk])->update([
                'client_pk' => $client_pk,
                'authorizer_appid' => $data['result']['authorization_info']['authorizer_appid'],
                'expires_in' => $data['result']['authorization_info']['expires_in'],
                'authorizer_access_token' => $data['result']['authorization_info']['authorizer_access_token'],
                'authorizer_refresh_token' => $data['result']['authorization_info']['authorizer_refresh_token'],
                'func_info' => json_encode($data['result']['authorization_info']['func_info'], JSON_UNESCAPED_UNICODE),
                'deadline' => date("Y-m-d H:i:s", time() + $data['result']['authorization_info']['expires_in']),
                'state' => 1,
                'reason' => "授权绑定成功",
            ]);
        }
        $redirect_uri = $this->setRedisKey($this->redisKey . "getPcPass:" . $client_pk)->getRedisString();
        if (!empty($redirect_uri)) {
            return $redirect_uri;
        }
        return true;
    }

    /**
     * 获取授权信息
     *
     */
    public function getAuthorizerInfo($param = [])
    {
        $component_access_token = ApiConfig::_startGetData(['method' => "getComponentAccessToken", "alias" => "wechat_component_token"]);
        $param['param_config']['component_access_token'] = $component_access_token;

        $param['request_url'] = sprintf($param['request_url'], $param['param_config']['component_access_token']);
        $data = $this->sendRuqest($param);
        return $data;
    }

    /**
     * 检查租户是否授权
     */
    public function checkUserPassWechat($param = [])
    {
        return $this->appModel->getInfo(['client_pk' => $param['client_pk']]);
    }

    /**
     * 获取授权的AuthorizerToken
     */
    public function getAuthorizerToken($param = [])
    {
        $check = $this->appModel->getInfo(['client_pk' => $param['param_config']['client_pk']]);
        if (empty($check['authorizer_appid'])) {
            Err::errorMsg(911);
        }
        $param['param_config']['authorizer_appid'] = $check["authorizer_appid"];
        $param['param_config']['authorizer_refresh_token'] = $check["authorizer_refresh_token"];

        $component_access_token = ApiConfig::_startGetData(['method' => "getComponentAccessToken", "alias" => "wechat_component_token"]);
        $param['param_config']['component_access_token'] = $component_access_token;

        $param['request_url'] = sprintf($param['request_url'], $param['param_config']['component_access_token']);

        $data = $this->sendRuqest($param);
        if (!empty($data['result']['authorizer_access_token'])) {
            $this->appModel->where("id", $check['id'])->update([
                'authorizer_access_token' => $data['result']['authorizer_access_token'],
                'authorizer_refresh_token' => $data['result']['authorizer_refresh_token'],
            ]);
            return $data['result']['authorizer_access_token'];
        }
        return '';
    }

    /**
     * 发送请求
     *
     * @param array $param
     * @return void|array
     */
    public function sendRuqest($param = [])
    {
        $data = RequestService::_setUrl($param['request_url'])->setMethod($param['mode'])
            ->setHeaderConfig(array_merge($param['header_config'], []))
            ->setParamConfig($param['param_config'])
            ->setReturnField($param['return_field'])
            ->request();
        return [
            'code' => !empty($data['errcode']) ? "500" : '200',
            'msg' => !empty($data['errcode']) ? "fail" : 'success',
            "result" => $data,
        ];
    }
}
