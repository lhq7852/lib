<?php
/**
 * 宝塔 - 网站接口
 */
namespace chb_external\bt;

use chb_lib\common\RedisToolTrait;
use chb_lib\common\ServiceTrait;

class Website extends BtBase
{

    use RedisToolTrait, ServiceTrait;

    /**
     * 公共请求方法
     */
    public function sendRequest($param = [])
    {
        $this->BT_KEY = $param['param_config']['bt_token'];
        $this->BT_PANEL = $param['param_config']['bt_url'];
        $url = $param['param_config']['bt_url'] . $param['request_url'];

        //准备POST数据
        $p_data = $this->GetKeyData(); //取签名
        $p_data = array_merge($p_data, $param['param_config']);
        //请求面板接口
        $result = $this->HttpPostCookie($url, $p_data);
        //解析JSON数据
        $data = json_decode($result, true);
        return [
            'code' => 200,
            'msg' => !empty($data) && empty($data['siteStatus']) ? 'success' : 'fail',
            'result' => $data,
        ];
    }

    /**
     * 上传文件
     */
    public function uploadFile($param = [])
    {
        $this->BT_KEY = $param['param_config']['bt_token'];
        $this->BT_PANEL = $param['param_config']['bt_url'];
        $url = $param['param_config']['bt_url'] . $param['request_url'];
        $file = $this->getFile($param['param_config']['file']);
        if (empty($file)) {
            return [
                'code' => 500,
                'msg' => '文件不存在,上传失败',
                'result' => [],
            ];
        }
        //准备POST数据
        $p_data = $this->GetKeyData(); //取签名
        $p_data = array_merge($p_data, $param['param_config']);
        $p_data['f_name'] = $file['filename'];
        $p_data['f_size'] = $file['size'];
        $p_data['f_start'] = 0;
        $p_data['blob'] = 0;
        if (class_exists("\CURLFile")) {
            $p_data['blob'] = new \CURLFile(realpath($file['path']), $file['type'], $file['filename']);
        }
        //请求面板接口
        $result = $this->HttpPostCookie($url, $p_data);
        //解析JSON数据
        $data = json_decode($result, true);
        return [
            'code' => 200,
            'msg' => !empty($data) && empty($data['siteStatus']) ? 'success' : 'fail',
            'result' => $data,
        ];
    }

    /**
     * 根据文件后缀获取类型
     */
    private function getFile($file = '')
    {
        if (empty($file)) {
            return [];
        }
        $arr = explode(".", $file);
        $name = explode("/", $file);
        $t = end($arr);
        $type = [
            'gz' => 'application/gzip',
        ];
        return [
            'type' => $type[$t] ?? '',
            'size' => $this->getFileSize($file),
            'filename' => end($name),
            'path' => $file,
        ];
    }
}
