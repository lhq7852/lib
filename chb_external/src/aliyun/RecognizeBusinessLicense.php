<?php

/**
 * 机构-营业执照
 */
namespace chb_external\aliyun;

use AlibabaCloud\Client\AlibabaCloud;
use AlibabaCloud\Ocr\Ocr;
use chb_lib\common\exception\Err;
use chb_lib\common\RedisToolTrait;
use chb_lib\common\ServiceTrait;

class RecognizeBusinessLicense
{

    use RedisToolTrait, ServiceTrait;

    public $accessKeyId = '';
    public $accessKeySecret = '';

    /**
     * 获取信息
     */
    public function getBusinessLicenseInfo($param)
    {
        $info = $this->getInfo(['md5_image' => md5($param['md5_image'])], false);
        if (empty($info)) {
            return [];
        }
        $info['result'] = json_decode($info['result'], true);
        return $info;
    }

    /**
     * 识别营业执照
     *
     * @return array|Exception
     */
    public function businessLicense($param = [])
    {
        trace($param, "chb_error");
        if (empty($param['param_config']['image'])) {
            Err::errorMsg(800);
        }
        $info = $this->getBusinessLicenseInfo(['md5_image' => $param['param_config']['image']], false);
        if (!empty($info)) {
            return [
                'code' => 200,
                'msg' => 'success',
                'result' => $info['result']['Data'],
            ];
        }
        $this->accessKeyId = $param['param_config']['accessKeyId'] ?? '';
        $this->accessKeySecret = $param['param_config']['accessKeySecret'] ?? '';

        $result = $this->businessLicenseApi($param['param_config']);
        $this->addData([
            'result' => json_encode($result),
            'image' => $param['param_config']['image'],
            'md5_image' => md5($param['param_config']['image']),
        ]);
        if (empty($result['Data'])) {
            return [
                'code' => 200,
                'msg' => 'fail',
                'result' => $result,
            ];
        }
        return [
            'code' => 200,
            'msg' => 'success',
            'result' => $result['Data'],
        ];
    }

    /**
     * 识别营业执照-API
     *
     * @return array|Exception
     */
    public function businessLicenseApi($param = [])
    {
        AlibabaCloud::accessKeyClient($this->accessKeyId, $this->accessKeySecret)
            ->regionId('cn-shanghai')
            ->asDefaultClient();

        try {
            $request = Ocr::v20191230()->recognizeBusinessLicense();
            $result = $request
                ->withImageURL($param['image'])
                ->debug(false) // Enable the debug will output detailed information
                ->connectTimeout(1) // Throw an exception when Connection timeout
                ->timeout(1) // Throw an exception when timeout
                ->request();
            return $result->toArray();
        } catch (\Exception $exception) {
            trace("识别营业执照错误:" . $exception->getMessage());
        }
        return [];
    }
}
