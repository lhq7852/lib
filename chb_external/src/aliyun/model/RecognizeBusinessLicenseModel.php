<?php

/**
 * 模型
 */

namespace chb_external\aliyun\model;

use chb_lib\common\BaseModel;

class RecognizeBusinessLicenseModel extends BaseModel
{
    protected $name = 'recognize_business_license';
    protected $pk = 'id';

    protected $hidden = ['delete_time'];
    protected $likeList = ["keyword" => "md5_image"]; //设置模糊搜索映射的字段 alias|value


}
