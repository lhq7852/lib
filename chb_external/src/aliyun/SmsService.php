<?php

namespace chb_external\aliyun;

use AlibabaCloud\SDK\Dysmsapi\V20170525\Dysmsapi;
use AlibabaCloud\SDK\Dysmsapi\V20170525\Models\SendSmsRequest;
use chb_lib\common\SingleTrait;
use Darabonba\OpenApi\Models\Config;

class SmsService
{
    use SingleTrait;

    /**
     * 发送请求
     *
     * @return void
     */
    public function sendMsg($param)
    {
        $config = new Config([
            "accessKeyId" => $param['param_config']['accessKeyId'] ?? '', // 您的AccessKey ID
            "accessKeySecret" => $param['param_config']['accessKeySecret'] ?? '', // 您的AccessKey Secret
        ]);
        // 访问的域名
        $config->endpoint = "dysmsapi.aliyuncs.com";
        $client = new Dysmsapi($config);
        $sendSmsRequest = new SendSmsRequest([
            "phoneNumbers" => $param['param_config']['phone'] ?? '',
            "signName" => $param['param_config']['signName'] ?? '',
            "templateCode" => $param['param_config']['templateCode'] ?? '',
            "templateParam" => $param['param_config']['templateParam'] ?? '',
        ]);
        $ret = $client->sendSms($sendSmsRequest);
        return [
            'code' => 200,
            'msg' => $ret->body->code == 'OK' ? 'success' : 'fail',
            'result' => $ret->body,
        ];
    }
}
