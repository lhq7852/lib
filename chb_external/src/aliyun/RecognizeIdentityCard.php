<?php

/**
 * 身份证
 */
namespace chb_external\aliyun;

use AlibabaCloud\Client\AlibabaCloud;
use AlibabaCloud\Ocr\Ocr;
use chb_lib\common\exception\Err;
use chb_lib\common\RedisToolTrait;
use chb_lib\common\ServiceTrait;

class RecognizeIdentityCard
{

    use RedisToolTrait, ServiceTrait;

    public $accessKeyId = '';
    public $accessKeySecret = '';

    /**
     * 获取信息
     */
    public function getCardInfo($param)
    {
        $info = $this->getInfo(['md5_image' => md5($param['image'])], false);
        if (empty($info)) {
            return [];
        }
        $info['result'] = json_decode($info['result'], true);
        return $info;
    }

    /**
     * 识别身份证
     *
     * @return array|Exception
     */
    public function identityCard($param = [])
    {
        if (empty($param['param_config']['image']) || !in_array($param['param_config']['side'], ['face', 'back'])) {
            Err::errorMsg(800);
        }
        $info = $this->getCardInfo(['image' => $param['param_config']['image']]);
        if (!empty($info)) {
            return [
                'code' => 200,
                'msg' => 'success',
                'result' => $info['result']['Data']['FrontResult'],
            ];
        }
        $this->accessKeyId = $param['param_config']['accessKeyId'] ?? '';
        $this->accessKeySecret = $param['param_config']['accessKeySecret'] ?? '';

        $result = $this->identityCardApi($param['param_config']);
        $this->addData([
            'result' => json_encode($result),
            'side' => $param['param_config']['side'],
            'image' => $param['param_config']['image'],
            'md5_image' => md5($param['param_config']['image']),
        ]);
        if (empty($result['Data'])) {
            return [
                'code' => 200,
                'msg' => 'fail',
                'result' => $result,
            ];
        }
        return [
            'code' => 200,
            'msg' => 'success',
            'result' => $result['Data']['FrontResult'],
        ];
    }

    /**
     * 识别身份证-API
     *
     * @return array|Exception
     */
    public function identityCardApi($param = [])
    {
        AlibabaCloud::accessKeyClient($this->accessKeyId, $this->accessKeySecret)
            ->regionId('cn-shanghai')
            ->asDefaultClient();

        try {
            $request = Ocr::v20191230()->recognizeIdentityCard();
            $result = $request
                ->withSide($param['side'])
                ->withImageURL($param['image'])
                ->debug(false) // Enable the debug will output detailed information
                ->connectTimeout(1) // Throw an exception when Connection timeout
                ->timeout(1) // Throw an exception when timeout
                ->request();
            return $result->toArray();
        } catch (\Exception $exception) {
            trace("识别身份证错误:" . $exception->getMessage());
        }
        return [];
    }
}
