<?php
/**
 * 短信消息-模板
 */
namespace chb_external\aliyun;

use AlibabaCloud\SDK\Dysmsapi\V20170525\Dysmsapi;
use AlibabaCloud\SDK\Dysmsapi\V20170525\Models\AddSmsTemplateRequest;
use AlibabaCloud\SDK\Dysmsapi\V20170525\Models\DeleteSmsTemplateRequest;
use AlibabaCloud\SDK\Dysmsapi\V20170525\Models\ModifySmsTemplateRequest;
use AlibabaCloud\SDK\Dysmsapi\V20170525\Models\QuerySmsTemplateRequest;
use chb_lib\common\RedisToolTrait;
use chb_lib\common\ServiceTrait;
use Darabonba\OpenApi\Models\Config;

class SmsTemplate
{

    use RedisToolTrait, ServiceTrait;

    /**
     * 客户端
     *
     * @param array $param
     * @return object
     */
    public function getClient($param = [])
    {
        $config = new Config([
            // 您的AccessKey ID
            "accessKeyId" => $param['accessKeyId'],
            // 您的AccessKey Secret
            "accessKeySecret" => $param['accessKeySecret'],
        ]);
        // 访问的域名
        $config->endpoint = "dysmsapi.aliyuncs.com";
        return new Dysmsapi($config);
    }

    /**
     * 添加签名
     * TemplateType 来源:
     * 0：验证码。
     * 1：短信通知。
     * 2：推广短信。
     * 3：国际/港澳台消息。
     */
    public function addSmsTemplate($param = [])
    {
        $client = $this->getClient([
            "accessKeyId" => $param['param_config']['accessKeyId'] ?? '', // 您的AccessKey ID
            "accessKeySecret" => $param['param_config']['accessKeySecret'] ?? '', // 您的AccessKey Secret
        ]);
        $addSmsTemplateRequest = new AddSmsTemplateRequest([
            "templateType" => $param['param_config']['templateType'] ?? '',
            "templateName" => $param['param_config']['templateName'] ?? '',
            "templateContent" => $param['param_config']['templateContent'] ?? '',
            "remark" => $param['param_config']['remark'] ?? '',
        ]);
        // 复制代码运行请自行打印 API 的返回值
        $ret = $client->addSmsTemplate($addSmsTemplateRequest);
        return [
            'code' => 200,
            'msg' => $ret->body->code == 'OK' ? 'success' : 'fail',
            'result' => $ret->body,
        ];
    }

    /**
     * 查询
     */
    public function querySmsTemplate($param = [])
    {
        $client = $this->getClient([
            "accessKeyId" => $param['param_config']['accessKeyId'] ?? '', // 您的AccessKey ID
            "accessKeySecret" => $param['param_config']['accessKeySecret'] ?? '', // 您的AccessKey Secret
        ]);
        $querySmsTemplateRequest = new QuerySmsTemplateRequest([
            "templateCode" => $param['param_config']['templateCode'] ?? '',
        ]);
        // 复制代码运行请自行打印 API 的返回值
        $ret = $client->querySmsTemplate($querySmsTemplateRequest);
        return [
            'code' => 200,
            'msg' => $ret->body->code == 'OK' ? 'success' : 'fail',
            'result' => $ret->body,
        ];
    }

    /**
     * 删除
     */
    public function deleteSmsTemplate($param = [])
    {
        $client = $this->getClient([
            "accessKeyId" => $param['param_config']['accessKeyId'] ?? '', // 您的AccessKey ID
            "accessKeySecret" => $param['param_config']['accessKeySecret'] ?? '', // 您的AccessKey Secret
        ]);
        $deleteSmsTemplateRequest = new DeleteSmsTemplateRequest([
            "templateCode" => $param['param_config']['templateCode'] ?? '',
        ]);
        // 复制代码运行请自行打印 API 的返回值
        $ret = $client->deleteSmsTemplate($deleteSmsTemplateRequest);
        return [
            'code' => 200,
            'msg' => $ret->body->code == 'OK' ? 'success' : 'fail',
            'result' => $ret->body,
        ];
    }

    /**
     * 编辑
     */
    public function modifySmsTemplate($param = [])
    {
        $client = $this->getClient([
            "accessKeyId" => $param['param_config']['accessKeyId'] ?? '', // 您的AccessKey ID
            "accessKeySecret" => $param['param_config']['accessKeySecret'] ?? '', // 您的AccessKey Secret
        ]);
        $modifySmsTemplateRequest = new ModifySmsTemplateRequest([
            "templateType" => $param['param_config']['templateType'] ?? '',
            "templateName" => $param['param_config']['templateName'] ?? '',
            "templateContent" => $param['param_config']['templateContent'] ?? '',
            "remark" => $param['param_config']['remark'] ?? '',
            "templateCode" => $param['param_config']['templateCode'] ?? '',
        ]);
        // 复制代码运行请自行打印 API 的返回值
        $ret = $client->modifySmsTemplate($modifySmsTemplateRequest);
        return [
            'code' => 200,
            'msg' => $ret->body->code == 'OK' ? 'success' : 'fail',
            'result' => $ret->body,
        ];
    }

}
