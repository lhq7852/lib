<?php
/**
 * 消息-签名
 */
namespace chb_external\aliyun;

use AlibabaCloud\Client\AlibabaCloud;
use AlibabaCloud\SDK\Dysmsapi\V20170525\Dysmsapi;
use chb_lib\common\RedisToolTrait;
use chb_lib\common\ServiceTrait;
use Darabonba\OpenApi\Models\Config;

class EmailService
{

    use RedisToolTrait, ServiceTrait;

    /**
     * 客户端
     *
     * @param array $param
     * @return object
     */
    public function getClient($param = [])
    {
        $config = new Config([
            // 您的AccessKey ID
            "accessKeyId" => $param['accessKeyId'],
            // 您的AccessKey Secret
            "accessKeySecret" => $param['accessKeySecret'],
        ]);
        // 访问的域名
        $config->endpoint = "dysmsapi.aliyuncs.com";
        return new Dysmsapi($config);
    }

    /**
     * 发送邮件
     */
    public function sendEmail($param = [])
    {
        AlibabaCloud::accessKeyClient($param['param_config']['accessKeyId'] ?? '', $param['param_config']['accessKeySecret'] ?? '')
            ->regionId('cn-hangzhou')
            ->asDefaultClient();
        $result = AlibabaCloud::rpc()
            ->product('Dm')
            ->version('2015-11-23')
            ->action('SingleSendMail')
            ->method('POST')
            ->host('dm.aliyuncs.com')
            ->options([
                'query' => [
                    'AccountName' => $param['param_config']['accountName'] ?? "service@yisimu800.com",
                    'AddressType' => $param['param_config']['addressType'] ?? "0",
                    'ReplyToAddress' => $param['param_config']['replyToAddress'] ?? "false",
                    'ToAddress' => $param['param_config']['toAddress'] ?? "",
                    'Subject' => $param['param_config']['subject'] ?? "",
                    'HtmlBody' => $param['param_config']['htmlBody'] ?? "",
                    'TextBody' => $param['param_config']['textBody'] ?? "",
                    'FromAlias' => $param['param_config']['fromAlias'] ?? "匿名",
                    'ClickTrace' => $param['param_config']['clickTrace'] ?? "0",
                ],
            ])
            ->request()->toArray();
        return [
            'code' => 200,
            'msg' => !empty($result['EnvId']) ? 'success' : 'fail',
            'result' => $result,
        ];
    }
}
