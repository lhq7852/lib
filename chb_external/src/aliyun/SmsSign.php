<?php
/**
 * 消息-签名
 */
namespace chb_external\aliyun;

use AlibabaCloud\SDK\Dysmsapi\V20170525\Dysmsapi;
use AlibabaCloud\SDK\Dysmsapi\V20170525\Models\AddSmsSignRequest;
use AlibabaCloud\SDK\Dysmsapi\V20170525\Models\AddSmsSignRequest\signFileList;
use AlibabaCloud\SDK\Dysmsapi\V20170525\Models\DeleteSmsSignRequest;
use AlibabaCloud\SDK\Dysmsapi\V20170525\Models\ModifySmsSignRequest;
use AlibabaCloud\SDK\Dysmsapi\V20170525\Models\QuerySmsSignRequest;
use chb_lib\common\RedisToolTrait;
use chb_lib\common\ServiceTrait;
use Darabonba\OpenApi\Models\Config;

class SmsSign
{

    use RedisToolTrait, ServiceTrait;

    /**
     * 客户端
     *
     * @param array $param
     * @return object
     */
    public function getClient($param = [])
    {
        $config = new Config([
            // 您的AccessKey ID
            "accessKeyId" => $param['accessKeyId'],
            // 您的AccessKey Secret
            "accessKeySecret" => $param['accessKeySecret'],
        ]);
        // 访问的域名
        $config->endpoint = "dysmsapi.aliyuncs.com";
        return new Dysmsapi($config);
    }

    /**
     * 添加签名
     * signSource 来源:
     * 0：企事业单位的全称或简称。
     * 1：工信部备案网站的全称或简称。
     * 2：App应用的全称或简称。
     * 3：公众号或小程序的全称或简称。
     * 4：电商平台店铺名的全称或简称。
     * 5：商标名的全称或简称。
     */
    public function addSmsSign($param = [])
    {
        $client = $this->getClient([
            "accessKeyId" => $param['param_config']['accessKeyId'] ?? '', // 您的AccessKey ID
            "accessKeySecret" => $param['param_config']['accessKeySecret'] ?? '', // 您的AccessKey Secret
        ]);
        $arrObject = [];
        if (!empty($param['param_config']['signFileList'])) {
            foreach ($param['param_config']['signFileList'] as $v) {
                $arrObject[] = new signFileList([
                    "fileContents" => $v['fileContents'], //签名的资质证明文件经base64编码后的字符串。图片不超过2 MB。
                    "fileSuffix" => $v['fileSuffix'], //签名的证明文件格式，支持上传多张图片。当前支持JPG、PNG、GIF或JPEG格式的图片。
                ]);
            }
        }
        $addSmsSignRequest = new AddSmsSignRequest([
            "signName" => $param['param_config']['signName'] ?? '',
            "signSource" => $param['param_config']['signSource'] ?? '',
            "remark" => $param['param_config']['remark'] ?? '',
            "signFileList" => $arrObject,
        ]);
        // 复制代码运行请自行打印 API 的返回值
        $ret = $client->addSmsSign($addSmsSignRequest);
        return [
            'code' => 200,
            'msg' => $ret->body->code == 'OK' ? 'success' : 'fail',
            'result' => $ret->body,
        ];
    }

    /**
     * 查询
     */
    public function querySmsSign($param = [])
    {
        $client = $this->getClient([
            "accessKeyId" => $param['param_config']['accessKeyId'] ?? '', // 您的AccessKey ID
            "accessKeySecret" => $param['param_config']['accessKeySecret'] ?? '', // 您的AccessKey Secret
        ]);
        $querySmsSignRequest = new QuerySmsSignRequest([
            "signName" => $param['param_config']['signName'] ?? '',
        ]);
        // 复制代码运行请自行打印 API 的返回值
        $ret = $client->querySmsSign($querySmsSignRequest);
        return [
            'code' => 200,
            'msg' => $ret->body->code == 'OK' ? 'success' : 'fail',
            'result' => $ret->body,
        ];
    }

    /**
     * 删除
     */
    public function deleteSmsSign($param = [])
    {
        $client = $this->getClient([
            "accessKeyId" => $param['param_config']['accessKeyId'] ?? '', // 您的AccessKey ID
            "accessKeySecret" => $param['param_config']['accessKeySecret'] ?? '', // 您的AccessKey Secret
        ]);
        $deleteSmsSignRequest = new DeleteSmsSignRequest([
            "signName" => $param['param_config']['signName'] ?? '',
        ]);
        // 复制代码运行请自行打印 API 的返回值
        $ret = $client->deleteSmsSign($deleteSmsSignRequest);
        return [
            'code' => 200,
            'msg' => $ret->body->code == 'OK' ? 'success' : 'fail',
            'result' => $ret->body,
        ];
    }

    /**
     * 编辑
     */
    public function modifySmsSign($param = [])
    {
        $client = $this->getClient([
            "accessKeyId" => $param['param_config']['accessKeyId'] ?? '', // 您的AccessKey ID
            "accessKeySecret" => $param['param_config']['accessKeySecret'] ?? '', // 您的AccessKey Secret
        ]);
        $arrObject = [];
        if (!empty($param['param_config']['signFileList'])) {
            foreach ($param['param_config']['signFileList'] as $v) {
                $arrObject[] = new signFileList([
                    "fileContents" => $v['fileContents'], //签名的资质证明文件经base64编码后的字符串。图片不超过2 MB。
                    "fileSuffix" => $v['fileSuffix'], //签名的证明文件格式，支持上传多张图片。当前支持JPG、PNG、GIF或JPEG格式的图片。
                ]);
            }
        }
        $modifySmsSignRequest = new ModifySmsSignRequest([
            "signName" => $param['param_config']['signName'] ?? '',
            "signSource" => $param['param_config']['signSource'] ?? '',
            "remark" => $param['param_config']['remark'] ?? '',
            "signFileList" => $arrObject,
        ]);
        // 复制代码运行请自行打印 API 的返回值
        $ret = $client->modifySmsSign($modifySmsSignRequest);
        return [
            'code' => 200,
            'msg' => $ret->body->code == 'OK' ? 'success' : 'fail',
            'result' => $ret->body,
        ];
    }

}
