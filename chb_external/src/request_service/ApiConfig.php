<?php

namespace chb_external\request_service;

use chb_lib\common\exception\Err;
use chb_lib\common\RedisToolTrait;
use chb_lib\common\ServiceTrait;

class ApiConfig
{
    use RedisToolTrait, ServiceTrait;

    /**
     * 发起获取接口数据
     *
     * @return array
     */
    public function startGetData($param = [])
    {
        // TenantRequestLog::_checkAppId($param);
        $info = $this->getObject($param['alias']);
        $method = $param['method'];
        $class = new $info['main_class'];
        if (!method_exists($class, $method)) {
            Err::errorMsg(500, "接口对应的方法不存在,请查看外部数据资源");
        }
        $param = $this->headleParamConfig($param, $info);
        $ret = $class->$method($param);
        if (!empty($ret['is_api'])) {
            $this->editApiNum($param['alias']);
        }
        TenantRequestLog::_addData(array_merge($param, ['result' => $ret]));
        $ret = json_decode(json_encode($ret), true);
        return $ret['result'] ?? $ret;
    }

    /**
     * 处理请求参数,以配置的param_config字段为准,已外部传过来的优先
     *
     * @param [type] $param
     * @param [type] $info
     * @return array
     */
    protected function headleParamConfig($param, $info)
    {
        if (empty($info['param_config'])) {
            return [];
        }
        foreach ($info['param_config'] as $k => &$v) {
            $v = empty($param[$k]) ? $v : $param[$k];
        }
        $info['app_id'] = $param['app_id'] ?? ''; //租户app_id
        $info['client_user_no'] = $param['client_user_no'] ?? ''; //租户编号
        return $info;
    }

    /**
     * 获取接口对象命名空间
     *
     * @param string $alias
     * @return array|Excetpion
     */
    public function getObject($alias = '')
    {
        $info = $this->getDataInfo($alias, 'api_alias', false);
        if (empty($info)) {
            Err::errorMsg(500, "接口不存在,请查看外部数据资源");
        }
        if ($info['state'] != 1) {
            Err::errorMsg(500, "接口已关闭,请查看外部数据资源");
        }
        if ($info['api_num'] == 0) {
            Err::errorMsg(500, "接口剩余次数用完,请查看外部数据资源");
        }
        if (empty($info['main_class']) || !class_exists($info['main_class'])) {
            Err::errorMsg(500, "接口还没有对接,请查看外部数据资源");
        }
        $param = RequestConfig::_getRequestConfigInfo(['api_alias' => $alias]);
        return array_merge($info, $param);
    }

    /**
     * 更新接口使用次数
     *
     * @param string $alias
     * @return bool
     */
    public function editApiNum($alias = '')
    {
        return true;
        $info = $this->getDataInfo($alias, 'alias', false);
        if (empty($info)) {
            Err::errorMsg(500, "接口不存在,请查看外部数据资源");
        }
        if ($info['api_num'] > 0) {
            $this->editData([
                'id' => $info['id'],
                'api_num' => $info['api_num'] - 1,
            ]);
        }
        return true;
    }
}
