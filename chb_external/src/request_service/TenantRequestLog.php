<?php

namespace chb_external\request_service;

use chb_lib\common\exception\Err;
use chb_lib\common\RedisToolTrait;
use chb_lib\common\ServiceTrait;
use chb_platform\resource_client\ResourceClient;

class TenantRequestLog
{

    use RedisToolTrait, ServiceTrait;

    /**
     * 检查APP_id
     *
     * @return void
     */
    public function checkAppId($param = [])
    {
        if (empty($param['app_id']) || empty($param['client_user_no'])) {
            Err::errorMsg(410);
        }
        $user_no = ResourceClient::_getPlatformByAppId(['app_id' => $param['app_id']]);
        if ($param['client_user_no'] != $user_no) {
            Err::errorMsg(410);
        }
        return true;
    }

    /**
     * 添加日志
     *
     * @param array $param
     * @return bool
     */
    public function addData($param = [])
    {
        $data = [
            'user_no' => $param['client_user_no'] ?? '',
            'app_id' => $param['app_id'] ?? '',
            'api_alias' => $param['api_alias'] ?? '',
            'header' => empty($param['header_config']) ? '' : json_encode($param['header_config'], JSON_UNESCAPED_UNICODE),
            'request_param' => empty($param['param_config']) ? '' : json_encode($param['param_config'], JSON_UNESCAPED_UNICODE),
            'result_state' => $param['result']['msg'] ?? '',
            'remark' => empty($param['result']) ? '' : json_encode($param['result'], JSON_UNESCAPED_UNICODE),
        ];
        $this->getModel()->addData($data);
        return true;
    }

}
