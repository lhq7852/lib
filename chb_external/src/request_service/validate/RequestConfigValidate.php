<?php

/**
 * 验证
 */

namespace chb_external\request_service\validate;

use chb_lib\common\ValidateTrait;
use think\Validate;

class RequestConfigValidate extends Validate
{
    use ValidateTrait;

    protected $being = "request_alias"; //唯一字段检查

    protected $rule = [
        'request_name' => 'require|max:200',
        'request_alias' => 'checkBeing',
    ];
}
