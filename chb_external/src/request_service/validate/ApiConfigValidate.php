<?php

/**
 * 验证
 */

namespace chb_external\request_service\validate;

use chb_lib\common\ValidateTrait;
use think\Validate;

class ApiConfigValidate extends Validate
{
    use ValidateTrait;

    protected $being = "api_alias"; //唯一字段检查

    protected $rule = [
        'api_name' => 'require|max:200',
        'api_alias' => 'checkBeing',
    ];
}
