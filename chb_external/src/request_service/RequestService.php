<?php

namespace chb_external\request_service;

use chb_lib\common\exception\Err;
use chb_lib\common\RedisToolTrait;
use chb_lib\common\ServiceTrait;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\RequestException;

class RequestService
{

    use RedisToolTrait, ServiceTrait;

    protected $method = "get";
    protected $headers = [];
    protected $param = [];
    protected $requestUrl = [];
    protected $returnField = '';

    /**
     * 设置请求地址
     *
     * @param [type] $url
     * @return object
     */
    public function setUrl($url)
    {
        $this->requestUrl = $url;
        return $this;
    }

    /**
     * 设置请求方法: get / post
     *
     * @param [type] $method
     * @return object
     */
    public function setMethod($method)
    {
        $this->method = strtolower($method);
        return $this;
    }

    /**
     * 设置请求头参数
     *
     * @return object
     */
    public function setHeaderConfig($config = [])
    {
        $this->headers = $config;
        return $this;
    }

    /**
     * 设置请求参数
     *
     * @return object
     */
    public function setParamConfig($config = [])
    {
        $this->param = $config;
        return $this;
    }

    /**
     * 设置请求参数
     *
     * @return object
     */
    public function setReturnField($returnField = '')
    {
        $this->returnField = $returnField;
        return $this;
    }

    /**
     * 发送请求
     *
     * @return void|array
     */
    public function request()
    {
        if (!in_array($this->method, ['get', 'post'])) {
            Err::errorMsg(800);
        }
        try {
            trace("request===", "chb_error");
            trace("requestUrl===" . $this->requestUrl, "chb_error");
            trace("method===" . $this->method, "chb_error");
            trace($this->param, "chb_error");

            $obj = new Client();
            $ret = $obj->request($this->method, $this->requestUrl, [
                'headers' => empty($this->headers) ? [] : $this->headers, //请求头
                'form_params' => empty($this->param) && $this->method != 'post' ? [] : $this->param, //post表单提交
                'query' => empty($this->param) && $this->method != 'get' ? [] : $this->param, //get参数
                'json' => empty($this->param) && $this->method != 'post' ? [] : $this->param, //json参数
            ]);
            if ($ret->getStatusCode() != 200) {
                return [];
            }
            $result = (string) $ret->getBody();
            $arr = json_decode($result, true);
            if (!empty($this->returnField) && isset($arr[$this->returnField])) {
                return $arr[$this->returnField];
            }
            return $arr;
        } catch (RequestException $e) {
            $result = (string) $e->getResponse()->getBody();
            $arr = json_decode($result, true);
            return $arr;
        }
    }

    public function aliRequest()
    {

    }

}
