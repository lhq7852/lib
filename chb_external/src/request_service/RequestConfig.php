<?php

namespace chb_external\request_service;

use chb_lib\common\RedisToolTrait;
use chb_lib\common\ServiceTrait;

class RequestConfig
{

    use RedisToolTrait, ServiceTrait;

    /**
     * 获取信息
     *
     * @return void|array
     */
    public function getRequestConfigInfo($param = [])
    {
        $info = $this->getInfo(['api_alias' => $param['api_alias']], false);
        if (empty($info)) {
            return [];
        }
        $info['header_config'] = !empty($info['header_config']) ? json_decode($info['header_config'], true) : [];
        $info['param_config'] = !empty($info['param_config']) ? json_decode($info['param_config'], true) : [];
        return $info;
    }

    /**
     * 发送请求
     *
     * @param array $param
     * @return void|array
     */
    public function sendRuqest($param = [])
    {
        $data = RequestService::_setUrl($param['request_url'])->setMethod($param['mode'])
            ->setHeaderConfig(array_merge($param['header_config'], []))
            ->setParamConfig($param['param_config'])
            ->setReturnField($param['return_field'])
            ->request();
        return [
            'code' => '200',
            'msg' => !empty($data) ? 'success' : "fail",
            "result" => $data,
        ];
    }
}
