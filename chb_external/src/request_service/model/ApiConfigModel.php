<?php

/**
 * 模型
 */

namespace chb_external\request_service\model;

use chb_lib\common\BaseModel;

class ApiConfigModel extends BaseModel
{

    protected $name = 'api_config';
    protected $pk = 'id';

    protected $hidden = ['delete_time'];
    protected $likeList = ["keyword" => "api_name"]; //设置模糊搜索映射的字段 alias|value

}
