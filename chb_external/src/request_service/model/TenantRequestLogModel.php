<?php

/**
 * 模型
 */

namespace chb_external\request_service\model;

use chb_lib\common\BaseModel;

class TenantRequestLogModel extends BaseModel
{

    protected $name = 'tenant_request_log';
    protected $pk = 'id';

    protected $hidden = ['delete_time'];
    protected $likeList = ["keyword" => "api_alias"]; //设置模糊搜索映射的字段 alias|value

    /**
     * 请求头参数
     *
     * @param [type] $value
     * @return void|array
     */
    public function getHeaderAttr($value)
    {
        $arr = json_decode($value, true);
        return empty($arr) ? $value : $arr;
    }

    /**
     * 请求参数
     *
     * @param [type] $value
     * @return void|array
     */
    public function getRequestParamAttr($value)
    {
        $arr = json_decode($value, true);
        return empty($arr) ? $value : $arr;
    }
}
