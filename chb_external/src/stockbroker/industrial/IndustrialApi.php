<?php
/**
 * 活体
 */
namespace chb_external\stockbroker\industrial;

use chb_external\request_service\ApiConfig;
use chb_external\request_service\RequestService;

class IndustrialApi
{
    protected $username = "P106663999";
    protected $password = "yEz7zKbg8R0Ut2cRviYjRA==";
    protected $host = "https://glr.xyzq.cn:8433/pposapi";
    

    /**
     * 获取授权码
     */
    public function getAuthCode($param = [])
    {
        $param['request_url'] = $this->host . $param['request_url'];
        $param['param_config']['redirect_uri'] = $this->host;
        $param['param_config']['client_id'] = $param['param_config']['username'] = $this->username;
        $param['param_config']['password'] = $this->password;
        $param['param_config']['servername'] = "GetApiAuthCode";
        $data = $this->sendRuqest($param);
        if (!empty($data['result']['code'])) {
            return $data['result']['code'];
        }
        return "";
    }

    /**
     * 获取令牌
     *
     */
    public function getToken($param = [])
    {
        $param['param_config']['code'] = ApiConfig::_startGetData(['method' => "getAuthCode", "alias" => "industrial_auth_code"]);
        $param['request_url'] = $this->host;
        $param['param_config']['redirect_uri'] = $this->host;
        $param['param_config']['client_id'] = $this->username;
        $param['param_config']['client_secret'] = $this->password;
        $param['param_config']['servername'] = "GetApiToken";
        $data = $this->sendRuqest($param);
        print_r($data);
        print_r($param);exit;
        if (!empty($data['result']['code'])) {
            return $data['result']['code'];
        }
        return "";
    }

    /**
     * 发送请求
     *
     * @param array $param
     * @return void|array
     */
    public function sendRuqest($param = [])
    {
        $data = RequestService::_setUrl($param['request_url'])->setMethod($param['mode'])
            ->setHeaderConfig(array_merge($param['header_config'] ?? [], []))
            ->setParamConfig($param['param_config'])
            ->setReturnField($param['return_field'])
            ->request();
        return [
            'code' => !empty($data['errcode']) ? "500" : '200',
            'msg' => !empty($data['errcode']) ? "fail" : 'success',
            "result" => $data,
        ];
    }
}
