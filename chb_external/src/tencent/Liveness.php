<?php
/**
 * 活体
 */
namespace chb_external\tencent;

use chb_lib\common\SingleTrait;
use TencentCloud\Common\Credential;
use TencentCloud\Common\Profile\ClientProfile;
use TencentCloud\Common\Profile\HttpProfile;
use TencentCloud\Faceid\V20180301\FaceidClient;
use TencentCloud\Faceid\V20180301\Models\LivenessCompareRequest;

class Liveness
{
    use SingleTrait;

    /**
     * 活体比较
     *
     * @return void
     */
    public function compareRequest($param = [])
    {
        try {
            $secretId = $param['param_config']['secretId'];
            $secretKey = $param['param_config']['secretKey'];
            $endpoint = $param['param_config']['endpoint'];
            $region = $param['param_config']['region'];

            $imageUrl = $param['param_config']['imageUrl'];
            $videoUrl = $param['param_config']['videoUrl'];
            $livenessType = $param['param_config']['livenessType'];
            $validateData = $param['param_config']['validateData'];

            $cred = new Credential($secretId, $secretKey);
            $httpProfile = new HttpProfile();
            $httpProfile->setEndpoint($endpoint);

            $clientProfile = new ClientProfile();
            $clientProfile->setHttpProfile($httpProfile);
            $client = new FaceidClient($cred, $region, $clientProfile);

            $req = new LivenessCompareRequest();

            $params = array(
                "ImageUrl" => $imageUrl,
                "LivenessType" => $livenessType,
                "ValidateData" => $validateData,
                "VideoUrl" => $videoUrl,
            );
            $req->fromJsonString(json_encode($params));
            $resp = $client->LivenessCompare($req);
            $result = json_decode($resp->toJsonString(), true);
            if (!empty($result['BestFrameBase64'])) { //字段太长,过滤掉
                unset($result['BestFrameBase64']);
            }
            return [
                'code' => 200,
                'msg' => $result['Result'] == 'Success' ? 'success' : 'fail',
                'result' => $result,
            ];
        } catch (\Exception $e) {
            return [
                'code' => 200,
                'msg' => 'fail',
                'result' => $e->getMessage(),
            ];
        }
    }

}
