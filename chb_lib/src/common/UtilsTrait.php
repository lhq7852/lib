<?php

/**
 * 通用工具
 */

namespace chb_lib\common;

use chb_lib\common\exception\Err;
use chb_lib\common\utils\Pinyin;

trait UtilsTrait
{
    /**
     * 将数组转换为树状结构数组
     *
     * @param $data 原始数组
     * @param $parent_id 最外层父级id
     * @param $primary_field 主键字段名
     * @param string $parent_field 父级id字段名
     * @param string $children_field 存放子节点的字段名
     * @return array
     */
    public function buildTree($data, $parent_id, $primary_field, $parent_field = 'parent_id', $children_field = 'children', $level = -1)
    {
        $data_tree = array();
        $level++;

        foreach ($data as $key => $value) {
            if ($value[$parent_field] == $parent_id) {
                $children = $this->buildTree($data, $value[$primary_field], $primary_field, $parent_field, $children_field, $level);
                $value['level'] = $level;

                if ($children) {
                    $value[$children_field] = $children;
                }

                $data_tree[] = $value;
            }
        }

        return $data_tree;
    }
    /**
     * 获取一个节点的所有子节点
     * @param $data_tree
     * @param $data
     * @param $parent_id
     * @param $primary_field
     * @param string $parent_field
     */
    public function getChildrens(&$data_tree, $data, $parent_id, $primary_field, $parent_field = 'parent_id')
    {
        foreach ($data as $key => $value) {
            if ($value[$parent_field] == $parent_id) {
                $this->getChildrens($data_tree, $data, $value[$primary_field], $primary_field, $parent_field);
                $data_tree[] = $value;
            }
        }
    }

    /**
     * @param $delimiter 分割符
     * @param $str 要分割的字符串
     * @return array|false|string[]
     */
    public function getExplode($delimiter, $str)
    {
        if (!strlen($str)) {
            return array();
        }

        return explode($delimiter, $str);
    }

    /**
     * 合并数组，去重
     *
     * @param $data1
     * @param $data2
     * @return array
     */
    public function getMerge($data1, $data2)
    {
        return array_values(array_unique(array_merge($data1, $data2)));
    }

    public function implodeIds($data)
    {
        if ($data) {
            $first = reset($data);
            $end = end($data);

            if ($first !== '') {
                array_unshift($data, '');
            }

            if ($end !== '') {
                array_push($data, '');
            }

            return implode(',', $data);
        } else {
            return '';
        }
    }

    public function checkIdInIds($id, $ids)
    {
        return (strpos($ids, $this->implodeIds([$id])) === false) ? false : true;
    }

    /**
     * 检查字符长度
     */
    public function checkStringLong($str = '', $min = 6, $max = 18)
    {
        if (empty($str)) {
            return false;
        }
        if (mb_strlen($str) < $min) {
            return false;
        }
        if (mb_strlen($str) > $max) {
            return false;
        }
        return true;
    }

    /**
     * 字符串-中文转拼音
     *
     * @param string $str
     * @return string
     */
    public function mb_pinyin($str = '')
    {
        $txt = iconv('utf-8', 'gbk', $str);
        return (new Pinyin)->getAllPY($txt);
    }

    /**
     * 获取二维数组中某个键的值,不存在返回默认值
     *
     * @param [type] $data
     * @param [type] $key
     * @param [type] $field_name
     * @param string $default
     * @return string
     */
    public function getTwoArrayField($data, $key, $field_name, $default = '')
    {
        if (isset($data[$key]) && isset($data[$key][$field_name])) {
            return $data[$key][$field_name];
        } else {
            return $default;
        }
    }

    /**
     * 手机号码正则脱敏
     *
     * @param [type] $mobile
     * @return string
     */
    public function mobileRegular($mobile = '')
    {
        if (empty($mobile)) {
            return '';
        }

        return preg_replace("/(\d{3})\d\d(\d{2})/", "\$1****\$3", $mobile);
    }

    /**
     * 邮箱正则脱敏
     *
     * @param [type] $email
     * @return string
     */
    public function emailRegular($email = '')
    {
        if (empty($email)) {
            return '';
        }

        return preg_replace("/(^[_a-z0-9-]+(\.[_a-z0-9-]+)*@)([a-z0-9-]+(\.[a-z0-9-]+)*)(\.[a-z]{2,})$/", "\$1***\$5", $email);
    }

    /**
     * 手机正则
     *
     * @param string $mobile
     * @return bool
     */
    public function mobileVerify($mobile = '')
    {
        if (empty($mobile)) {
            return '';
        }
        if (strlen($mobile) == 1) {
            return preg_match("/^1[3-9]\d{9}$/", $mobile);
        }
        return is_numeric($mobile);
    }

    /**
     * 邮箱正则
     *
     * @param string $email
     * @return bool
     */
    public function emailVerify($email = '')
    {
        if (empty($email)) {
            return '';
        }

        return preg_match("/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,})$/", $email);
    }

    /**
     * 获取数组的某一列,并去重+去空值
     *
     * @param array $array
     * @param string|null $column
     * @return array
     */
    public function columnFormat(array $array = [], string $column = null)
    {
        if (empty($array) || empty($column)) {
            return [];
        }
        return array_values(array_unique(array_filter(array_column($array, $column))));
    }

    /**
     * 归类数据到同一个key下面
     *
     * @param array $array
     * @param string $key
     * @return array
     */
    public function array_column_key($array = [], $key = '', $default = true)
    {
        if (empty($array) || empty($key)) {
            return $array;
        }
        $data = [];
        foreach ($array as $k => $v) {
            if (!isset($v[$key])) {
                throw new \Exception("键值" . $key . "不存在");
            }
            if ($default) {
                $data[$v[$key]] = $v;
            } else {
                $data[$v[$key]][] = $v;
            }
        }
        return $data;
    }

    /**
     * 比较两个一维数组（value集合），得出交集、差集
     * 应用场景：更新数据时，找出新数组-数据库中的旧数组的差集（添加）、旧数组-新数组的差集（删除）
     * @param array $new
     * @param array $old
     * @return array
     */
    public function compareArrayDiff(array $new = [], array $old = [])
    {
        return [
            'add' => array_values(array_diff($new, $old)),
            'same' => array_intersect($new, $old),
            'delete' => array_diff($old, $new),
        ];
    }

    /**
     * 检查数组的维数: 一维 或者 二位[以上]
     * @param array|string $array
     * @return int
     */
    public function checkArrayDepth($array)
    {
        if (!is_array($array)) {
            return 0;
        }

        $i = 0;
        foreach ($array as $item) {
            if (is_array($item)) {
                $i = 2;
                break;
            }
            $i = 1;
        }
        return $i;
    }

    /**
     * 搜索替换
     * 按数组键搜索字符串是否匹配,如果匹配,则替换数组的值
     *
     * @param [type] $string
     * @param array $array
     * @return void
     */
    public function searchReplace($string = '', $array = [])
    {
        if (empty($array)) {
            return $string;
        }
        foreach ($array as $field => $txt) {
            if (strchr($string, $field)) {
                $string = str_replace($field, $txt, $string);
                break;
            }
        }
        return $string;
    }

    /**
     * 随机字符串
     *
     * @param integer $length
     * @return string
     */
    public function getNonceStr($length = 8)
    {
        $str = md5($this->createUid());
        $token = substr($str, 5, $length);
        return $token;
    }

    /**
     * 唯一uid
     * 18位
     *
     * @return string
     */
    public function createUid()
    {
        $init = "20210219";
        $time = date("Ymd");
        $micTime = microtime(true);
        $uid = rand(100, 999) . ($time - $init) . date("His") . (explode(".", $micTime)[1] ?? '0000') . rand(10, 99);
        return $uid;
    }

    //驼峰命名转下划线命名
    public function toUnderScore($str)
    {
        $dstr = preg_replace_callback('/([A-Z]+)/', function ($matchs) {
            return '_' . strtolower($matchs[0]);
        }, $str);
        return trim(preg_replace('/_{2,}/', '_', $dstr), '_');
    }

    //下划线命名到驼峰命名
    public function toCamelCase($str)
    {
        $array = explode('_', $str);
        $result = $array[0];
        $len = count($array);
        if ($len > 1) {
            for ($i = 1; $i < $len; $i++) {
                $result .= ucfirst($array[$i]);
            }
        }
        return $result;
    }

    //替换特殊字符中间的字符串
    public function content_replace($content, $param = [], $str = "", $str2 = "", $default = false)
    {
        // preg_match_all("/\{\{.+?\}\}/", $content, $arr);
        preg_match_all("/" . $str . ".+?" . $str2 . "/", $content, $arr);
        if (empty($arr) || empty($arr[0])) {
            return $content;
        }
        $arrField = [];
        foreach ($arr[0] as $v) {
            $field = str_replace($str2, "", str_replace($str, "", strip_tags($v)));
            if (isset($param[$field])) {
                $content = str_replace($v, $param[$field], $content);
                $arrField[$field] = $param[$field];
            }
        }
        if (!$default) {
            return $content;
        }
        return [
            'field' => $arrField,
            'content' => $content,
        ];
    }

    /**
     * 获取特殊字符中间的字符串
     *
     * @param [type] $content {ddd}
     * @param string $str {
     * @param string $str2 }
     * @return string
     */
    public function get_content($content = '', $str = "", $str2 = "")
    {
        if (!strchr($content, $str) || !strchr($content, $str2)) {
            return $content;
        }
        $arr = explode($str, $content);
        $arr = explode($str2, $arr[1] ?? $content);
        return $arr[0] ?? $content;
    }

    /**
     * 检查判断-分割字符串
     *
     * @param [type] string $separator
     * @param [string] string $param
     * @return array
     */
    public function explodeData($separator, $param)
    {
        if (empty($param)) {
            return [];
        }
        if (is_array($param)) {
            return $param;
        }
        $data = explode($separator, $param);
        return array_filter(array_unique($data));
    }

    /**
     * 获取HTTP_ORIGIN
     *
     * @return string
     */
    public function getHostOrigin()
    {
        return preg_replace("/(http|https):\/\//", '', $_SERVER['HTTP_ORIGIN']);
    }

    /**
     * 加解密
     *
     * @param [type] $string 加密的数据
     * @param [type] $key 加密的key
     * @param [type] $decrypt 0 加密 1 解密
     * @return void|string
     */
    public function encryptDecrypt($string, $key, $decrypt, $mode = "DES-ECB")
    {
        if ($decrypt) {
            $decrypted = openssl_decrypt($string, $mode, $key);
            return $decrypted;
        } else {
            $encrypted = openssl_encrypt($string, $mode, $key);
            return urlencode($encrypted);
        }
    }

    /**
     * 获取加密key的算法1
     *
     * @param string $key
     * @return void
     */
    public function encrypt1($key = '', $time = null)
    {
        if (empty($key)) {
            return md5(md5("encrypt1"));
        }
        $time = !empty($time) ? (is_numeric($time) ? $time : strtotime($time)) : request()->time();
        $h = (int) date("H", $time);
        $s = (int) date("s", $time) ?: 61;
        $str = substr($key, $h, $s);
        return [
            'time' => $time,
            'private_key' => md5(md5($str)),
        ];
    }

    /**
     * 获取并处理传入系统的参数
     *
     * @return array
     */
    public function getParam()
    {
        $param = request()->param('');
        foreach ($param as &$v) { // 防止 eval 方法注入
            if (is_string($v) && (strchr($v, '@eval(') || strchr($v, '<?php '))) {
                Err::errorMsg(500);
            }
            $v = is_string($v) ? trim($v) : $v; //过滤前后空格
        }
        if (isset($param['s'])) {
            unset($param['s']);
        }
        $param['login_organization_no'] = request()->user['organization_no'] ?? 0;
        $param['login_user_no'] = request()->user['user_no'] ?? 0;
        $param['login_account_no'] = request()->user['account_no'] ?? 0;
        $param['login_user_no'] = request()->user['user_no'] ?? 0;
        if (!empty($this->server)) {
            $this->server::_setParam($param); //传参进 业务层
        }
        return $param;
    }

    /**
     * json + 加密输出
     *
     * @param [type] $param
     * @return array
     */
    public function jsonEncrypt($param, $public_key, $time = null)
    {
        $ret = $this->encrypt1($public_key, $time);
        $data = $this->encryptDecrypt(json_encode($param), $ret['private_key'], 0);
        return [
            'time' => $ret['time'],
            'ip' => request()->ip(),
            'data' => $data,
        ];
    }

    /**
     * 解析json + 解密输出
     *
     * @param [type] $param
     * @return array
     */
    public function jsonDecrypt($param, $public_key, $time = null)
    {
        $ret = $this->encrypt1($public_key, $time);
        $str = $this->encryptDecrypt(urldecode($param), $ret['private_key'], 1);
        $data = json_decode($str, true);
        return [
            'time' => $ret['time'],
            'data' => $data,
        ];
    }

    /**
     * 服务器ip-内网地址
     *
     * @return string
     */
    protected function getServerIp()
    {
        if (strtoupper(PHP_OS) == 'LINUX') {
            return exec("ip a | grep 'inet' | grep -v inet6 | grep -v 127* | grep -v .0.1/* | awk '{print $2}'|awk -F '/' '{print $1}'");
        }
        return '127.0.0.1';
    }

    /**
     * 服务器ip-外网地址
     *
     * @return string
     */
    protected function getServerPublicIp()
    {
        if (strtoupper(PHP_OS) == 'LINUX') {
            return shell_exec("curl ifconfig.me");
        }
        return '127.0.0.1';
    }

    /**
     * 获取文件大小
     * 单位: Bytes
     */
    protected function getFileSize($url, $default = true)
    {
        if (empty($url)) {
            return 0;
        }
        $file = file_get_contents($url);
        if (!$default) {
            $file = chunk_split(base64_encode($file));
        }
        return strlen($file);
    }

    /**
     * 获取图片base64格式
     */
    protected function getImageBase64($image = '')
    {
        if (empty($image)) {
            return '';
        }
        $file = file_get_contents($image);
        return chunk_split(base64_encode($file));
    }

    /**
     * 获取图片 后缀 | 类型
     */
    protected function getImageType($image = '')
    {
        if (empty($image)) {
            return '';
        }
        $arrType = explode(".", $image);
        return end($arrType);
    }

    /**
     * 获取客户端唯一标识
     */
    public function getSessionId()
    {
        if (!empty(session_id())) {
            return session_id();
        }
        return md5($_SERVER['HTTP_USER_AGENT'] . request()->ip());
    }
}
