<?php
namespace chb_lib\common\system;

use chb_lib\common\RedisToolTrait;
use chb_lib\common\ServiceTrait;

class SystemInfo
{
    use RedisToolTrait, ServiceTrait;

    public function __construct()
    {
        $this->redisKey = "SystemInfo:";
    }
    
    /**
     * 键值对获取
     * 示例
     * @param [type] $alias
     * @return array
     */
    public function getKeyConfig($alias)
    {
        $info = $this->getDataInfo($alias, 'alias', false);
        if (empty($info)) {
            return '';
        }
        if (is_array($info['value'])) {
            return $info['value'];
        } 
        try {
            $value = json_decode($info['value'], true);
            if (is_array($value)) {
                return $value;
            }
            return $info['value'] ?? '';
        } catch (\Exception $e) {
        }
        return $info['value'] ?? '';
    }
}
