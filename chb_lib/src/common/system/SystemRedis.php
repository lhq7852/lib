<?php

namespace chb_lib\common\system;

use chb_lib\common\ServiceTrait;
use chb_lib\common\exception\Err;
use chb_lib\common\RedisToolTrait;
use think\facade\Cache;
use think\facade\Config;

class SystemRedis
{

    use RedisToolTrait, ServiceTrait;

    public function __construct()
    {
        $this->redisKey = "System:Redis:";
        $this->cacheConfig = Config::get("cache") ?? [];
    }

    /**
     * redis数据库配置
     *
     * @param [type] $alias
     * @return array
     */
    public function getRedisConfig($redis_key)
    {
        if (empty($this->cacheConfig['stores']) || empty($this->cacheConfig['stores']['redis'])) {
            Err::errorMsg(460);
        }
        $config['cache_config'] = $this->cacheConfig;
        if ($this->redisKey == $redis_key) {
            return $config;
        }
        try {
            $info = $this->getSystemRedis($redis_key);
            if (empty($info)) {
                return $config;
            }
            $config['redis_config'] = $info;
            return $config;
        } catch (\Exception $e) {
        }
        return $config;
    }

    protected function getSystemRedis($redis_key)
    {
        $objRedis = Cache::store("redis")->handler();
        $objRedis->select(15);
        $redisKey = $this->getRedisKayName() . str_replace(":", "_", $redis_key);
        $ret = $objRedis->get($redisKey);
        if (!empty($ret)) {
            return json_decode($ret, true);
        }
        $info = $this->getModel()->where(['redis_key' => $redis_key])->find();
        if (empty($info)) {
            return [];
        }
        $objRedis->setex($redisKey, $this->redisTime, json_encode($info));
        return $info;
    }
}
