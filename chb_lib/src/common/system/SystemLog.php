<?php

/**
 * 系统日志
 */

namespace chb_lib\common\system;

use chb_lib\common\ServiceTrait;
use chb_lib\common\exception\Err;
use chb_lib\common\RedisToolTrait;
use think\facade\Queue;

class SystemLog
{
    use ServiceTrait, RedisToolTrait;

    protected $model;

    public function __construct()
    {
        $this->redisKey = "System:Log:";
    }

    public function getPageList($param = [])
    {
        return $this->getModel()->getPageList($param);
    }

    public function getInfo($log_id)
    {
        $info = $this->getDataInfo($log_id);
        $info['header'] = json_decode($info['header'], true);
        $info['parameter'] = !empty($info['parameter']) ? json_decode($info['parameter'], true) : '';
        $info['error'] = !empty($info['error']) ? json_decode($info['error'], true) : '';
        return $info;
    }

    /**
     * 加入队列
     *
     * @param array $param
     * @return void
     */
    public function addLogQueue($param = [])
    {
        $data = [
            'client_ip' => $param['client_ip'] ?? request()->ip(),
            'type' => $param['type'] ?? 'system',
            'domain' => $param['domain'] ?? request()->domain(),
            'environment' => $param['environment'] ?? ($_SERVER['ENV'] ?? 'produce'),
            'link_url' => $param['link_url'] ?? request()->baseUrl(true),
            'request' => $param['request'] ?? request()->method(),
            'header' => $param['header'] ?? request()->header(),
            'parameter' => $param['parameter'] ?? request()->param(),
            'api' => $param['api'] ?? request()->param('api', ''),
            'code' => $param['code'] ?? '',
            'error_msg' => $param['msg'] ?? ($param['error_msg'] ?? ''),
            'error' => isset($param['code']) && $param['code'] != 200 ? Err::$errData : '',
        ];
        Queue::push("chb_lib\common\job\LogJob@addLog", $data, 'system:add_log');
    }

    /**
     * 记录日志
     *
     * @param array $param
     * @return void
     */
    public function addLog($param = [])
    {
        $data = [
            'client_ip' => $param['client_ip'],
            'header' => json_encode($param['header']),
            'type' => $param['type'] ?? 'user',
            'domain' => $param['domain'],
            'environment' => $param['environment'],
            'request' => $param['request'],
            'parameter' => json_encode($param['parameter']),
            'link_url' => $param['link_url'] ?? '',
            'api' => $param['api'],
            'code' => $param['code'],
            'error_msg' => !isset($param['error_msg']) ? '' : (is_array($param['error_msg']) ? json_encode($param['error_msg']) : $param['error_msg']),
            'error' => json_encode($param['error']),
        ];
        $this->getModel()->insert($data);
        return true;
    }

    /**
     * 定时删日志
     *
     * @return void
     */
    public function delLog()
    {
        $day = date("Y-m-d 00:00:00", strtotime("-15 day"));
        $this->getModel()->where("create_time", "<", $day)->where("code", 200)->delete();
        return true;
    }

    /**
     * 一键删日志
     * @param int $type
     * @return bool
     */
    public function oneClickClearQueue($type = 0)
    {
        if ($type == 1) {
            $this->getModel()->where("code", '200')->delete();
        }
        if ($type == 2) {
            $this->getModel()->where("code", '<>', '200')->delete();
        }
        return true;
    }
}
