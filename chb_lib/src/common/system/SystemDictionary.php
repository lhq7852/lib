<?php

namespace chb_lib\common\system;

use chb_lib\common\ServiceTrait;
use chb_lib\common\RedisToolTrait;

class SystemDictionary
{

    use RedisToolTrait, ServiceTrait;

    public function __construct()
    {
        $this->redisKey = "System:Dictionary:";
    }

}
