<?php

/**
 * 系统配置-redis模型
 */

namespace chb_lib\common\system\model;

use chb_lib\common\BaseModel;

class SystemRedisModel extends BaseModel
{

    protected $name = 'system_redis';
    protected $pk = 'id';
    protected $alias = 's_r';

    protected $likeList = ["keyword" => "redis_key|remark"]; //设置模糊搜索映射的字段 alias|value

}
