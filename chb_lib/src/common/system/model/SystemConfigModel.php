<?php

/**
 * 系统配置参数模型
 */

namespace chb_lib\common\system\model;

use chb_lib\common\BaseModel;

class SystemConfigModel extends BaseModel
{

    protected $name = "system_config";
    protected $pk = 'id';
    protected $alias = 's_c';

    protected $likeList = ["keyword" => "alias|remark"]; //设置模糊搜索映射的字段 alias|value

    /**
     * 格式化json数据
     *
     * @param [stirng] stirng  $value
     * @return array|string
     */
    protected function getValueAttr($value)
    {
        $json = json_decode($value, true);
        return empty($json) ? $value : $json;
    }
}
