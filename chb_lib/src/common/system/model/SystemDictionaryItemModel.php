<?php

/**
 * 模型
 */

namespace chb_lib\common\system\model;

use chb_lib\common\BaseModel;

class SystemDictionaryItemModel extends BaseModel
{

    protected $name = 'system_dictionary_item';
    protected $pk = 'id';
    protected $alias = 's_d_i';

    protected $likeList = ["keyword" => "tag_name|alias"]; //设置模糊搜索映射的字段 alias|value
}
