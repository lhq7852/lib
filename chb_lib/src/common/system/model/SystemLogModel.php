<?php

namespace chb_lib\common\system\model;

use chb_lib\common\BaseModel;

class SystemLogModel extends BaseModel
{

    protected $connection = 'log_database';

    protected $name = 'system_log';
    protected $pk = 'log_id';
    protected $alias = 's_l';

    protected $likeList = ["keyword" => "error_msg"]; //设置模糊搜索映射的字段 alias|value

    public function getPageList($param = [])
    {
        $objWhere = $this->field("client_ip,domain,environment,request,api,code,log_id,create_time,error_msg")->order("log_id", "desc");
        if (!empty($param['type']) && $param['type'] == 'err') {
            $objWhere = $objWhere->where("code", "<>", "200");
        }
        $list = $objWhere->paginate(50);
        return $list;
    }
}
