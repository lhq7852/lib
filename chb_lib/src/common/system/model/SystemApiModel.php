<?php

/**
 * 模型
 */

namespace chb_lib\common\system\model;

use chb_lib\common\BaseModel;

class SystemApiModel extends BaseModel
{

    protected $name = 'system_api';
    protected $pk = 'system_api_id';
    protected $alias = 's_a';

    protected $likeList = ["keyword" => "api_name"]; //设置模糊搜索映射的字段 alias|value

}
