<?php

/**
 * 模型
 */

namespace chb_lib\common\system\model;

use chb_lib\common\BaseModel;

class SystemDictionaryModel extends BaseModel
{

    protected $name = 'system_dictionary';
    protected $pk = 'id';
    protected $alias = 's_d';

    protected $likeList = ["keyword" => "key_name|alias"]; //设置模糊搜索映射的字段 alias|value

}
