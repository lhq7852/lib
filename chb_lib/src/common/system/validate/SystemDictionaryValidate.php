<?php

/**
 * 
 */

namespace chb_lib\common\system\validate;

use chb_lib\common\ValidateTrait;
use think\Validate;

class SystemDictionaryValidate  extends Validate
{
    use ValidateTrait;

    protected $being = "alias"; //唯一字段检查

    protected $rule = [
        'alias' => 'require|max:200|checkBeing',
        'remark' => 'max:5000',
    ];
}
