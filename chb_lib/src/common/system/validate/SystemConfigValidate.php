<?php

/**
 * 系统配置参数检验
 */

namespace chb_lib\common\system\validate;

use chb_lib\common\ValidateTrait;
use chb_lib\common\exception\Err;
use think\Validate;

class SystemConfigValidate  extends Validate
{
    use ValidateTrait;

    protected $being = "alias"; //唯一字段检查

    protected $rule = [
        'alias' => 'require|max:200|checkBeing',
        'value' => 'require|max:5000',
    ];
}
