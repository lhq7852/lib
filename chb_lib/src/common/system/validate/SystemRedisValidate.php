<?php

/**
 * 
 */

namespace chb_lib\common\system\validate;

use chb_lib\common\ValidateTrait;
use think\Validate;

class SystemRedisValidate  extends Validate
{
    use ValidateTrait;

    protected $being = "redis_key"; //唯一字段检查

    protected $rule = [
        'redis_key' => 'require|max:200|checkBeing',
        'redis_time' => 'require|number',
        'select_db' => 'require|between:0,15',
        'remark' => 'max:5000',
    ];
}
