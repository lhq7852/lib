<?php

namespace chb_lib\common\system;

use chb_lib\common\ServiceTrait;
use chb_lib\common\RedisToolTrait;

class SystemApi
{

    use RedisToolTrait, ServiceTrait;

    /**
     * 收集接口api
     *
     * @return bool
     */
    public function addCollectApi()
    {
        $api_alias = request()->param("api", "", "trim");
        $check = $this->getInfo(['api_alias' => $api_alias], false);
        if (!empty($check)) {
            return true;
        }
        if (strchr($api_alias, 'add')) {
            $name = '添加';
        } else if (strchr($api_alias, 'edit')) {
            $name = '修改';
        } else if (strchr($api_alias, 'delete')) {
            $name = '删除';
        } else if (strchr(strtolower($api_alias), 'list')) {
            $name = '列表';
        } else if (strchr($api_alias, 'info')) {
            $name = '查看';
        }
        $data = [
            'api_alias' => $api_alias,
            'api_name' => $name ?? '',
            'api_url' => "/admin/api/index",
            'api_domain' => '',
            'api_type' => 1,
            'remark' => $name ?? '',
        ];
        $this->addData($data);
        return true;
    }
}
