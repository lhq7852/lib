<?php

namespace chb_lib\common\system;

use chb_lib\common\ServiceTrait;
use chb_lib\common\RedisToolTrait;

class SystemDictionaryItem
{

    use RedisToolTrait, ServiceTrait;

    public function __construct()
    {
        $this->redisKey = "System:Dictionary:Item:";
    }

    /**
     * 键值对获取
     * 示例
     * @param [type] $alias
     * @return array
     */
    public function getKeyConfig($alias)
    {
        $data = $this->getDataList(['alias' => $alias, 'field' => 'tag_name,value']);
        return $data;
    }
}
