<?php

/**
 * 异常接管: 国际多语言异常提示
 */

namespace chb_lib\common\exception;

use chb_lib\common\BaseSystem;
use think\Lang;

class Err
{
    public static $data = [];
    public static $errData = [];

    /**
     * 根据错误码抛出异常
     *
     * @param integer $code
     * @return \Exception
     */
    public static function errorMsg($code = 500, $msg = '')
    {
        empty($code) && $code = '500';
        if (empty($msg)) {
            $msg = lang($code);
        }
        (empty($msg) || $msg == $code) && $msg = '系统异常';
        throw new \Exception($msg, $code);
    }

    /**
     * 成功信息
     *
     * @return array
     */
    public static function getReturnSuccess()
    {
        return [
            'code' => 200,
            'msg' => lang('200'),
        ];
    }

    /**
     * 获取异常返回
     *
     * @param \Exception $e
     * @return array
     */
    public static function getReturnMsg(\Exception $e)
    {
        $debug = (BaseSystem::_getObject("SystemConfig"))::_getKeyConfig("debug");
        $method = $debug ? 'returnMsgInLine' : 'returnMsgNotLine';
        return self::$method($e);
    }

    /**
     * 返回带有行号的错误信息= 开启debug
     *
     * @param \Exception $e
     * @return array
     */
    public static function returnMsgInLine(\Exception $e)
    {
        $trace_string = explode("#", $e->getTraceAsString());
        $code = $e->getCode();
        $err = self::$errData = [
            'code' => empty($code) ? 500 : $code,
            'msg' => $e->getMessage(),
            'line' => self::getMsgLine($e),
            'trace_string' => array_slice($trace_string, 1, 3),
        ];
        $err = array_merge($err, self::$data);
        trace($err, 'chb_error');
        return $err;
    }

    /**
     * 返回没有行号的错误信息 = 关闭debug
     *
     * @param \Exception $e
     * @return array
     */
    public static function returnMsgNotLine(\Exception $e)
    {
        $trace_string = explode("#", $e->getTraceAsString());
        $code = $e->getCode();
        $msg = $e->getMessage();
        $err = self::$errData = [
            'code' => empty($code) ? 500 : $code,
            'msg' => $msg,
            'line' => self::getMsgLine($e),
            'trace_string' => array_slice($trace_string, 1, 3),
        ];
        if (strchr($msg, 'SQLSTATE')) {
            $msg = lang(500);
        }
        $ret = [
            'code' => empty($code) ? 500 : $code,
            'msg' => $msg,
        ];
        trace($err, 'chb_error');
        return $ret;
    }

    /**
     * 获取异常错误位置
     *
     * @param \Exception $e
     * @return string
     */
    public static function getMsgLine(\Exception $e)
    {
        $trace_string = explode("#", $e->getTraceAsString());
        $arr = array_slice($trace_string, 1, 1);
        $str =  str_replace("0", "", $arr[0]);
        return $str;
    }

    /**
     * 键值对获取
     * 示例
     * @param [type] $alias
     * @return array
     */
    public static function getKeyConfig($module)
    {
        $lang = $param['lang'] ?? 'zh-cn';
        $path = __DIR__ . '/../lang/' . $lang . '.php';
        if (!file_exists($path)) {
            return [];
        }
        $config = include($path);
        return $config[$module] ?? [];
    }
}
