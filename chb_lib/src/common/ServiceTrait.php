<?php

namespace chb_lib\common;

use chb_lib\common\exception\Err;
use chb_lib\common\log\OperationLog;
use think\Exception;
use think\exception\ValidateException;
use think\facade\Db;

/**
 * Trait ServiceTrait
 * @package chb_lib\common
 *
 * 公用逻辑封装
 */
trait ServiceTrait
{
    use UtilsTrait;

    protected static $staticObj = [];

    protected $param = []; //对象传参属性
    protected $is_validate = true; //是否使用验证器
    protected $model; //当前模型
    protected $classFile;
    protected $transHook;
    protected $dataInfo = []; //新的数据
    protected $oldDataInfo = []; //旧数据
    protected $editParam = [];
    protected $className;
    protected $isEdit = false;

    /**
     * [静态]转 [单例访问对象]
     *
     * @param [type] $method
     * @param [type] $avg
     * @return void
     */
    public static function __callStatic($method = '', $avg = [])
    {
        if (strpos("$" . $method, "_") == 1) {
            $objStatic = self::_callInitObj();
            $method = str_replace("_", "", $method);

            if (!method_exists($objStatic, $method) && !empty($objStatic->getModel(false)) && !method_exists($objStatic->getModel(false), $method)) {
                Err::errorMsg(453);
            }
            if (method_exists($objStatic, $method)) {
                return call_user_func_array([$objStatic, $method], $avg);
            }
            if (!empty($objStatic->getModel(false)) && method_exists($objStatic->getModel(false), $method)) {
                return call_user_func_array([$objStatic->getModel(), $method], $avg);
            }
        }
        Err::$data['method'] = __CLASS__ . " 不存在方法 " . $method;
        Err::errorMsg(455);
    }

    /**
     * 设置传入的参数
     *
     * @param [type] $param
     * @return object
     */
    public function setParam($param = [])
    {
        $this->param = $param;
        if (!empty($this->getModel(false))) {
            $this->getModel(false)->setParam($param);
        }
        $this->param['login_organization_no'] = request()->user['organization_no'] ?? '';
        $this->param['login_user_no'] = request()->user['user_no'] ?? '';
        $this->param['_class'] = __CLASS__;
        return $this;
    }

    /**
     * 单例
     *
     * @param string $uuid
     * @param array $constructParam 构造函数参数
     * @return object
     */
    protected static function _callInitObj($uuid = '0')
    {
        if (!empty(static::$staticObj[$uuid])) {
            return static::$staticObj[$uuid];
        }
        static::$staticObj[$uuid] = new static();
        return static::$staticObj[$uuid];
    }

    /**
     * 最后一次执行的sql语句,方便调试用,不用每次都引入db对象
     *
     * @return string
     */
    protected function getlastsql()
    {
        return Db::getLastSql();
    }

    /**
     * 查看单条数据（根据主键）
     * @param $primary_key
     * @return array
     */
    public function getDataInfo($value = '', $field = null, $default = true)
    {
        if (method_exists($this, "getDataByRedisHook") && !isset($this->redisHook[$this->createHookKey(__FUNCTION__, func_get_args())])) {
            return $this->getDataByRedisHook(__FUNCTION__, func_get_args(), false);
        }
        $where[($field ?? $this->getModel()->getPK())] = $value;
        $info = $this->getModel()->getDataInfo($where);
        empty($info) && $default && Err::errorMsg(801);
        $info = $this->formatInfo($info);
        $this->dataInfo = $info;
        return $info;
    }

    /**
     * 格式化
     *
     * @param [type] $info
     * @return array
     */
    public function formatInfo(&$info)
    {
        return $info;
    }

    /**
     * 获取单条数据
     *
     * @param array $param
     * @return array
     */
    public function getInfo($param = [], $default = true)
    {
        if (method_exists($this, "getDataByRedisHook") && !isset($this->redisHook[$this->createHookKey(__FUNCTION__, func_get_args())])) {
            return $this->getDataByRedisHook(__FUNCTION__, func_get_args(), false);
        }
        $info = $this->setFilterWhere($param, __FUNCTION__)->getModel()->getInfo($param);
        empty($info) && $default && Err::errorMsg(801);

        $info = $this->formatInfo($info);
        $this->dataInfo = $info;
        return $info;
    }

    /**
     * 无分页列表
     *
     * @param array $param
     * @return array
     */
    public function getDataList($param = [])
    {
        if (method_exists($this, "getDataByRedisHook") && !isset($this->redisHook[$this->createHookKey(__FUNCTION__, func_get_args())])) {
            return $this->getDataByRedisHook(__FUNCTION__, func_get_args(), false);
        }
        $list = $this->setFilterWhere($param, __FUNCTION__)->getModel()->getDataList($param);
        if (empty($list)) {
            return [];
        }
        return $list;
    }

    /**
     * 无分页列表,树形列表-模糊搜索带子级
     *
     * @param array $param
     * @return array
     */
    public function getTreeList($param)
    {
        if (method_exists($this, "getDataByRedisHook") && !isset($this->redisHook[$this->createHookKey(__FUNCTION__, func_get_args())])) {
            return $this->getDataByRedisHook(__FUNCTION__, func_get_args(), false);
        }
        if (empty($param['keyword'])) {
            $param['parent_id'] = 0;
        }
        $pk = $this->getModel()->getPk();
        if (!empty($param['is_all'])) {
            $list = $this->getDataList($param);
            $list = $this->buildTree($list, 0, $pk);
            return $list;
        }
        $list = $this->getPageList($param);
        if (empty($list['data'])) {
            return [];
        }
        $parent = array_column($list['data'], 'parent_id');

        $arrChildren = array_column($list['data'], 'children_ids');
        $arrChildren = implode('', $arrChildren);
        $arrChildren = array_unique(array_filter(explode(",", $arrChildren)));
        if (empty($arrChildren)) {
            return [];
        }
        $childList = $this->getDataList([$pk => $arrChildren]);
        $list['data'] = array_values(array_column(array_merge($list['data'], $childList), null, $pk));
        $list['data'] = $this->buildTree($list['data'], (empty($parent) ? 0 : min($parent)), $pk);
        return $list;
    }

    /**
     * 分页列表
     *
     * @param array $param
     * @return array
     */
    public function getPageList($param = [])
    {
        if (method_exists($this, "getDataByRedisHook") && !isset($this->redisHook[$this->createHookKey(__FUNCTION__, func_get_args())])) {
            return $this->getDataByRedisHook(__FUNCTION__, func_get_args(), false);
        }
        $list = $this->setFilterWhere($param, __FUNCTION__)->getModel()->getPageList($param);
        if (empty($list)) {
            return [];
        }
        return $list;
    }

    /**
     * 多类型表单
     * // protected $typeList = [];
     */
    public function getTypeList($param = [])
    {
        if (method_exists($this, "getDataByRedisHook") && !isset($this->redisHook[$this->createHookKey(__FUNCTION__, func_get_args())])) {
            return $this->getDataByRedisHook(__FUNCTION__, func_get_args(), false);
        }
        $method = $this->typeList[$param['type_list']] ?? '';
        if (empty($method) || !method_exists($this, $method)) {
            Err::errorMsg(820);
        }
        $data = $this->$method($param);
        return $data;
    }

    /**
     * 自定义过滤字段
     *
     * @param array $param
     * @param [type] $method
     * @return object
     */
    protected function setFilterWhere(&$param = [], $method = null)
    {
        return $this;
    }

    /**
     * 是否使用验证器
     *
     * @param boolean $default
     * @return object
     */
    protected function setIsValidate($default = false)
    {
        $this->is_validate = $default;
        return $this;
    }

    /**
     * 检验参数
     * validate
     * @param array $param
     * @param string $action
     * @return object
     */
    public function validateData($param = [], $action = '')
    {
        if (!$this->is_validate) {
            return $this;
        }

        //验证逻辑
        try {
            $this->getClass('validate', false);
            class_exists($this->classFile) && is_array($param) && validate($this->classFile)->scene($action)->check($param);
        } catch (ValidateException $e) {
            $config = Err::getKeyConfig($this->getModel()->getName());
            $msg = $this->searchReplace($e->getError(), $config); //搜索替换 字段语言
            Err::$data['data_error'] = $e->getMessage() . $e->getFile() . $e->getLine();
            Err::errorMsg(800, $msg);
        }
        return $this;
    }

    /**
     * 事务钩子
     *
     * @param [string] $action
     * @param array $param
     * @param boolean $default
     * @return array|string|Exception
     * if (!isset($this->transHook[__FUNCTION__])) {
     *      return $this->transHook(__FUNCTION__, func_get_args());
     *  }
     */
    public function transHook($action = '', $param = [])
    {
        if (!method_exists($this, $action)) {
            Err::errorMsg(451);
        }
        if (!empty($this->transHook[$action])) {
            return call_user_func_array([$this, $action], $param);
        }
        $this->transHook[$action] = 1; //记录访问次数
        Db::startTrans();
        try {
            $result = call_user_func_array([$this, $action], $param);
            Db::commit();
        } catch (\Exception $e) {
            Db::rollback();
            Err::$data['data_error'] = Err::getReturnMsg($e);
            Err::$data['sql'] = $this->getlastsql();
            $code = $e->getCode();
            Err::errorMsg(empty($code) ? 808 : $code, $e->getMessage());
        }
        return $result;
    }

    /**
     * 设置入库的参数,可重写
     *
     * @param [type] $param
     * @return object
     */
    protected function setFieldParam(&$param = [], $mode = "add")
    {
        return $this;
    }

    /**
     * 新增数据-单表
     * @param array $param
     * @return mixed
     * @throws Exception
     */
    public function addData(array $param = [])
    {
        if (!isset($this->transHook[__FUNCTION__])) {
            return $this->transHook(__FUNCTION__, func_get_args());
        }
        $id = $this->validateData($param, __FUNCTION__)->setFieldParam($param, 'add')->getModel()->addData($param);

        $this->dataInfo = $this->getDataInfo($id);
        $this->listenRedis(__FUNCTION__);
        return $id;
    }

    /**
     * 根据主键删除单条数据
     * @param $param primary_key|where  主键或者条件数组
     * @return mixed
     * @throws Exception
     */
    public function deleteData($param)
    {
        if (!isset($this->transHook[__FUNCTION__])) {
            return $this->transHook(__FUNCTION__, func_get_args());
        }
        if (empty($param)) {
            Err::errorMsg(800);
        }
        $pk = $this->getModel()->getPk();
        $where = is_array($param) ? $param : [$pk => explode(",", $param)];
        $check = $this->oldDataInfo = $this->getInfo($where);
        empty($check) && Err::errorMsg(801);
        $this->checkDataLevel($check);

        $this->getModel()->deleteData($where);

        $this->listenRedis(__FUNCTION__);
        return true;
    }

    /**
     * 批量更新
     *
     * $param = [
     *  [
     *      id=>1,
     *     field=>5,
     *  ]
     * ]
     * $updateField 更新的字段名
     * @param array $param
     * @param string $updateField
     * @return array
     */
    public function batchEditData($param = [], $updateField = '')
    {
        if (!isset($this->transHook[__FUNCTION__])) {
            return $this->transHook(__FUNCTION__, func_get_args());
        }
        $this->getModel()->batchEditData($param, $updateField);
        $this->listenRedis(__FUNCTION__);
        return true;
    }

    /**
     * 更新数据
     * @param $param
     * @return string|object
     */
    public function editData(array $param = [], $where = null)
    {
        if (!isset($this->transHook[__FUNCTION__])) {
            return $this->transHook(__FUNCTION__, func_get_args());
        }
        $pk = $this->getModel()->getPk();
        !isset($param[$pk]) && empty($where) && Err::errorMsg(800);
        if (!empty($param[$pk])) {
            $where[$pk] = $param[$pk];
        }
        $check = $this->oldDataInfo = $this->getInfo($where);
        empty($check) && Err::errorMsg(801);
        $this->checkDataLevel($check);
        $param = array_merge($param, [$pk => $check[$pk]]);
        // 是否有数据不一致 有才更新 $isEdit
        foreach ($check as $key => $value) {
            if ($key == $pk) {
                continue;
            }
            if (isset($param[$key])) {
                if ($value != $param[$key]) {
                    $this->isEdit = true;
                }
            }
        }
        if (method_exists($this, 'setIsEdit')) {
            $this->setIsEdit();
        }
        if (!$this->isEdit) {
            return true;
        }
        $this->validateData($param, __FUNCTION__)->setFieldParam($param, 'edit')->getModel()->editData($param, [$pk => $check[$pk]]);
        $this->editParam = $param; //更新数据记录
        $this->dataInfo = $this->getDataInfo($check[$pk]);
        $this->listenRedis(__FUNCTION__);

        return true;
    }

    /**
     * 检查数据权限
     *
     * @return object
     */
    public function checkDataLevel($param = [])
    {
        return $this;
    }

    /**
     *  监听- 删除旧缓存 redis
     *
     * @return object
     */
    public function listenRedis($method = null)
    {
        if (in_array($method, ['addData']) && method_exists($this, 'listenAdd')) {
            $this->listenAdd();
        }
        if (in_array($method, ['editData', 'batchEditData']) && method_exists($this, 'listenEdit')) {
            $this->listenEdit();
        }
        if (in_array($method, ['deleteData']) && method_exists($this, 'listenDel')) {
            $this->listenDel();
        }
        if (method_exists($this, 'delRedis')) {
            $this->delRedis();
        }
        $objLog = OperationLog::_setModule($this->getModel()->getTable())
            ->setNewData($this->dataInfo)
            ->setOldData($this->oldDataInfo)
            ->setTypeId($this->getModel()->getPK())
            ->setActionType($method);
        if (!empty($this->titleField)) {
            $objLog = $objLog->setTitleField($this->titleField);
        }
        $objLog->addLog();
        return $this;
    }

    /**
     * 获取当前模型
     *
     * @return object
     */
    public function getModel($default = true)
    {
        if (!empty($this->model)) {
            return $this->model;
        }
        $this->model = $this->getClass('model');
        if (empty($this->model)) {
            if (!$default) {
                return null;
            }
            Err::$data['model'] = "数据模型:" . $this->classFile;
            Err::errorMsg(490);
        }
        return $this->model;
    }

    /**
     * 获取对象
     *
     * @param [type] $string
     * @return object|null|string
     */
    protected function getClass($string = '', $default = true)
    {
        $file = $this->getExplode('\\', get_class());
        $this->className = array_pop($file);
        $file[] = $string;
        $file[] = $this->className . ucfirst($string);
        $this->classFile = implode('\\', $file);
        if (class_exists($this->classFile) && $default) {
            return (new $this->classFile());
        }
        return null;
    }
}
