<?php

namespace chb_lib\common\utils;

use chb_lib\common\SingleTrait;
use Endroid\QrCode\QrCode;

/**
 * 二维码
 * @package chb_lib\common\utils
 */
class QrcodeUtils
{
    use SingleTrait;

    /**
     * 通用二维码
     *
     * @return void|string
     */
    public function currency($param = [])
    {
        $qrCode = new QrCode();
        $qrCode->setText($param['txt'] ?? '12346'); //设置二维码上的内容
        $qrCode->setSize(300); //二维码尺寸
        $qrCode->setWriterByName('png'); //设置输出的二维码图片格式
        $qrCode->setEncoding('UTF-8');
        $qrCode->setMargin(10);
        // $qrCode->setLogoPath(__DIR__.'/logo.png'); //logo水印图片的所在的路径
        // $qrCode->setLogoSize(150, 200); //设置logo水印的大小，单位px ，参数如果是一个int数字等比例缩放
        header('Content-Type: ' . $qrCode->getContentType());
        exit($qrCode->writeString());
    }
}
