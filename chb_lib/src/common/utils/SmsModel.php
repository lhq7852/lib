<?php

/**
 * 阿里云短信发送
 */

namespace chb_lib\common\utils;

use AlibabaCloud\SDK\Dysmsapi\V20170525\Dysmsapi;
use Darabonba\OpenApi\Models\Config;
use AlibabaCloud\SDK\Dysmsapi\V20170525\Models\SendSmsRequest;
use chb_lib\common\RedisToolTrait;
use chb_lib\common\UtilsTrait;

class SmsModel
{
    use RedisToolTrait, UtilsTrait;

    public $accessKeyId = '';
    protected $accessKeySecret = '';
    protected $maxSms = 10; //每天上限发送短信次数
    protected $arrCheck;
    protected $phone;
    protected $template = [
        "accessKeyId" => '',
        "accessKeySecret" => '',
        "signName" => "",
        "templateCode" => "",
        "maxMobile" => 10,
        "durationTime" => 900,
        "intervalTime" => 60,
    ]; //配置模板

    public function __construct()
    {
        $this->time = time();
        $this->redisKey = "SmsModel:" . date("Ymd") . ":";
    }

    /**
     * 发送短信的手机号
     *
     * @param string $phone
     * @return object
     */
    public function setPhone($phone = '')
    {
        if (!$this->mobileVerify($phone)) {
            throw new \Exception("请输入正确的手机号码", 500);
        }
        $this->phone = $phone;
        $this->redisKey .= $phone;
        $this->arrCheck = $this->setRedisKey($this->redisKey)->getRedisArray();
        return $this;
    }

    /**
     * 配置阿里云模板
     *
     * @param array $template
     * @return object
     */
    public function setTemplate($template = [])
    {
        if (!isset($template['accessKeySecret']) || empty($template['accessKeySecret']) || !isset($template['accessKeyId']) || empty($template['accessKeyId'])) {
            throw new \Exception("请先配置阿里云短信");
        }
        $this->template = $template;
        $this->maxSms = $template['maxMobile'] ?? $this->maxSms;
        $this->accessKeyId = $template['accessKeyId'] ?? $this->accessKeyId;
        $this->accessKeySecret = $template['accessKeySecret'] ?? $this->accessKeySecret;
        return $this;
    }

    /**
     * 检查当天发送短信上限次数
     *
     * @return object
     */
    public function checkSms()
    {
        $ret = $this->arrCheck ?? [];
        if (empty($ret)) {
            return $this;
        }
        if ($ret['num'] >= $this->maxSms) {
            throw new \Exception("当天发送短信测试已上限");
        }
        $s = $this->time - $ret['lastTime'];
        if ($s <= $this->template['intervalTime']) {
            throw new \Exception("请" . ($this->template['intervalTime'] - $s) . "秒后重新发送");
        }
        return $this;
    }

    /**
     * 验证短信验证码
     *
     * @param string $code
     * @return bool
     */
    public function verifyCode($code = '')
    {
        if ($this->arrCheck['code'] != $code) {
            throw new \Exception("验证码错误");
        }
        $s = $this->time - $this->arrCheck['lastTime'];
        if ($s > $this->template['durationTime']) {
            throw new \Exception("短信验证码已过期");
        }
        return true;
    }

    /**
     * 生成短信验证码
     *
     * @return void
     */
    public function createCode()
    {
        $this->arrCheck['code'] = rand(10000, 99999);
        return $this;
    }

    /**
     * 使用AK&SK初始化账号Client
     * @param string $accessKeyId
     * @param string $accessKeySecret
     * @return Dysmsapi Client
     */
    public function createClient()
    {
        $config = new Config([
            // 您的AccessKey ID
            "accessKeyId" => $this->accessKeyId,
            // 您的AccessKey Secret
            "accessKeySecret" => $this->accessKeySecret,
        ]);
        // 访问的域名
        $config->endpoint = "dysmsapi.aliyuncs.com";
        return new Dysmsapi($config);
    }

    /**
     * 发送短信验证码
     * @param string[] $args
     * @return void
     */
    public function send()
    {
        $this->checkSms()->createCode();
        $client = $this->createClient();
        $sendSmsRequest = new SendSmsRequest([
            "phoneNumbers" => $this->phone,
            "signName" => $this->template['signName'],
            "templateCode" => $this->template['templateCode'],
            "templateParam" => "{\"code\":\"" . $this->arrCheck['code'] . "\"}",
        ]);
        // 复制代码运行请自行打印 API 的返回值
        $ret = $client->sendSms($sendSmsRequest);
        if ($ret->body->code == 'OK') {
            $this->arrCheck['num'] = empty($this->arrCheck['num']) ? 1 : $this->arrCheck['num'] + 1; //当天发送次数
            $this->arrCheck['lastTime'] = $this->time; //上一次发送短信时间
            $endTime = strtotime(date("Y-m-d", strtotime("+1 day")));//第二天0时
            $this->setRedisKey($this->redisKey)->setRedisTime($endTime - $this->time)->setRedisValue($this->arrCheck);
            return true;
        }
        throw new \Exception("发送短信失败,请检查阿里云配置");
    }
}
