<?php

namespace chb_lib\common\utils;

use AlibabaCloud\Client\AlibabaCloud;
use chb_lib\common\RedisToolTrait;
use chb_lib\common\RpcServerTrait;
use OSS\OssClient;
use OSS\Core\OssException;

class OssTool
{
    use RpcServerTrait, RedisToolTrait;

    private $accessKeyId = '';
    private $accessKeySecret = '';
    private $endpoint = '';
    public $oss_client;
    public $bucket = '';
    private $read_stream_read = 0;
    private $original_name = ''; //当前上传文件名
    private $size = 0; //当前上传文件大小
    private $to_filename;
    private $from_filename;
    private $user_id;

    private $roleArn = "";
    private $roleSessionName = "";
    private $stsAccessKeyId = '';
    private $stsAccessKeySecret = '';
    private $stsHost = "";
    private $regionId = "";

    public function __construct()
    {
        $this->redisKey = "OssTool:OSSjingtaichucunpeizhi:";
        try {
            $data = $this->getConfig();
            //php上传配置
            $this->accessKeyId = $data['accessKeyId'] ?? '';
            $this->accessKeySecret = $data['accessKeySecret'] ?? '';
            $this->bucket = $data['bucket'] ?? '';
            $this->endpoint = $data['endpoint'] ?? '';

            //sts配置
            $this->stsAccessKeyId = $data['stsAccessKeyId'] ?? '';
            $this->stsAccessKeySecret = $data['stsAccessKeySecret'] ?? '';
            $this->roleArn = $data['roleArn'] ?? '';
            $this->roleSessionName = $data['roleSessionName'] ?? '';
            $this->stsHost = $data['stsHost'] ?? '';
            $this->regionId = $data['regionId'] ?? '';

            $this->oss_client = new OssClient($this->accessKeyId, $this->accessKeySecret, $this->endpoint);
        } catch (OssException $e) {
            throw new \Exception("OSS对象配置错误,请检查!", 500);
        }
    }

    public function getConfig()
    {
        if (!isset($this->redisHook[__FUNCTION__])) {
            return $this->getDataByRedisHook(__FUNCTION__, [], false);
        }
        return $this->rpcUserResult(['rpc' => 'config.config.common_config', 'type_no' => 'OSSjingtaichucunpeizhi']);
    }

    /**
     * 文件上传
     * @param $to_path oss的存储目录（包括文件名）
     * @param $from_path 要上传的源文件目录（包括文件名）
     * @return bool
     * @throws \think\Exception
     */
    public function uploadFile($to_path, $from_path, $param = [])
    {
        try {
            $this->original_name = $param['original_name'] ?? '';
            $this->size = $param['size'] ?? '';
            $this->user_id = request()->user_id;
            $this->to_filename = $to_path;
            $this->from_filename = $from_path;
            $response_row = $this->oss_client->register_streaming_read_callback([$this, "setUploadprogress"])->uploadFile($this->bucket, $to_path, $from_path);
            if (isset($response_row['info']['http_code']) && $response_row['info']['http_code'] == 200) {
                return true;
            } else {
                throw new \Exception('文件上传失败');
            }
        } catch (OssException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function setUploadprogress($curl_handle, $file_handle, $out)
    {
        $redisKey = "uploadprogress:";
        $this->read_stream_read += strlen($out);
        $this->setRedisKey($redisKey . $this->user_id . $this->original_name)->setRedisValue([
            'read_stream_read' => $this->read_stream_read,
            'size' => $this->size,
            'to_filename' => $this->to_filename,
            'from_filename' => $this->from_filename,
        ]);
        return true;
    }

    public function getUploadprogress($param = [])
    {
        $redisKey = "uploadprogress:";
        $ret = $this->setRedisKey($redisKey . request()->user_id . $param['original_name'])->getRedisArray();
        if (!$ret) {
            return ['progress' => 0, 'file_url' => ''];
        }
        if (!empty($ret['read_stream_read']) && !empty($ret['size'])) {
            if ($ret['read_stream_read'] >= $ret['size']) {
                return [
                    'progress' => 100,
                    'file_url' => isset($ret['to_filename']) ? $this->getDownLoadUrl($ret['to_filename']) : '',
                    'to_filename' => $ret['to_filename'] ?? '',
                    'from_filename' => $ret['from_filename'] ?? '',
                ];
            }
            return [
                'progress' => (int)bcmul(bcdiv($ret['read_stream_read'], $ret['size'], 2), 100, 2),
                'file_url' => isset($ret['to_filename']) ? $this->getDownLoadUrl($ret['to_filename']) : '',
                'to_filename' => $ret['to_filename'] ?? '',
                'from_filename' => $ret['from_filename'] ?? '',
            ];
        }
        return [
            'progress' => 100,
            'file_url' => isset($ret['to_filename']) ? $this->getDownLoadUrl($ret['to_filename']) : '',
            'to_filename' => $ret['to_filename'] ?? '',
            'from_filename' => $ret['from_filename'] ?? '',
        ];
    }

    /**
     * 删除文件
     *
     * @param $to_path
     * @return bool
     * @throws \think\Exception
     */
    public function deleteFile($to_path)
    {
        try {
            return true;
            $response_row = $this->oss_client->deleteObject($this->bucket, $to_path);
            if (isset($response_row['info']['http_code']) && in_array($response_row['info']['http_code'], [200, 204])) {
                return true;
            } else {
                throw new \Exception('删除文件失败');
            }
        } catch (OssException $e) {
            throw new \Exception($e->getMessage());
        }
    }

    public function getDownLoadUrl($filename)
    {
        return 'https://' . $this->bucket . '.' . $this->endpoint . '/' . $filename;
    }

    public function getStsToken()
    {
        AlibabaCloud::accessKeyClient($this->stsAccessKeyId, $this->stsAccessKeySecret)
            ->regionId($this->regionId)
            ->asDefaultClient();
        try {
            $result = AlibabaCloud::rpc()
                ->product('Sts')
                ->scheme('https') // https | http
                ->version('2015-04-01')
                ->action('AssumeRole')
                ->method('POST')
                ->host($this->stsHost)
                ->options([
                    'query' => [
                        'RoleArn' => $this->roleArn,
                        'RoleSessionName' => $this->roleSessionName,
                        'DurationSeconds' => 15 * 60, //持续秒 15min/1hr
                    ],
                ])
                ->request();
            $data = $result->toArray();
            $ret = empty($data['Credentials']) ? [] : $data['Credentials'];
            if (empty($ret)) {
                return [];
            }
            $ret = array_merge($ret, [
                'regionId' => $this->regionId,
                'bucket' => $this->bucket,
                'stsHost' => $this->stsHost,
                'endpoint' => $this->endpoint,
            ]);
            return $ret;
        } catch (\Exception $e) {
            throw new \Exception($e->getMessage());
        }
    }
}
