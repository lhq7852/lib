<?php

/**
 * 邮件发送
 */

namespace chb_lib\common\utils;

use AlibabaCloud\SDK\Dysmsapi\V20170525\Dysmsapi;

use Darabonba\OpenApi\Models\Config;
use AlibabaCloud\SDK\Dysmsapi\V20170525\Models\SendSmsRequest;
use chb_lib\common\RedisToolTrait;
use chb_lib\common\UtilsTrait;
use PHPMailer\PHPMailer\PHPMailer;

class SmtpEmail
{
    use RedisToolTrait, UtilsTrait;

    public $accessKeyId = '';
    protected $accessKeySecret = '';
    protected $maxSms = 10; //每天上限发送短信次数
    protected $arrCheck;
    protected $email;
    protected $template = [
        "host" => "", //服务器地址
        'port' => 465, //端口
        'password' => '', //授权码
        'tls_or_ssl' => 'ssl', //TLS 或者SSL协议
        "form_email" => "", //发送者邮箱
        "form_author" => '', //发送者名称
        "to_author" => "尊敬的客户", //
        "reply_email" => "", //收回复邮箱
        "title" => "财虎邦邮箱验证码",
        "body" => "验证码：{code},请勿泄露给TA人！",
        "alt_body" => "邮件不支持html的时候,发纯文字内容",
        "maxEmail" => 10, //当天发送上限
    ]; //配置模板

    public function __construct()
    {
        $this->time = time();
        $this->redisKey = "SmtpEmail:" . date("Ymd") . ":";
    }

    /**
     * 发送邮箱
     *
     * @param string $phone
     * @return object
     */
    public function setEmail($email = '')
    {
        if (!$this->emailVerify($email)) {
            throw new \Exception("收件人邮箱不能为空", 500);
        }
        $this->email = $email;
        $this->redisKey .= $email;
        $this->arrCheck = $this->setRedisKey($this->redisKey)->getRedisArray();
        return $this;
    }

    /**
     * 配置阿里云模板
     *
     * @param array $template
     * @return object
     */
    public function setTemplate($template = [])
    {
        $this->template = $template;
        $this->maxSms = $template['maxEmail'] ?? $this->maxSms;
        return $this;
    }

    /**
     * 检查当天发送短信上限次数
     *
     * @return object
     */
    public function checkSms()
    {
        $ret = $this->arrCheck ?? [];
        if (empty($ret)) {
            return $this;
        }
        if ($ret['num'] >= $this->maxSms) {
            throw new \Exception("当天发送短信测试已上限");
        }
        $s = $this->time - $ret['lastTime'];
        if ($s <= 60) {
            throw new \Exception("请" . (60 - $s) . "秒后重新发送");
        }
        return $this;
    }

    /**
     * 验证短信验证码
     *
     * @param string $code
     * @return bool
     */
    public function verifyCode($code = '')
    {
        if ($this->arrCheck['code'] != $code) {
            throw new \Exception("验证码错误");
        }
        $s = $this->time - $this->arrCheck['lastTime'];
        if ($s > 60 * 15) {
            throw new \Exception("邮箱验证码已过期");
        }
        return true;
    }

    /**
     * 生成短信验证码
     *
     * @return void
     */
    public function createCode()
    {
        $this->arrCheck['code'] = rand(10000, 99999);
        $this->template['body'] = sprintf(str_replace("{code}", "%s", $this->template['body']), $this->arrCheck['code']);
        $this->template['alt_body'] = strip_tags(sprintf(str_replace("{code}", "%s", $this->template['alt_body']), $this->arrCheck['code']));
        return $this;
    }


    /**
     * 发送验证码-邮箱
     * @param string[] $args
     * @return void
     */
    public function sendCode()
    {
        $this->checkSms()->createCode();
        // 复制代码运行请自行打印 API 的返回值
        $ret = $this->sendSmtp();
        if ($ret == 'ok') {
            $this->arrCheck['num'] = empty($this->arrCheck['num']) ? 1 : $this->arrCheck['num'] + 1; //当天发送次数
            $this->arrCheck['lastTime'] = $this->time; //上一次发送短信时间
            $endTime = strtotime(date("Y-m-d", strtotime("+1 day")));
            $this->setRedisKey($this->redisKey)->setRedisTime($endTime - $this->time)->setRedisValue($this->arrCheck);
            return true;
        }
        throw new \Exception("发送邮箱失败,请检查邮箱配置");
    }

    /**
     * 发送邮箱
     *
     * @return void
     */
    public function sendSmtp()
    {
        $mail = new PHPMailer(true);                              // Passing `true` enables exceptions
        try {
            //服务器配置
            $mail->CharSet = "UTF-8";                     //设定邮件编码
            $mail->SMTPDebug = 0;
            $mail->isSMTP();                             // 使用SMTP
            $mail->Host = $this->template['host'];                // SMTP服务器
            $mail->SMTPAuth = true;                      // 允许 SMTP 认证
            $mail->Username = $this->template['form_email'];                // SMTP 用户名  即邮箱的用户名
            $mail->Password = $this->template['password'];             // SMTP 密码  部分邮箱是授权码(例如163邮箱)
            $mail->SMTPSecure = $this->template['tls_or_ssl'];                    // 允许 TLS 或者ssl协议
            $mail->Port = $this->template['port'];                            // 服务器端口 25 或者465 具体要看邮箱服务器支持

            $mail->setFrom($this->template['form_email'], $this->template['form_author']);  //发件人
            $mail->addAddress($this->email, $this->template['to_author']);  // 收件人
            //$mail->addAddress('ellen@example.com');  // 可添加多个收件人
            $mail->addReplyTo($this->template['reply_email'], $this->template['form_author']); //回复的时候回复给哪个邮箱 建议和发件人一致
            //$mail->addCC('cc@example.com');                    //抄送
            //$mail->addBCC('bcc@example.com');                    //密送

            //发送附件
            // $mail->addAttachment('../xy.zip');         // 添加附件
            // $mail->addAttachment('../thumb-1.jpg', 'new.jpg');    // 发送附件并且重命名

            //Content
            $mail->isHTML(true);                                  // 是否以HTML文档格式发送  发送后客户端可直接显示对应HTML内容
            $mail->Subject = $this->template['title'];
            $mail->Body    = $this->template['body'];
            $mail->AltBody = $this->template['alt_body'];

            $mail->send();
            return 'ok';
        } catch (\Exception $e) {
            throw new \Exception('邮件发送失败: ', $mail->ErrorInfo);
        }
    }
}
