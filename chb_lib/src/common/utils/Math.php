<?php


namespace chb_lib\common\utils;

/**
 * 数学逻辑运算类
 * Class Math
 * @package chb_lib\common\utils
 */
class Math
{
    /**
     * 计算年化收益率（回报率）
     * @param array $cashflows
     * @param string $field_time
     * @param string $field_money
     * @return int|string
     */
    public static function getXirr(array $cashflows = [], string $field_time = 'stringTime', string $field_money = 'payment')
    {
        if (empty($cashflows)) return 0;
        $years = [];
        $first_time = strtotime($cashflows[0][$field_time]);

        foreach ($cashflows as $v) {
            $years[] = (strtotime($v[$field_time]) - $first_time) / 86400 / 365;
        }
        $residual = 1.0;
        $step = 0.05;
        $guess = 0.05;
        $epsilon = 0.0001;
        $limit = 10000;
        $count = 0;
        while (abs($residual) > $epsilon && $limit > 0) {
            $limit--;
            $residual = 0.0;
            foreach ($cashflows as $i => $trans) {
                ((float)pow($guess, $years[$i]) == 0) ?: $residual += $trans[$field_money] / pow($guess, $years[$i]);
            }
            if (abs($residual) > $epsilon) {
                if ($residual > 0) {
                    $guess += $step;
                } else {
                    $guess -= $step;
                    $step /= 2.0;
                }
            }
            $count++;
            if ($count > 1000) break;
        }
        return number_format(100 * ($guess - 1), 2, '.', '');
    }
}
