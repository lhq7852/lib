<?php

/**
 * api 接口验签+工厂
 */

namespace chb_lib\common;

use chb_lib\common\exception\Err;

trait ApiAuthTrait
{
    use RedisToolTrait;

    private static $timeStamp; // 时间戳
    private static $nonceStr; // 随机字符串
    private static $signKey; // 加密后签名
    private static $dataSign; // 请求参数
    private static $md5String; //加密的字符串
    private $app_name; //应用名

    /**
     * 验签
     */
    public function checkAuth($app_name = '')
    {
        $this->app_name = $app_name;
        $param = request()->param();
        self::$timeStamp = isset($param['timeStamp']) ? $param['timeStamp'] : ""; //当前时间戳
        self::$nonceStr = isset($param['nonceStr']) ? $param['nonceStr'] : ""; //随机字符串
        self::$signKey = isset($param['signKey']) ? $param['signKey'] : ""; //加密结果

        $open_sign = (BaseSystem::_getObject("SystemConfig"))::_getKeyConfig("open_sign");
        //request()->ip() == '127.0.0.1'
        if (!empty($open_sign) && strchr(self::$signKey, $open_sign)) {
            return true;
        }
        if (!self::$timeStamp) {
            Err::errorMsg(502);
        }
        if (!self::$nonceStr) {
            Err::errorMsg(502);
        }
        if (!self::$signKey) {
            Err::errorMsg(502);
        }
        unset($param['signKey']);
        if (isset($param['s'])) {
            unset($param['s']);
        }
        self::$dataSign = $param;
        /*校验签名*/
        $this->checkSignKey();
        return true;
    }

    /**
     * 检查签名是否正确
     */
    public function checkSignKey()
    {
        $sessionkey = $this->setRedisKey($this->redisKey . md5(request()->ip() . 'signKey'))->getRedisString();

        if ($sessionkey && self::$signKey == $sessionkey) {
            $this->addErrLog($sessionkey, "请求不能重复");
            Err::errorMsg(501);
        }
        $mySignKey = $this->appKeyCreat();
        if (self::$signKey == $mySignKey) {
            $this->setRedisKey($this->redisKey . md5(request()->ip() . 'signKey'))->setRedisValue($mySignKey);
        } else {
            $this->addErrLog($mySignKey);
            Err::$data['sign'] = "密钥不对:" . self::$md5String;
            Err::errorMsg(505);
        }
        return true;
    }

    private function addErrLog($signKey, $msg = '签名验证错误')
    {
        $content = json_encode([
            'msg' => $msg,
            'timeStamp' => self::$timeStamp,
            'nonceStr' => self::$nonceStr,
            'dataSign' => self::$dataSign,
            'signKey' => self::$signKey, // 前端加密sig
            'mysignKey' => $signKey, // 自身加密sig
            'md5String' => self::$md5String, //加密的字符串
            'ip' => request()->ip(),
            '_REQUEST' => request()->param(),
        ]);
        //写入错误日志
        trace("秘钥不对：" . $content, 'error');
        return true;
    }

    /**
     * $timestamp 时间戳
     * $nonceStr 随机字符串
     * $signType 签名类型，默认为MD5
     * $dataSign  需要加入加密的参数
     */
    private function appKeyCreat()
    {
        $signStr = "";
        if (self::$dataSign) {
            ksort(self::$dataSign); //按拼音重新排序
            foreach (self::$dataSign as $key => $val) {
                if (!isset($val) || strtolower($val) == 'null') {
                    continue;
                }
                $checkVal = is_string($val) ? json_decode($val, true) : '';
                if (is_array($checkVal) || is_array($val)) {
                    continue;
                }
                $signStr .= strtoupper($key) . "=$val" . "&";
            }
        }
        $key = (BaseSystem::_getObject("SystemConfig"))::_getKeyConfig($this->app_name);
        if (empty($key)) {
            Err::errorMsg(410);
        }
        self::$md5String = $signStr . $key;
        return md5(self::$md5String);
    }
}
