<?php

namespace chb_lib\common;

use chb_rpc\resource_client\ResourceClientRpc;
use Hprose\Http\Client;
use think\Exception;
/**
 * Rpc服务
 * Trait RpcServerTrait
 * @package chb_lib\common
 */
trait RpcServerTrait
{
    /**
     * 用户RPC服务
     * @param array $param
     * @return mixed
     * @throws Exception
     */
    public function rpcUserResult(array $param = [])
    {
        try {
            $data['rpc_key'] = 'asIU^YT%Rld678jdf454';
            $objClient = new Client(config("chb.user_url") . '/rpc/RpcServer/index', false);
            $result = $objClient->rpc_server(array_merge($data, $param, ['param' => request()->param('')], ['client_domain' => $_SERVER['SERVER_NAME'] ?? '']));
        } catch (\Exception $e) {
            throw new \Exception('系统异常!');
        }
        if ($result['code'] != 200) {
            throw new \Exception('100:' . $result['msg'], $result['code']);
        }
        return $result['data'];
    }

    /**
     * 外部接口RPC服务
     * @param array $param
     * @return mixed
     * @throws Exception
     */
    public function rpcExternalResult(array $param = [])
    {
        try {
            $appInfo = ResourceClientRpc::_getAppIdByClientPk();
            $data['rpc_key'] = "yhjk56789G%%HJK";
            $objClient = new Client(config("chb.external_url") . '/rpc/RpcServer/index', false);
            $result = $objClient->rpc_server(array_merge($data, $appInfo, $param, ['param' => request()->param('')], ['client_domain' => $_SERVER['SERVER_NAME'] ?? '']));
        } catch (\Exception $e) {
            echo $e->getMessage();exit;
            throw new \Exception('资源系统异常!');
        }
        if ($result['code'] != 200) {
            throw new \Exception('900:' . $result['msg'], $result['code']);
        }
        return $result['data'];
    }

    /**
     * 消息接口RPC服务
     * @param array $param
     * @return mixed
     * @throws Exception
     */
    public function rpcMessageResult(array $param = [])
    {
        try {
            $data['rpc_key'] = 'hjkHJKL:%^&*(653565GHJK';
            $objClient = new Client(config("chb.message_url") . '/rpc/RpcServer/index', false);
            $result = $objClient->rpc_server(array_merge($data, $param, ['param' => request()->param('')], ['client_domain' => $_SERVER['SERVER_NAME'] ?? '']));
        } catch (\Exception $e) {
            throw new \Exception('消息系统异常!');
        }
        if ($result['code'] != 200) {
            throw new \Exception('300' . $result['msg'], $result['code']);
        }
        return $result['data'];
    }

    /**
     * 消息接口RPC服务
     * @param array $param
     * @return mixed
     * @throws Exception
     */
    public function rpcPlatformResult(array $param = [])
    {
        try {
            $data['rpc_key'] = 'yhjk56789G%%HJK';
            $objClient = new Client(config("chb.platform_url") . '/rpc/RpcServer/index', false);
            $result = $objClient->rpc_server(array_merge($data, $param, ['param' => request()->param('')], ['client_domain' => $_SERVER['SERVER_NAME'] ?? '']));
        } catch (\Exception $e) {
            throw new \Exception('平台系统异常!');
        }
        if ($result['code'] != 200) {
            throw new \Exception('800:' . $result['msg'], $result['code']);
        }
        return $result['data'];
    }
}
