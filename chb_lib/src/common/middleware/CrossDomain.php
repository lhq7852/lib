<?php

namespace chb_lib\common\middleware;

use think\Response;

class CrossDomain
{
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {
        header('Access-Control-Allow-Origin: *');
        header('Access-Control-Max-Age: 1800');
        header('Cache-Control: no-cache');
        header('Access-Control-Allow-Methods: GET, POST, PATCH, PUT, DELETE');
        header('Access-Control-Allow-Headers: DNT,X-Mx-ReqToken,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Authorization,User-Token');
        if (strtoupper($request->method()) == "OPTIONS") {
            return Response::create(['code' => 100, 'msg' => 'error'], "json");
        }
        foreach (request()->param("") as &$v) { // 防止 eval 方法注入
            if (is_string($v) && (strchr($v, '@eval(') || strchr($v, '<?php'))) {
                return Response::create(['code' => 100, 'msg' => 'error'], "json");
            }
        }
        return $next($request);
    }
}
