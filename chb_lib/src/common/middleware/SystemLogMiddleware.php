<?php

declare(strict_types=1);

namespace chb_lib\common\middleware;

use chb_lib\common\system\SystemLog;
use think\Response;

class SystemLogMiddleware
{
    /**
     * 处理请求
     *
     * @param \think\Request $request
     * @param \Closure       $next
     * @return Response
     */
    public function handle($request, \Closure $next)
    {
        $response = $next($request);
        // 添加中间件执行代码
        (new SystemLog)->addLogQueue($response->getData());

        return $response;
    }
}
