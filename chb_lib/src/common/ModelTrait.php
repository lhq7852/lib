<?php

namespace chb_lib\common;

use chb_lib\common\exception\Err;
use think\Exception;
use think\facade\Db;

/**
 * Trait ModelTrait
 * @package chb_lib\common
 *
 * 公用数据库操作封装
 */
trait ModelTrait
{
    use UtilsTrait;
    protected $param = []; //对象传参属性
    protected $objWhere = null;
    protected $fieldList = [];
    protected $oldData = [];
    protected $newData = [];
    protected $currentMethod = '';

    /**
     * 获取单条数据（根据键名，默认主键）
     * @param $value
     * @param string|array $key 如果是数组，则key不可用。
     * @return array
     */
    public function getDataInfo($where = [])
    {
        if (!empty($param)) {
            $where = $this->filterSetWhere($param);
        }
        if (empty($where)) {
            return [];
        }
        $data = $this->alias($this->alias)->where($where)->findOrEmpty();
        return $data ? $data : [];
    }

    /**
     * 单表查询单条数据,可设置字段,排序
     *
     * @param array $param
     * @return array
     */
    public function getInfo($param = [])
    {
        $objWhere = $this->setWhereLike($param);

        if (!empty($param)) {
            $where = $this->filterSetWhere($param);
        }
        if (!empty($where)) {
            $objWhere = $objWhere->where($where);
        }
        $objWhere = $objWhere->order($this->getOrderSort(empty($param['order_type']) ? 0 : $param['order_type']));
        if (!empty($param['field'])) {
            $objWhere = $objWhere->field($param['field']);
        }
        return $objWhere->alias($this->alias)->findOrEmpty();
    }

    /**
     * 获取数据列表,无分页
     *
     * @param array $param
     * @return array
     */
    public function getDataList($param = [])
    {
        $objWhere = $this->setWhereLike($param);

        if (!empty($param)) {
            $where = $this->filterSetWhere($param);
        }
        if (!empty($where)) {
            $objWhere = $objWhere->where($where);
        }
        $objWhere = $objWhere->order($this->getOrderSort(empty($param['order_type']) ? 0 : $param['order_type']));
        if (!empty($param['field'])) {
            $objWhere = $objWhere->field($param['field']);
        }
        return $objWhere->alias($this->alias)->select()->toArray();
    }

    /**
     * 排序
     *
     * @param [type] int $param
     * @return string
     */
    public function getOrderSort(int $param = 0)
    {
        $alias = '';
        if (!empty($this->alias)) {
            $alias = $this->alias . '.';
        }
        if (!is_numeric($param) || empty($param)) {
            if (in_array('sort', $this->getModelFields())) {
                return $alias . 'sort asc';
            }
            return $alias . $this->getPK() . ' desc';
        }
        $arr_order = [
            1 => $alias . 'create_time asc',
            2 => $alias . 'create_time desc',
        ];
        if (!empty($this->orderByConfig)) { //如果有配置排序,用配置排序
            $arr_order = $this->orderByConfig;
        }
        return $arr_order[($param ?? 0)] ?? 'field desc';
    }

    /**
     * 获取数据列表,带分页
     *
     * @param array $param
     * @return array
     */
    public function getPageList($param = [])
    {
        $objWhere = $this->setWhereLike($param);
        if (!empty($param)) {
            $where = $this->filterSetWhere($param);
        }
        if (!empty($where)) {
            $objWhere = $objWhere->where($where);
        }
        $objWhere = $objWhere->order($this->getOrderSort(empty($param['order_type']) ? 0 : $param['order_type']));
        if (!empty($param['field'])) {
            $objWhere = $objWhere->field($param['field']);
        }
        if (!empty($this->param["first_page_time"])) {
            $objWhere = $objWhere->where("create_time", ">=", date("Y-m-d H:i:s", $this->param["first_page_time"]));
        }
        return $objWhere->alias($this->alias)->paginate($param['pageSize'] ?? 30)->toArray();
    }

    /**
     * 新增数据
     * @param $param
     * @return mixed|null
     * @throws \Exception
     */
    public function addData($param)
    {
        if (empty($param)) {
            return false;
        }
        $this->currentMethod = "add";
        $depth = $this->checkArrayDepth($param);
        $obj = $this->filterField($param)->beforeAction($param);
        if ($depth < 2) {
            $primary_id = $obj->insertGetId($param);
            if (empty($primary_id)) {
                Err::errorMsg(802);
            }
            $param = array_merge($param, [$this->pk => $primary_id]);
            $this->listenSort($param);
            $this->newData = $this->getDataInfo([$this->pk => $primary_id]);
            $this->listenAdd();

            return $primary_id;
        }
        return $obj->insertAll($param);
    }

    /**
     * 设置字段入库前的格式
     *
     * @return object
     */
    protected function beforeAction(&$param)
    {
        return $this;
    }

    /**
     * 删除数据
     * @param $param
     * @return int|array|bool
     */
    public function deleteData($param = [])
    {
        if (empty($param)) {
            return false;
        }
        $table_fields = $this->getModelFields();
        if (!empty($param)) {
            $where = $this->filterSetWhere($param);
        }
        if (empty($where)) {
            Err::errorMsg(800);
        }
        $this->oldData = $this->getDataInfo($where);
        if (in_array("delete_time", $table_fields)) {
            $data = ['delete_time' => date("Y-m-d H:i:s", request()->time())];
            $this->setCommonParam($data);
            $this->where($where)->update($data);
        } else {
            $this->where($where)->delete();
        }
        $this->listenDel();
        return true;
    }

    /**
     * 更新数据
     * @param $param
     * @return string
     */
    public function editData($param = [], $where = [])
    {
        if (empty($param)) {
            return false;
        }
        $this->currentMethod = "edit";
        $depth = $this->checkArrayDepth($param);
        $depth > 1 && Err::errorMsg(804);
        // 验证数据数组中是否包含主键ID
        !isset($param[$this->getPk()]) && empty($where) && Err::errorMsg(800);
        if (!empty($param[$this->getPk()])) {
            $where[$this->getPk()] = $param[$this->getPk()];
        }
        $this->oldData = $this->getDataInfo($where);
        $this->filterField($param)->beforeAction($param);
        $ret = $this->allowField($this->allow_filed_key ?? [])->where($where)->update($param); //返回更新成功影响的条数
        if ($ret === false) {
            Err::errorMsg(805);
        }
        $this->newData = $this->getDataInfo($where);

        $this->listenSort($param);
        $this->listenChildren($param);
        return $ret;
    }

    public function listenData()
    {
    }

    /**
     * 监听删除-连子级一起删,父级数据维护
     *
     * @return bool
     */
    protected function listenDel()
    {
        $table_fields = $this->getModelFields();
        if (!in_array("parent_id", $table_fields) || !in_array("children_ids", $table_fields)) {
            return true;
        }
        $oldParent = $this->whereFindInSet("children_ids", $this->oldData[$this->pk])->where($this->pk, "<>", $this->oldData[$this->pk])->select()->toArray(); //父级迁移 旧父级

        $arrChildren = array_filter(array_unique(explode(",", $this->oldData['children_ids'])));

        $editParent = [];
        foreach ($arrChildren as $id) {
            if (empty($id)) {
                continue;
            }
            foreach ($oldParent as $parent) {
                if (!empty($parent)) {
                    $editParent[$parent[$this->pk]][$this->pk] = $parent[$this->pk];
                    $children_ids = empty($editParent[$parent[$this->pk]]['children_ids']) ? $parent['children_ids'] : $editParent[$parent[$this->pk]]['children_ids'];
                    $editParent[$parent[$this->pk]]['children_ids'] = str_replace("," . $id, "", $children_ids);
                }
            }
        }
        if (!empty($editParent)) { //全部父级更新子级
            $this->batchEditData($editParent, 'children_ids');
        }
        $where[$this->pk] = $arrChildren; //全部子级一起删
        if (in_array("delete_time", $table_fields)) {
            $this->where($where)->update(['delete_time' => date("Y-m-d H:i:s", request()->time())]);
        } else {
            $this->where($where)->delete();
        }
        return true;
    }

    /**
     * 监听删除-父级删除,子级向上一级顶
     *
     * @return bool
     */
    protected function listenDelBequeath()
    {
        $table_fields = $this->getModelFields();
        if (!in_array("parent_id", $table_fields) || !in_array("children_ids", $table_fields)) {
            return true;
        }
        $oldParent = $this->whereFindInSet("children_ids", $this->oldData[$this->pk])->where($this->pk, "<>", $this->oldData[$this->pk])->select()->toArray(); //父级迁移 旧父级

        $arrChildren = array_filter(array_unique(explode(",", $this->oldData['children_ids'])));

        $editParent = [];
        foreach ($oldParent as $parent) {
            if (!empty($parent)) {
                $editParent[$parent[$this->pk]][$this->pk] = $parent[$this->pk];
                $children_ids = empty($editParent[$parent[$this->pk]]['children_ids']) ? $parent['children_ids'] : $editParent[$parent[$this->pk]]['children_ids'];
                $editParent[$parent[$this->pk]]['children_ids'] = str_replace("," . $this->oldData[$this->pk], "", $children_ids);
            }
        }
        if (!empty($editParent)) { //全部父级更新子级
            $this->batchEditData($editParent, 'children_ids');
        }
        $where[$this->pk] = $arrChildren; //一级子级向上顶
        $this->where("parent_id", $this->oldData[$this->pk])->update(['parent_id' => $this->oldData['parent_id']]);
        return true;
    }

    /**
     * 添加监听
     *
     * @return bool
     */
    protected function listenAdd()
    {
        $table_fields = $this->getModelFields();
        if (!in_array("parent_id", $table_fields) || !in_array("children_ids", $table_fields)) {
            return true;
        }
        //更新自身的子级
        $param["children_ids"] = "," . $this->newData[$this->pk] . ",";
        $this->where([$this->pk => $this->newData[$this->pk]])->update($param);
        if (empty($this->newData['parent_id'])) {
            return true;
        }
        //更新父级子级
        $parent = $this->getDataInfo([$this->pk => $this->newData['parent_id']]);
        if (empty($parent)) {
            $param['parent_id'] = 0; //父级不存在,维护数据
            $this->where([$this->pk => $this->newData[$this->pk]])->update($param);
            return true;
        }
        $parentParam['children_ids'] = $parent['children_ids'] . $this->newData[$this->pk] . ",";
        $this->where([$this->pk => $this->newData['parent_id']])->update($parentParam);

        $multiParent = $this->whereFindInSet("children_ids", $this->newData['parent_id'])
            ->where($this->pk, "<>", $this->newData['parent_id'])
            ->select()->toArray();

        if (!empty($multiParent)) {
            $editMultiData = [];
            foreach ($multiParent as $k => $value) {
                $editMultiData[$k][$this->pk] = $value[$this->pk];
                $editMultiData[$k]['children_ids'] = $value['children_ids'] . $this->newData[$this->pk] . ",";
            }

            $this->batchEditData($editMultiData, 'children_ids');
        }
        return true;
    }

    /**
     * 监听排序
     *
     * @param array $param
     * @return bool
     */
    protected function listenSort($param = [])
    {
        $table_fields = $this->getModelFields();
        if (!in_array("sort", $table_fields) || !isset($param['sort']) || !isset($param[$this->pk])) {
            return true;
        }
        //更新大于等于我的排序
        $this->where("sort", ">=", $param['sort'])->where($this->pk, "<>", $param[$this->pk])->update(['sort' => Db::raw("sort+1")]);
        return true;
    }

    /**
     * 监听子级维护子级数据
     *
     * @param array $param
     * @return bool
     */
    protected function listenChildren($param = [])
    {
        $table_fields = $this->getModelFields();
        if (!in_array("parent_id", $table_fields) || !in_array("children_ids", $table_fields) || !isset($param[$this->pk])) {
            return true;
        }
        if ($this->newData['parent_id'] == $this->oldData['parent_id']) {
            return true;
        }
        $oldParent = $this->getDataInfo([$this->pk => $this->oldData['parent_id']]); //父级迁移 旧父级
        $newParent = $this->getDataInfo([$this->pk => $this->newData['parent_id']]); //新父级
        $arrChildren = explode(',', $this->oldData['children_ids']);
        foreach ($arrChildren as $id) {
            if (empty($id)) {
                continue;
            }
            if (!empty($oldParent)) {
                $oldParent['children_ids'] = str_replace("," . $id, "", $oldParent['children_ids']);
            }
            if (!empty($newParent)) {
                $newParent['children_ids'] .= $id . ",";
            }
        }
        if (!empty($oldParent)) {
            $this->where($this->pk, $this->oldData['parent_id'])->update(['children_ids' => $oldParent['children_ids']]);
        }
        if (!empty($newParent)) {
            $this->where($this->pk, $this->newData['parent_id'])->update(['children_ids' => $newParent['children_ids']]);
        }
        return true;
    }

    /**
     * 批量更新
     * $param = [
     *  [
     *      id=>1,
     *     field=>5,
     *  ]
     * ]
     * $updateField 更新的字段名
     * @param [type] $param
     * @return bool
     */
    public function batchEditData($param = [], $updateField = '')
    {
        if (empty($param) || empty($updateField)) {
            Err::errorMsg(800);
        }
        $ids = array_column($param, $this->getPK());
        $fieldSql = " CASE " . $this->getPK() . " %s END";
        $str = " WHEN %s THEN '%s' ";
        $when_str = '';
        foreach ($param as $val) {
            if (!isset($val[$this->getPK()]) || !isset($val[$updateField])) {
                Err::errorMsg(810);
            }
            $when_str .= sprintf($str, $val[$this->getPK()], $val[$updateField]);
        }
        $updateData = sprintf($fieldSql, $when_str);
        $this->where([$this->getPK() => $ids])->update([$updateField => Db::raw($updateData)]);
        return true;
    }

    /**
     * 过滤非模型的字段
     *
     * @param array $param
     * @return object
     */
    protected function filterField(&$param = [])
    {
        if (empty($param)) {
            return $this;
        }
        $depth = $this->checkArrayDepth($param);
        $table_fields = $this->getModelFields();
        if ($depth < 2) {
            foreach ($param as $field => $value) {
                if (!in_array($field, $table_fields)) {
                    unset($param[$field]);
                }
            }
        } else {
            foreach ($param as $key => $arr) {
                foreach ($arr as $field => $value) {
                    if (!in_array($field, $table_fields)) {
                        unset($param[$key][$field]);
                    }
                }
            }
        }
        $this->setCommonParam($param);
        return $this;
    }

    /**
     * 设置公共参数
     *
     * @param array $param
     * @return object
     */
    protected function setCommonParam(&$param = [])
    {
        $depth = $this->checkArrayDepth($param);
        $table_fields = $this->getModelFields();
        if ($depth < 2) {
            if (in_array("create_user_no", $table_fields) && !empty(request()->user['user_no']) && $this->currentMethod == 'add') {
                $param['create_user_no'] = request()->user['user_no'] ?? '';
            }
            if (in_array("update_user_no", $table_fields) && !empty(request()->user['user_no'])) {
                $param['update_user_no'] = request()->user['user_no'] ?? '';
            }
            if (in_array("update_account_no", $table_fields) && !empty(request()->user['account_no'])) {
                $param['update_account_no'] = request()->user['account_no'] ?? '';
            }
            //自动设置公司编号,排除身份表
            if (in_array("organization_no", $table_fields) && !in_array($this->getPK(), ['identity_id', 'organization_id'])) {
                $param['organization_no'] = $param['organization_no'] ?? (request()->user['organization_no'] ?? '');
            }
        } else {
            foreach ($param as &$v) {
                if (in_array("create_user_no", $table_fields) && !empty(request()->user['user_no']) && $this->currentMethod == 'add') {
                    $v['create_user_no'] = request()->user['user_no'] ?? '';
                }
                if (in_array("update_user_no", $table_fields) && !empty(request()->user['user_no'])) {
                    $v['update_user_no'] = request()->user['user_no'] ?? '';
                }
                if (in_array("update_account_no", $table_fields) && !empty(request()->user['account_no'])) {
                    $v['update_account_no'] = request()->user['account_no'] ?? '';
                }
                //自动设置公司编号,排除身份表
                if (in_array("organization_no", $table_fields) && !in_array($this->getPK(), ['identity_id', 'organization_id'])) {
                    $v['organization_no'] = $v['organization_no'] ?? (request()->user['organization_no'] ?? '');
                }
            }
        }
        return $this;
    }

    /**
     * 过滤模型关联表不存在字段，并且按=条件组装where数组
     * @param array $array
     * @param array $where
     * @return array
     */
    public function filterSetWhere(array $array = [])
    {
        if (empty($array)) {
            return [];
        }
        $where = [];
        $table_fields = $this->getModelFields();
        foreach ($array as $field => $value) {
            if (!strchr($field, "|") && !in_array($field, $table_fields)) {
                continue;
            }
            if (empty($value) && !is_numeric($value)) {
                continue;
            }
            if (in_array($field, ['platform'])) {
                continue;
            }
            $where[$field] = $array[$field];
        }
        return $where;
    }

    /**
     * 设置模糊查询
     *
     * @param array $param
     * @return object
     */
    public function setWhereLike($param = [], &$objWhere = null)
    {
        $objWhere = $objWhere ?? $this;
        if (empty($param)) {
            return $objWhere;
        }
        $alias = '';
        if (!empty($this->alias)) {
            $alias = $this->alias . '.';
        }
        foreach ($param as $field => $value) {
            if (!empty($this->likeList[$field]) && !empty($value)) {
                $objWhere = $objWhere->whereLike($alias . $this->likeList[$field], "%" . $value . "%");
            }
        }
        return $objWhere;
    }

    /**
     * 获取主键名称
     * @return mixed
     */
    public function getPK()
    {
        return $this->pk;
    }

    /**
     * 获取数据表的字段
     *
     * @return array
     */
    public function getModelFields()
    {
        if (!empty($this->fieldList)) {
            return $this->fieldList;
        }
        $this->fieldList = $this->getTableFields();
        return $this->fieldList;
    }

    /**
     * 删除时间过滤
     */
    public function scopeDeleteTime($query)
    {
        $alias = empty($this->alias) ? '' : $this->alias . '.';
        if ($this->isDeleteTime && in_array("delete_time", $this->getModelFields())) {
            $query->alias($this->alias)->where($alias . "delete_time", "=", "");
        }
        return $query;
    }

    /**
     * 设置机构编号
     *
     * @param [type] $query
     * @return object|void
     */
    public function scopeOrganizationNo(&$query)
    {
        $alias = empty($this->alias) ? '' : $this->alias . '.';
        if (!empty(request()->user['organization_no']) && in_array("organization_no", $this->getModelFields())) {
            $query->alias($this->alias)->where($alias . "organization_no", "=", request()->user['organization_no']);
        }
        return $query;
    }
}
