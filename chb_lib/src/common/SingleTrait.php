<?php

/**
 * 单例,静态调用
 */

namespace chb_lib\common;

use chb_lib\common\exception\Err;

trait SingleTrait
{

    private static $initObj = [];

    protected $param = []; //对象传参属性

    /**
     * [静态]转 [单例访问对象]
     *
     * @param [type] $method
     * @param [type] $avg
     * @return void
     */
    public static function __callStatic($method = '', $avg = [])
    {
        if (strchr($method, '_')) {
            $objStatic = self::_callInitObj();
            $method = str_replace("_", "", $method);
            if (!method_exists($objStatic, $method)) {
                Err::errorMsg(453);
            }
            return call_user_func_array([$objStatic, $method], $avg);
        }
        Err::$data['method'] = __CLASS__ . " 不存在方法 " . $method;
        Err::errorMsg(455);
    }

    /**
     * 单例
     *
     * @param string $uuid
     * @param array $constructParam 构造函数参数
     * @return object
     */
    protected static function _callInitObj($uuid = '0')
    {
        if (!empty(static::$initObj[$uuid])) {
            return static::$initObj[$uuid];
        }
        static::$initObj[$uuid] = new static();
        return static::$initObj[$uuid];
    }

    /**
     * 设置传入的参数
     *
     * @param [type] $param
     * @return object
     */
    public function setParam($param = [])
    {
        $this->param = $param;
        return $this;
    }
}
