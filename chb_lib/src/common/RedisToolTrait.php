<?php

/**
 * 通用redis工具
 */

namespace chb_lib\common;

use chb_lib\common\exception\Err;
use think\facade\Cache;

trait RedisToolTrait
{

    protected $redisHook = [];
    protected $redisKey = null;
    protected $redisTime = 60 * 30;
    protected $forceRedisTime = null;
    protected $redisKeyName = null;
    protected $redisDb = 0;
    protected $isCache = true;
    protected $objRedis = [];
    protected $config = [];

    protected function getObjRedis()
    {
        return $this->selectRedisDb();
    }

    /**
     * 选择redis数据库
     *
     * @return object
     */
    protected function selectRedisDb()
    {
        $redisDb = $this->redisDb;
        if (!empty($this->objRedis[$this->getClassName()])) {
            if (!empty($this->config[$this->getClassName()])) { //配置存在 代码强制> 优先数据库配置 > 代码配置 > 默认配置
                $config = $this->config[$this->getClassName()];
                $redisDb = $config['select_db'];
                $this->isCache = $config['is_cache'] ? true : false;
                $this->redisTime = $this->forceRedisTime ?? ($config['redis_time'] ? ($config['redis_time'] == '24' ? $this->setRedisTimeDay() : $config['redis_time']) : $this->redisTime);
                $this->objRedis[$this->getClassName()]->select($redisDb);
            }
            return $this->objRedis[$this->getClassName()];
        }
        //获取数据库配置
        $config = $this->config[$this->getClassName()] = (BaseSystem::_getObject("SystemRedis"))::_getRedisConfig($this->getClassName())['redis_config'] ?? [];
        if (!empty($config)) { //配置存在 优先数据库配置 > 代码配置 > 默认配置
            $redisDb = $config['select_db'];
            $this->isCache = $config['is_cache'] ? true : false;
            $this->redisTime = $config['redis_time'] ? $config['redis_time'] : $this->redisTime;
        }
        $this->objRedis[$this->getClassName()] = Cache::store("redis")->handler();
        $this->objRedis[$this->getClassName()]->select($redisDb);
        return $this->objRedis[$this->getClassName()];
    }

    /**
     * 生成钩子key
     *
     * @param [type] $action
     * @param [type] $param
     * @return string
     */
    public function createHookKey($action, $param)
    {
        return $this->md5Key(func_get_args());
    }

    /**
     * 获取数据,经过redis
     * @param string $action 访问方法
     * @param array $param 参数 数组/字符串
     * @param false $default 是否穿透缓存 true / false
     * 使用方法
     *  if (!isset($this->redisHook[$this->createHookKey(__FUNCTION__, func_get_args())])) {
     *      return $this->getDataByRedisHook(__FUNCTION__, [$param], false); // func_get_args() == [$param]
     *  }
     * @return mixed
     */
    public function getDataByRedisHook($action, $param, $default = false)
    {
        if (!method_exists($this, $action)) {
            Err::errorMsg(480);
        }
        $this->selectRedisDb();
        $this->redisHook[$this->createHookKey($action, $param)] = 1; //记录访问次数
        if ($default || !$this->isCache) { // 是否穿透 || 是否缓存
            $this->delRedis();
            return call_user_func_array([$this, $action], $param);
        }
        $redisName = $this->getClassName() . $action . ':' . $this->md5Key(func_get_args());
        $ret = $this->setRedisKey($redisName)->getRedisArray();
        if (!empty($ret) && !$default) {
            return $ret;
        }
        $data = call_user_func_array([$this, $action], $param);
        if (is_bool($data)) {
            return $data;
        }
        $this->setRedisKey($redisName)->setRedisValue($data);
        return $data;
    }

    /**
     * 右边入队列
     */
    public function rPush($value)
    {
        return $this->getObjRedis()->rPush($this->getRedisKayName(), $value);
    }

    /**
     * 左边出队列
     */
    public function blPop()
    {
        if (empty($this->llen())) {
            return null;
        }
        $i = 0;
        while (true) {
            //从右边（rPush）入队，左边阻塞出队
            $data = $this->getObjRedis()->blPop($this->getRedisKayName(), 10);
            if ($data) {
                return $data;
            } else {
                sleep(1);
                ++$i;
            }
            if ($i >= 10) { //十秒超时
                Err::errorMsg(470);
            }
        }
    }

    /**
     * 检查队列长度
     */
    public function llen()
    {
        return $this->getObjRedis()->llen($this->getRedisKayName());
    }

    /**
     * Md5 加密一下key
     */
    public function md5Key($param, $default = false)
    {
        if (empty($param)) {
            return '';
        }
        if (is_array($param)) {
            return md5(json_encode($param));
        } else {
            if ($default === true) {
                return md5($param);
            }
            return $param;
        }
    }

    protected function getClassName()
    {
        if (!empty($this->redisKey)) {
            return $this->redisKey;
        }
        $arr = explode("\\", get_class());
        $name = end($arr);
        if (!empty($name)) {
            return $name . ":";
        }
        return 'RedisToolTrait:';
    }

    /**
     * 获取redis键名
     *
     * @return string
     */
    protected function getRedisKayName()
    {
        if (empty($this->redisKeyName)) {
            $this->redisKeyName = $this->getClassName();
        }
        return $this->redisKeyName;
    }

    /**
     * 设置redis的key
     */
    public function setRedisKey($redisKey)
    {
        $this->redisKeyName = $redisKey;
        return $this;
    }

    /**
     * 设置缓存时间
     */
    public function setRedisTime($redisTime)
    {
        $this->redisTime = $redisTime;
        return $this;
    }

    /**
     * 设置缓存时间-当天0时前有效
     */
    public function setRedisTimeDay()
    {
        $this->redisTime = strtotime(date("Y-m-d 23:59:39")) - request()->time();
        return $this;
    }

    /**
     * 读取字符串型的redis
     */
    public function getRedisArray()
    {
        $data = $this->getObjRedis()->get($this->getRedisKayName());
        if (!empty($data)) {
            return json_decode($data, true);
        }
        return null;
    }

    /**
     * 读取字符串型的redis
     */
    public function getRedisString()
    {
        return $this->getObjRedis()->get($this->getRedisKayName());
    }

    /**
     * 保存数组型的redis
     */
    public function setRedisValue($value = null)
    {
        if (empty($value)) {
            return true;
        }
        if (is_array($value)) {
            $saveValue = json_encode($value, JSON_UNESCAPED_UNICODE);
        } else {
            $saveValue = $value;
        }
        return $this->getObjRedis()->setex($this->getRedisKayName(), $this->redisTime, $saveValue);
    }

    /**
     * 删缓存
     *
     * @return bool
     */
    public function delRedis($string = '')
    {
        if (!empty($string)) {
            $keys = $this->getObjRedis()->keys($string);
            if (!empty($keys)) {
                $this->getObjRedis()->del($keys);
            }
            return true;
        }
        $keyName = $this->getRedisKayName();
        if (strchr($keyName, ":")) {
            $redisKey = explode(":", $keyName)[0] . ":";
        } else {
            $redisKey = $keyName;
        }
        $keys = $this->getObjRedis()->keys($redisKey . '*');
        if (!empty($keys)) {
            $this->getObjRedis()->del($keys);
        }
        return true;
    }
}
