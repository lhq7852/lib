<?php

namespace chb_lib\common;

use chb_lib\common\exception\Err;

/**
 * 公共控制器
 * @package chb_lib\common
 *
 */
trait ControllerTrait
{
    use UtilsTrait;

    /**
     * 有分页列表
     *
     * @return array
     */
    public function pageList()
    {
        return $this->server::_getPageList($this->getParam());
    }

    /**
     * 无分页列表
     *
     * @return array
     */
    function list() {
        return $this->server::_getDataList($this->getParam());
    }

    /**
     * 无分页列表-树形列表
     *
     * @return array
     */
    public function treeList()
    {
        return $this->server::_getTreeList($this->getParam());
    }

    /**
     * 多类型列表-工厂模式
     *
     * @return array
     */
    public function typeList()
    {
        return $this->server::_getTypeList($this->getParam());
    }

    /**
     * 详情信息
     *
     * @return array
     */
    public function info()
    {
        return $this->server::_getInfo($this->getParam());
    }

    /**
     * 添加
     *
     * @return array
     */
    public function add()
    {
        $data['id'] = (int) $this->server::_addData($this->getParam());
        return $data;
    }

    /**
     * 编辑
     *
     * @return array
     */
    public function edit()
    {
        return $this->server::_editData($this->getParam());
    }

    /**
     * 删除
     *
     * @return array
     */
    public function delete()
    {
        return $this->server::_deleteData($this->getParam());
    }

    /**
     * 批量修改
     *
     * @return array
     */
    public function batchEdit()
    {
    }
}
