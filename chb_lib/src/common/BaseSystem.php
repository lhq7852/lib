<?php

/**
 * 单例,静态调用
 */

namespace chb_lib\common;

use chb_lib\common\exception\Err;

class BaseSystem
{

    use SingleTrait;

    /**
     * 获取系统对象
     *
     * @param [type] $name
     * @return object
     */
    public function getObject($name)
    {
        $config = config("chb.system");
        $class = 'chb_rpc\system\\' . $name . "Rpc";
        if (!empty($config) && $config == 'user_platform') {
            $class = 'chb_lib\common\system\\' . $name;
        }
        return $class;
    }
}
