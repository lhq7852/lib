<?php

/**
 * 行为轨迹日志
 */

namespace chb_lib\common\log;

use think\facade\Db;
use think\facade\Queue;

class BehaviorLog
{

    /**
     * 轨迹日志-添加
     *
     * @return bool
     */
    public function addLog()
    {
        $actionType = $this->getActionType();
        $typeId = $this->getTypeId();
        $data = [
            'account_id' => request()->user['account_id'] ?? '',
            'domain' => $_SERVER['HTTP_HOST'] ?? '',
            'user_agent' => $_SERVER['HTTP_USER_AGENT'] ?? '',
            'user_token' => request()->header("User-Token"),
            'origin' => $_SERVER['HTTP_ORIGIN'] ?? '',
            'methods' => $_SERVER['REQUEST_METHOD'] ?? '',
            'module' => $_SERVER['REQUEST_URI'] ?? '',
            'ip' => request()->ip(),
            'api' => request()->param("api", "") ?? '',
            'type' => $typeId['type'],
            'type_id' => $typeId['type_id'],
            'action_type' => $actionType['action_type'],
            'action_name' => $actionType['action_name'],
            'create_user_no' => request()->user['user_no'] ?? '',
            'api' => request()->param("api", "") ?? '',
            'ip' => request()->ip(),
            'http_referer' => $_SERVER['HTTP_REFERER'] ?? ($_SERVER['HTTP_ORIGIN'] ?? ''),
            'user_agent' => $_SERVER['HTTP_USER_AGENT'] ?? '',
        ];
        Queue::push("chb_lib\common\job\LogJob@addBehaviorLog", $data, 'behavior:add_log');
        return true;
    }

    /**
     * 保存行为轨迹日志
     *
     * @param [type] $param
     * @return bool
     */
    public function saveLog($param)
    {
        if (empty($param['account_id'])) {
            return false;
        }
        $num = sprintf("%02d", $param['account_id'] % 10, '0');
        $table = "a_behavior_log_" . $num;
        Db::connect("log_database")->table($table)->insert($param);
        return true;
    }

    /**
     * 日志id
     *
     * @return array
     */
    protected function getTypeId()
    {
        $param = request()->param();
        foreach ($param as $k => $v) {
            if (strchr(strtolower($k), '_id')) {
                return [
                    'type' => $k,
                    'type_id' => $v,
                ];
            }
        }
        return [
            'type' => '',
            'type_id' => 0,
        ];
    }

    /**
     * 日志类型
     *
     * @return array
     */
    protected function getActionType()
    {
        $data['action_type'] = 'unknown';
        $data['action_name'] = '未知';
        if (strchr(strtolower(request()->param("api", "")), 'add')) {
            $data['action_type'] = 'add';
            $data['action_name'] = '添加';
        }
        if (strchr(strtolower(request()->param("api", "")), 'edit')) {
            $data['action_type'] = 'edit';
            $data['action_name'] = '编辑';
        }
        if (strchr(strtolower(request()->param("api", "")), 'info')) {
            $data['action_type'] = 'info';
            $data['action_name'] = '详情';
        }
        if (strchr(strtolower(request()->param("api", "")), 'list')) {
            $data['action_type'] = 'list';
            $data['action_name'] = '列表';
        }
        if (strchr(strtolower(request()->param("api", "")), 'delet')) {
            $data['action_type'] = 'delet';
            $data['action_name'] = '删除';
        }
        if (strchr(strtolower(request()->param("api", "")), 'count')) {
            $data['action_type'] = 'count';
            $data['action_name'] = '统计';
        }
        if (strchr(strtolower(request()->param("api", "")), 'switch')) {
            $data['action_type'] = 'switch';
            $data['action_name'] = '切换';
        }
        if (strchr(strtolower(request()->param("api", "")), 'index')) {
            $data['action_type'] = 'index';
            $data['action_name'] = '首页';
        }
        if (strchr(strtolower(request()->param("api", "")), 'login')) {
            $data['action_type'] = 'login';
            $data['action_name'] = '登录';
        }
        return $data;
    }
}
