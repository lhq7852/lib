<?php

/**
 * 业务日志
 * $objLog = (new OperationLog)->setModule($this->toUnderScore($this->className))
 *           ->setNewData($this->dataInfo)
 *           ->setOldData($this->oldDataInfo)
 *           ->setTypeId($this->getModel()->getPK())
 *           ->setActionType($method);
 *       if (!empty($this->titleField)) {
 *           $objLog = $objLog->setTitleField($this->titleField);
 *       }
 *       $objLog->addLog();
 */

namespace chb_lib\common\log;

use chb_lib\common\SingleTrait;
use chb_lib\common\UtilsTrait;
use think\facade\Db;

class OperationLog
{

    use UtilsTrait, SingleTrait;


    public $module; //模块

    public $newData;

    public $oldData;

    public $titleField;

    public $actionType;

    public $typeId;

    public $table;

    public $maxLeng = 200;

    public $arrActionType = [
        'batchadd' => '批量添加',
        'batchedit' => '批量编辑',
        'batchdelete' => '批量删除',
        'add' => '添加',
        'edit' => '修改',
        'delete' => '删除',
    ];



    /**
     * 更新后新数据
     *
     * @param array $field
     * @return object
     */
    public function setNewData($data)
    {
        $this->newData = $data;
        return $this;
    }

    /**
     * 更新前旧数据
     *
     * @param array $field
     * @return object
     */
    public function setOldData($data)
    {
        $this->oldData = $data;
        return $this;
    }

    /**
     * 模块
     *
     * @param array $field
     * @return object
     */
    public function setModule($module)
    {
        $this->module = $module;
        return $this;
    }

    /**
     * 操作类型
     *
     * @param array $field
     * @return object
     */
    public function setActionType($actionType)
    {
        $this->actionType = $actionType;
        return $this;
    }

    /**
     * 标题字段
     *
     * @param array $field
     * @return object
     */
    public function setTitleField($titleField)
    {
        $this->titleField = $titleField;
        return $this;
    }

    /**
     * 类型id
     *
     * @param array $field
     * @return object
     */
    public function setTypeId($typeId)
    {
        $this->typeId = $typeId;
        return $this;
    }
    /**
     * 检查日志表是否存在
     *
     * @return void
     */
    public function checkTable()
    {
        try {
            $this->table = $this->getTableName($this->module);
            $table = Db::query("show tables like '" . $this->module . "';");
            $table_log = Db::query("show tables like '" . $this->table . "';");
            if (empty($table) || empty($table_log)) {
                return false;
            }
        } catch (\Exception $e) {
            trace("检查日志表是否存在" . $e->getMessage() . $e->getFile() . $e->getLine());
        }
        return true;
    }

    protected function getTableName($table)
    {
        // $arrTable = explode("_", $table);
        // array_shift($arrTable);
        // $table = implode("_", $arrTable);
        return "a_" . $table . "_log";
    }

    /**
     * 获取日志表的备注
     *
     * @return array
     */
    public function getLogFieldText()
    {
        $data = Db::query("show full columns from " . $this->module . ";");
        $field = [];
        foreach ($data as $v) {
            if (empty($v['Comment'])) {
                continue;
            }
            $field[$v['Field']] = $this->get_content($v['Comment'],  "[", "]");
        }
        return $field;
    }

    /**
     * 日志标题
     *
     * @param array $field
     * @return string
     */
    public function getTitle($field = [])
    {
        if (!empty($this->titleField) && isset($field[$this->titleField])) {
            return $this->titleField;
        }
        foreach ($field as $k => $v) {
            if (strchr($k, '_name')) {
                return $k;
            }
        }
        return '';
    }

    /**
     * 操作类型
     *
     * @return string
     */
    public function getActionType()
    {
        if (empty($this->actionType)) {
            return '';
        }
        foreach ($this->arrActionType as $k => $v) {
            if (strchr(strtolower($this->actionType), $k)) {
                return $k;
            }
        }
        return '';
    }

    /**
     * 日志内容
     *
     * @param [type] $field
     * @return string
     */
    public function getContent($field)
    {
        if (empty($this->newData) || empty($this->oldData) || empty($field)) {
            return '';
        }
        $str = "";
        foreach ($this->newData as $key => $val) {
            if (empty($field[$key])) {
                continue;
            }
            if (!isset($this->oldData[$key])) {
                continue;
            }
            if (in_array($key, ['update_time'])) {
                continue;
            }
            if ($val != $this->oldData[$key]) {
                if (!empty($str)) {
                    $str .= "<br/>";
                }
                try {
                    $isJson = json_decode($str, true); //不为空即是json格式数据
                } catch (\Exception $e) {
                }
                if (!empty($isJson) || mb_strlen($val) > $this->maxLeng || mb_strlen($this->oldData[$key]) > $this->maxLeng) {
                    $str .= $field[$key];
                } else {
                    $str .= "将 " . $field[$key] . " 从 " . $this->oldData[$key] . " 改为 " . $val;
                }
            }
        }
        return $str;
    }

    /**
     * 添加日志
     *
     * @return bool
     */
    public function addLog()
    {
        $check = $this->checkTable();
        if (!$check) {
            return true;
        }
        $field = $this->getLogFieldText();
        if (empty($field)) {
            return false;
        }
        $actionType = $this->getActionType();
        if (empty($actionType)) {
            return false;
        }
        $data = [
            'title' => $this->newData[$this->getTitle($field)],
            'type' => $this->module,
            'type_id' => $this->newData[$this->typeId],
            'action_type' => $actionType,
            'action_name' => $this->arrActionType[$actionType],
            'text_log' => json_encode([
                'newData' => $this->newData,
                'oldData' => $this->oldData,
            ]),
            'content' => $this->getContent($field),
            'create_user_no' => request()->user['user_no'] ?? 0,
            'update_user_no' => request()->user['user_no'] ?? 0,
        ];
        Db::connect("log_database")->table($this->table)->insert($data);
        return true;
    }

    /**
     * 分页列表 
     *
     * @param array $param
     * @return array
     */
    public  function getPageList($param = [])
    {
        $table = $this->getTableName($param['type']);
        $data = Db::connect("log_database")->table($table)
            ->order("create_time desc")
            ->field("operation_log_id,title,type,type_id,action_type,action_name,content,create_user_no")
            ->paginate($param['pageSize'] ?? 50)
            ->toArray();
        return $data;
    }
}
