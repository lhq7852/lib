<?php

namespace chb_lib\common;

/**
 * 公共验证器
 * @package chb_lib\common
 *
 */
trait ValidateTrait
{

    use UtilsTrait;

    // protected $being = "alias|title"; //唯一字段检查

    /**
     * 编辑不检查必填
     *
     * @return object
     */
    public function sceneEditData()
    {
        if (!empty($this->rule)) {
            foreach ($this->rule as $field => $rule) {
                $this->remove($field, 'require'); //编辑的时候移除全部规则的必填
            }
        }
        return $this; //编辑默认 不验证参数
    }

    /**
     * 获取逻辑对象
     *
     * @return string|object
     */
    protected function getClass()
    {
        $file = explode('\\', get_class());
        $class_name = array_pop($file);
        $path = array_pop($file);
        return implode("\\", $file) . '\\' . str_replace("Validate", "", $class_name);
    }

    /**
     * 唯一检查
     *
     * @param [type] $value
     * @param [type] $rule
     * @param array $data
     * @return string|bool
     */
    protected function checkBeing($value, $rule, $data = [])
    {
        if (empty($this->being)) {
            return "请配置being属性";
        }
        $class = $this->getClass();
        $model = $class::_getModel();
        $pk = $model->getPK();
        $arr = explode("|", $this->being);
        foreach ($arr as $being) {
            if (strchr($being, "+")) {
                $a = explode("+", $being);
                $param = [];
                foreach ($a as $v) {
                    $param[$v] = $data[$v];
                }
                $check = $class::_getInfo($param, false);
            } else {
                $check = $class::_getDataInfo($value, $being, false);
            }
            if (!empty($check) && empty($data[$pk])) {
                return lang(812) ?? false;
            }
            if (!empty($check) && !empty($data[$pk]) && $data[$pk] != $check[$pk]) {
                return lang(812) ?? false;
            }
        }
        return true;
    }

    /**
     * 检查是否json格式
     */
    protected function checkJson($value, $rule, $data = [])
    {
        if (empty($value)) {
            return lang(811) ?? false;
        }
        $arrValue = json_decode($value, true);
        if (empty($arrValue)) {
            return lang(811) ?? false;
        }
        return true;
    }

    /**
     * 检查图片大小
     * rule : 2048KB,字段
     */
    protected function checkImageSize($value, $rule, $data = [])
    {
        if (strchr($rule, ",")) {
            $arr = explode(",", $rule);
            $size_kb = $arr[0] ?? 0;
            $field = $arr[1] ?? '';
        } else {
            $size_kb = $rule;
            $field = '';
        }
        $arrValue = json_decode($value, true);
        if (!empty($arrValue) && is_array($arrValue) && !empty($field)) {
            foreach ($arrValue as $v) {
                if (!empty($v[$field])) {
                    $size = $this->getFileSize($v[$field]);
                    if ($size >= $size_kb * 1024) {
                        return lang(813) ?? false;
                    }
                }
            }
            return true;
        }
        $size = $this->getFileSize($value);
        if ($size >= $size_kb * 1024) {
            return lang(813) ?? false;
        }
        return true;
    }
}
