<?php

/**
 * TOKEN令牌配置类
 */

namespace chb_lib\common\auth_code;

class TokenCfg
{
    //token版本号
    const VERSION = '1.0';

    //token过期时间，预留功能，目前先永不过期
    const EXPIRE_TIME = 2 * 3600;

    //加密类型动作参数
    const TOKEN_ENCODE = 1;

    //解密类型动作参数
    const TOKEN_DECODE = 2;

    //可逆加减密秘钥
    const AUTH_KEY = '7#wF&TpR*KvoV9z&ALSDFAStHUAFkkd';

    //sign校验秘钥
    const SIGN_KEY = '3v66HC&I4FE@s$Nf$HUAdms#aFp515e';

    //可逆加减密秘钥
    const AUTH_KEY2 = '7#wF&TpR*KvoV9z&ALSDFAHUAdmsSDFkkd';
}
