<?php

/**
 * 日志队列
 */

namespace chb_lib\common\job;

use chb_message\template\SmsTemplate;
use think\queue\Job;

class SmsTemplateJob
{

    /**
     * 添加的队列
     *
     * @param Job $job
     * @param [type] $data
     * @return void
     */
    public function addSmsTemplate(Job $job, $data)
    {
        try {
            if ($job->attempts() > 3) {
                $job->delete();
                //通过这个方法可以检查这个任务已经重试了几次了
            }
            (new SmsTemplate)->handleAddQueue($data);
            $job->delete();
        } catch (\Exception $e) {
            trace("队列验证失败:" . $e->getMessage() . json_encode($e->getTraceAsString()), 'chb_error');
            $delay = 10;
            $job->release($delay); //$delay为延迟时间
            echo $e->getMessage();
            echo $e->getTraceAsString();
        }
    }

    /**
     * 删除的队列
     *
     * @param Job $job
     * @param [type] $data
     * @return void
     */
    public function deleteSmsTemplate(Job $job, $data)
    {
        try {
            if ($job->attempts() > 3) {
                $job->delete();
                //通过这个方法可以检查这个任务已经重试了几次了
            }
            (new SmsTemplate)->handleDeleteQueue($data);
            $job->delete();
        } catch (\Exception $e) {
            trace("队列验证失败:" . $e->getMessage() . json_encode($e->getTraceAsString()), 'chb_error');
            $delay = 10;
            $job->release($delay); //$delay为延迟时间
            echo $e->getTraceAsString();
        }
    }

    /**
     * 编辑的队列
     *
     * @param Job $job
     * @param [type] $data
     * @return void
     */
    public function editSmsTemplate(Job $job, $data)
    {
        try {
            if ($job->attempts() > 3) {
                $job->delete();
                //通过这个方法可以检查这个任务已经重试了几次了
            }
            (new SmsTemplate)->handleEditQueue($data);
            $job->delete();
        } catch (\Exception $e) {
            trace("队列验证失败:" . $e->getMessage() . json_encode($e->getTraceAsString()), 'chb_error');
            $delay = 10;
            $job->release($delay); //$delay为延迟时间
            echo $e->getTraceAsString();
        }
    }

}
