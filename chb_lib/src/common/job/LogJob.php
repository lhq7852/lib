<?php

/**
 * 日志队列
 */

namespace chb_lib\common\job;

use chb_lib\common\log\BehaviorLog;
use chb_lib\common\system\SystemLog;
use think\queue\Job;

class LogJob
{

    /**
     * 添加日志的队列-系统
     *
     * @param Job $job
     * @param [type] $data
     * @return void
     */
    public function addLog(Job $job, $data)
    {
        try {
            $job->delete();
            (new SystemLog)->addLog($data);
        } catch (\Exception $e) {
            echo $e->getMessage() . $e->getTraceAsString();
        }
    }

    /**
     * 添加日志的队列-行为轨迹
     *
     * @param Job $job
     * @param [type] $data
     * @return void
     */
    public function addBehaviorLog(Job $job, $data)
    {
        try {
            $job->delete();
            (new BehaviorLog)->saveLog($data);
        } catch (\Exception $e) {
            echo $e->getMessage() . $e->getTraceAsString();
        }
    }

    public function test(Job $job, $data)
    {
        try {
            if ($job->attempts() > 3) {
                //通过这个方法可以检查这个任务已经重试了几次了
            }

            //如果任务执行成功后 记得删除任务，不然这个任务会重复执行，直到达到最大重试次数后失败后，执行failed方法
            $job->delete();

            // 也可以重新发布这个任务
            $delay = 10;
            $job->release($delay); //$delay为延迟时间
        } catch (\Exception $e) {
        }
    }

    public function failed($data)
    {
        // $job->delete();
        trace("队列失败的参数:", json_encode($data));
    }
}
