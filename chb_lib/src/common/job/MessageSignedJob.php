<?php

/**
 * 日志队列
 */

namespace chb_lib\common\job;

use chb_message\message_signed\MessageSigned;
use think\queue\Job;

class MessageSignedJob
{

    /**
     * 添加的队列
     *
     * @param Job $job
     * @param [type] $data
     * @return void
     */
    public function addSmsSign(Job $job, $data)
    {
        try {
            if ($job->attempts() > 3) {
                $job->delete();
                //通过这个方法可以检查这个任务已经重试了几次了
            }
            (new MessageSigned)->handleAddQueue($data);
            $job->delete();
        } catch (\Exception $e) {
            trace("队列验证失败:" . $e->getMessage() . json_encode($e->getTraceAsString()), 'chb_error');
            $delay = 10;
            $job->release($delay); //$delay为延迟时间
            echo $e->getMessage();
            echo $e->getTraceAsString();
        }
    }

    /**
     * 删除的队列
     *
     * @param Job $job
     * @param [type] $data
     * @return void
     */
    public function deleteSmsSign(Job $job, $data)
    {
        try {
            if ($job->attempts() > 3) {
                $job->delete();
                //通过这个方法可以检查这个任务已经重试了几次了
            }
            (new MessageSigned)->handleDeleteQueue($data);
            $job->delete();
        } catch (\Exception $e) {
            trace("队列验证失败:" . $e->getMessage() . json_encode($e->getTraceAsString()), 'chb_error');
            $delay = 10;
            $job->release($delay); //$delay为延迟时间
            echo $e->getTraceAsString();
        }
    }

    /**
     * 编辑的队列
     *
     * @param Job $job
     * @param [type] $data
     * @return void
     */
    public function editSmsSign(Job $job, $data)
    {
        try {
            if ($job->attempts() > 3) {
                $job->delete();
                //通过这个方法可以检查这个任务已经重试了几次了
            }
            (new MessageSigned)->handleEditQueue($data);
            $job->delete();
        } catch (\Exception $e) {
            trace("队列验证失败:" . $e->getMessage() . json_encode($e->getTraceAsString()), 'chb_error');
            $delay = 10;
            $job->release($delay); //$delay为延迟时间
            echo $e->getTraceAsString();
        }
    }

}
