<?php

/**
 * excel 管理
 */

namespace app\lib\common\excel;

class ExcelManage
{

    public $max_row = 1000;

    public $file_path = ''; //相对路径
    public $file_type = '';
    public $date_letter = [];
    public $data_format = "Y-m-d";
    public $field_key = [];
    protected $readResult;

    public function __construct()
    {
        header("content-type:text/html;charset=utf-8");
    }

    /**
     * 设置最大行数
     *
     * @return object
     */
    public function setMaxRow($row = 1000)
    {
        $this->max_row = $row;
        return $this;
    }

    /**
     * 本地上传
     *
     * @return object
     */
    public function local_file()
    {
        $file = request()->file('file');
        //获取文件路径
        $this->file_path = runtime_path() . "storage" . DIRECTORY_SEPARATOR . \think\facade\Filesystem::putFile('excel', $file);
        $this->file_type = \PHPExcel_IOFactory::identify($this->file_path);
        return $this;
    }

    /**
     * 设置某一列的数据为时间格式
     *
     * @param array $date_letter
     * @param string $data_format
     * @return object
     */
    public function setLetter($date_letter = [], $data_format = 'Y-m-d')
    {
        $this->date_letter = $date_letter;
        $this->data_format = $data_format;
        return $this;
    }

    public function setFieldKey($field_key = [])
    {
        $this->field_key = $field_key;
        return $this;
    }

    /**
     * 读取excel方法二
     *
     * @return object
     */
    public function excelRead()
    {
        if (empty($this->file_path)) {
            throw new \Exception("请确认文件地址", 500);
        }
        //加载excel文件
        $objPHPExcelReader = \PHPExcel_IOFactory::load($this->file_path);
        $sheet = $objPHPExcelReader->getSheet(0); // 读取第一个工作表(编号从 0 开始)
        $this->readResult = $sheet->toArray(); //该方法读取不到图片，图片需单独处理
        $this->sheet = $sheet;
        return $this;
    }

    /**
     * 读取结果
     *
     * @return array
     */
    public function getReadResult()
    {
        unset($this->readResult[0]);
        return $this->readResult;
    }

    /**
     * 读取excel数据-方法一
     *
     * @return array
     */
    public  function read_excel_first_sheet()
    {
        if (empty($this->file_path)) {
            throw new \Exception("请确认文件地址", 500);
        }
        if (empty($this->file_type)) {
            throw new \Exception("请确认文件类型", 500);
        }
        $reader = \PHPExcel_IOFactory::createReader($this->file_type);
        //载入excel文件
        $excel = $reader->load($this->file_path);
        //读取第一张表
        $sheet = $excel->getSheet(0);
        //获取总行数
        $row_num = $sheet->getHighestRow();
        if ($row_num > $this->max_row) {
            throw new \Exception("允许读取最大条数：" . $this->max_row);
        }
        //获取总列数
        $col_num = $sheet->getHighestColumn();
        $data = []; //数组形式获取表格数据
        $letter = "A B C D E F G H I J K L M N O P Q R S T U V W S Y Z";
        $arr_letter = explode(" ", $letter);
        for ($i = 2; $i <= $row_num; $i++) {
            foreach ($arr_letter as $l) {
                if (in_array($l, $this->date_letter)) { //时间格式
                    $data[$i][($this->field_key[$l] ?? $l)]  = date($this->data_format, \PHPExcel_Shared_Date::ExcelToPHP($sheet->getCell($l . $i)->getValue())) ?? '';
                } else {
                    $data[$i][($this->field_key[$l] ?? $l)]  = $sheet->getCell($l . $i)->getValue() ?? '';
                }
                if ($l == $col_num) {
                    break;
                }
            }
        }
        return array_values($data);
    }
}
