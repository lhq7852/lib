<?php

/**
 * 导出CSV格式，一次实例化一次使用
 * Created by PhpStorm.
 * User: strongLH
 * Date: 2020/12/3
 * Time: 17:44
 */

namespace app\lib\common\excel;

use chb_lib\common\utils\OssTool;

class ExportCsvServer
{

    protected $fileName;
    protected $openFile;
    protected $title;
    protected $titleConfig;
    protected $toPath;

    /**
     * 执行导出
     * @param $type
     * @param array $titleKey
     * @param array $list
     * @return mixed
     * @throws \Exception
     */
    public static function queryExport($type, $titleKey, $list = [])
    {
        if (!is_array($titleKey) && strtolower($titleKey) != 'all') {
            throw new \Exception("请对照导出文件表头设置导出的表头KEY", '000000');
        }
        $titleKey = is_array($titleKey) ? $titleKey : [];
        $obj = (new self())->createCsv($type, $type);
        $obj->setTitle($type, $titleKey);
        foreach ($list as $item) {
            $obj->setValue($item);
        }
        $url = $obj->getResult();
        return $url;
    }

    protected function createRuntimeFile($fileType, $fileName)
    {
        $path = runtime_path() . "storage/export_csv/"  . $fileType . "/" . date("Ym") . "/" . date("d") . "/";
        if (!is_dir($path)) {
            mkdir($path, 0777, true);
        }
        return $path . $fileName . date("His") . rand(10000, 99999) . '.csv';
    }

    /**
     * 创建csv文件
     * @param $fileName
     * @return $this
     */
    public function createCsv($fileType, $fileName)
    {
        $this->fileName = $this->createRuntimeFile($fileType, $fileName);
        $this->toPath = explode("storage/", $this->fileName)[1];
        $this->openFile = fopen($this->fileName, "a");
        fprintf($this->openFile, chr(0xEF) . chr(0xBB) . chr(0xBF));
        return $this;
    }

    /**
     * @param array $arrTitle
     * [
     *  'A' => '标题',
     *  'B' => '结果',
     * ]
     * @return $this
     * @throws \Exception
     */
    public function setDiyTitle(array $arrTitle = [])
    {
        if (empty($arrTitle)) {
            throw new \Exception("请设置导出Excel文件表头", 300001);
        }
        $this->titleConfig = $arrTitle;
        $title = array_values($arrTitle);
        $this->title = $title;
        fputcsv($this->openFile, $title); //写标题
        return $this;
    }

    /**
     * 设置表头
     * @param $type
     * @param $titleKey
     * @return $this
     * @throws \Exception
     */
    public function setTitle($type, array $titleKey = [])
    {
        $titleConfig = config("export_title." . $type);
        if (empty($titleKey)) {
            $this->titleConfig = $titleConfig;
            $title = array_values($this->titleConfig);
        }
        if (!empty($titleKey) && !empty($titleConfig)) {
            $title = [];
            foreach ($titleConfig as $key => $value) {
                if (in_array($key, $titleKey)) { //检查key是否在要导出的字段里
                    $this->titleConfig[$key] = $value;
                    $title[] = $value;
                }
            }
        }

        if (empty($title)) {
            throw new \Exception("请设置导出Excel文件表头", 300001);
        }
        $this->title = $title;
        fputcsv($this->openFile, $title); //写标题
        return $this;
    }

    /**
     * 设置csv表数据
     * @param array $param
     * @return $this
     */
    public function setValue(array $param)
    {
        $data = [];
        foreach ($this->titleConfig as $key => $value) {
            //检查key是否在要导出的字段里
            $data[] = $param[$key] ?? '';
        }
        fputcsv($this->openFile, $data);
        return $this;
    }

    /**
     * 获取导出文件结果
     * @return mixed
     * @throws \Exception
     */
    public function getResult()
    {
        fclose($this->openFile);
        $oss_tool = new OssTool();
        if ($oss_tool->uploadFile($this->toPath, $this->fileName)) {
            $res = $oss_tool->getDownLoadUrl($this->toPath);
            if (is_file($this->fileName)) {
                unlink($this->fileName);
            }
            return $res;
        }
        throw new \Exception("导出失败");
    }

    public function __destruct()
    {
        // TODO: Implement __destruct() method.
        try {
            if (is_file($this->fileName)) {
                fclose($this->openFile);
                unlink($this->fileName);
            }
        } catch (\Exception $e) {
        }
    }
}
