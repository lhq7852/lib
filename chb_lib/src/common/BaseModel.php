<?php

/**
 * 模型
 */

namespace chb_lib\common;

use chb_lib\common\ModelTrait;
use chb_lib\common\SingleTrait;
use think\facade\Db;

class BaseModel
{
    use ModelTrait, SingleTrait;

    protected $connection = ''; //数据库链接

    protected $name = ''; //数据库表名
    protected $alias = 'a'; //数据库表名-别名
    protected $pk = 'id'; //主键字段名

    //默认隐藏删除时间
    protected $hidden = ['delete_time'];
    protected $globalScope = ['deleteTime', 'organizationNo'];
    protected $isDeleteTime = true; //是否全局过滤delete_time : true 是 false 否
    //允许写入的字段
    protected $allow_filed_key = [];

    /**
     * 访问Db对象的方法
     *
     * @param [type] $name
     * @param [type] $arguments
     * @return void|object|array|string
     */
    public function __call($name, $arguments)
    {
        if (method_exists($this->Db(), $name)) {
            $query = $this->Db();
            $this->before_select($query, $name);
            return call_user_func_array([$query, $name], $arguments);
        }
    }

    /**
     * 链接数据库
     *
     * @return object
     */
    protected function Db()
    {
        return Db::connect($this->connection)->name($this->name);
    }

    /**
     * 查询前事件
     *
     * @param [type] $query
     * @return void
     */
    protected function before_select($query, $name = '')
    {
        if (!empty($this->globalScope) && !strchr(strtolower($name), "insert")) {
            foreach ($this->globalScope as $value) {
                $method = "scope" . strtoupper($value);
                if (method_exists($this, $method)) {
                    $this->$method($query);
                }
            }
        }
        if (!empty($this->allow_filed_key)) {
            $query->field($this->allow_filed_key);
        }
    }

    /**
     * 获取表字段
     *
     * @return void|array
     */
    protected function getTableFields()
    {
        return $this->Db()->getTableFields();
    }

    /**
     * 设置全局过滤删除时间
     *
     * @param boolean $default
     * @return object
     */
    public function setIsDeleteTime($default = false)
    {
        $this->isDeleteTime = $default ?? false;
        return $this;
    }

    /**
     * 设置链接地址
     *
     * @param boolean $default
     * @return object
     */
    public function setConnection($config = null)
    {
        $this->connection = $config ?? '';
        return $this;
    }

    /**
     * 设置允许保存的字段
     *
     * @param array $param
     * @return object
     */
    protected function allowField($param = [])
    {
        if (empty($param) || !is_array($param)) {
            return $this;
        }
        $this->allow_filed_key = $param;
        return $this;
    }

    /**
     * 获取表名
     */
    public function getName()
    {
        return $this->name;
    }
}
