<?php

namespace chb_lib\common;

use chb_lib\common\exception\Err;

/**
 * 路由工厂
 * @package chb_lib\common
 *
 * 公用逻辑封装
 */
trait RouteTrait
{

    protected $msg = 'success';

    /**
     * 工厂分配路由
     * api 路由
     * version 版本
     * appName 应用
     * @return mixed
     * @throws Exception
     */
    private function goFactory($appName = '')
    {
        $route = request()->param("api");
        if (empty($route)) {
            Err::errorMsg(404);
        }
        $version = request()->param("version", 1);
        $arrRoute = explode(".", $route);
        if (count($arrRoute) != 3) {
            Err::errorMsg(405);
        }
        $version = $version > 1 ? "\V" . $version . "\\" : '\\';
        $class = "app\\" . strtolower($appName) . "\controller\\" . strtolower($arrRoute[0]) . $version . ucfirst($arrRoute[1]);
        if (!class_exists($class)) {
            Err::errorMsg(406);
        }
        $objClass = new $class();
        $method = $arrRoute[2];
        if (!method_exists($objClass, $method)) {
            Err::errorMsg(407);
        }
        $ret = $objClass->$method();
        $this->msg = $objClass->msg ?? $this->msg;
        return $ret;
    }

    /**
     * RPC 工厂分配路由
     *
     * @param [type] $route
     * @param integer $version
     * @return void
     */
    private function goRpcFactory($param)
    {
        $rpc_key = (BaseSystem::_getObject("SystemConfig"))::_getKeyConfig(config("chb.system"));
        if (!$param['rpc_key'] || $param['rpc_key'] != $rpc_key) {
            Err::errorMsg(408);
        }
        $route = $param['rpc'];
        if (empty($route)) {
            Err::errorMsg(404);
        }
        $version = request()->param("version", 1);
        $arrRoute = explode(".", $route);
        if (count($arrRoute) != 3) {
            Err::errorMsg(405);
        }
        $version = $version > 1 ? "\V" . $version . "\\" : '\\';
        $class = "app\\rpc\controller\\" . strtolower($arrRoute[0]) . $version . ucfirst($arrRoute[1]);
        if (!class_exists($class)) {
            Err::errorMsg(406);
        }
        $objClass = new $class();
        $method = $arrRoute[2];
        if (!method_exists($objClass, $method)) {
            Err::errorMsg(407);
        }
        return $objClass->$method($param);
    }
}
