<?php

namespace chb_user\union;

use chb_lib\common\BaseSystem;
use chb_lib\common\RedisToolTrait;
use chb_lib\common\ServiceTrait;

class UnionBookPerson extends UnionService
{

    use RedisToolTrait, ServiceTrait;

    /**
     * 获取联盟增量数据
     *
     * @param array $param
     * @return array|void
     */
    public function getUnionIncrementList($param = [])
    {
        $data = $this->getModel()->getUnionIncrementList($param);
        $config = (BaseSystem::_getObject("SystemConfig"))::_getKeyConfig("union_config");
        $data = $this->jsonEncrypt($data, $config['public_key'], request()->time());
        return $data;
    }

    /**
     * 更新联盟
     *
     * @param [type] $url 请求地址
     * @param [type] $param 请求参数
     * @return bool
     */
    public function updateData($url, $param)
    {
        $check = $this->getModel()->count();
        if (empty($check)) {
            $param['start_time'] = '';
        }
        $ret = $this->request($url, $param);
        if (empty($ret) || empty($ret['data'])) {
            return true;
        }
        $ret = $this->decryptData($ret['data']['data'], $ret['data']['time']);
        if (empty($ret['data']) || !is_array($ret['data'])) {
            return true;
        }
        $add = [];
        foreach ($ret['data'] as $v) {
            $info = $this->getModel()->getDataInfo(['user_no' => $v['user_no']]);
            $diff = array_diff($info, $v);
            if (!empty($diff)) {
                $this->getModel()->where("user_no", $v['user_no'])->update($v);
            }
            if (empty($info)) {
                $add[] = $v;
            }
        }
        if (!empty($add)) {
            $this->getModel()->insertAll($add);
        }
        echo "同步联盟个人实名成功" . PHP_EOL;
        return true;
    }

    /**
     * 数据加入联盟
     */
    public function addUnion($param = [])
    {
        $time = request()->time();
        $config = (BaseSystem::_getObject("SystemConfig"))::_getKeyConfig("union_config");
        $card_type = empty($param['card_type']) ? '' : $this->jsonEncrypt($param['card_type'], $config['public_key'], $time);
        $card_no = empty($param['card_no']) ? '' : $this->jsonEncrypt($param['card_no'], $config['public_key'], $time);
        $card_name = empty($param['card_name']) ? '' : $this->jsonEncrypt($param['card_name'], $config['public_key'], $time);
        $facade_image = empty($param['facade_image']) ? '' : $this->jsonEncrypt($param['facade_image'], $config['public_key'], $time);
        $country_image = empty($param['country_image']) ? '' : $this->jsonEncrypt($param['country_image'], $config['public_key'], $time);
        $data = [
            'phone_account_no' => md5($param['phone']),
            'email_account_no' => md5($param['email']),
            'user_no' => $param['user_no'],
            'card_type' => $card_type['data'] ?? '',
            'card_no' => $card_no['data'] ?? '',
            'card_name' => $card_name['data'] ?? '',
            'facade_image' => $facade_image['data'] ?? '',
            'country_image' => $country_image['data'] ?? '',
            'union_state' => 1,
            'create_time' => date("Y-m-d H:i:s", $time),
        ];
        $this->getModel()->addData($data);
        return true;
    }

    /**
     * 查询联盟数据
     */
    public function getUnion($param = [])
    {
        $info = $this->getInfo(['user_no' => $param['user_no']], false);
        if (empty($info)) {
            return [];
        }
        $config = (BaseSystem::_getObject("SystemConfig"))::_getKeyConfig("union_config");
        $info['card_type'] = empty($param['card_type']) ? '' : $this->jsonDecrypt($info['card_type'], $config['public_key'], $info['create_time']);
        $info['card_no'] = empty($param['card_no']) ? '' : $this->jsonDecrypt($info['card_no'], $config['public_key'], $info['create_time']);
        $info['card_name'] = empty($param['card_name']) ? '' : $this->jsonDecrypt($info['card_name'], $config['public_key'], $info['create_time']);
        $info['facade_image'] = empty($param['facade_image']) ? '' : $this->jsonDecrypt($info['facade_image'], $config['public_key'], $info['create_time']);
        $info['country_image'] = empty($param['country_image']) ? '' : $this->jsonDecrypt($info['country_image'], $config['public_key'], $info['create_time']);
        return $info;
    }

}
