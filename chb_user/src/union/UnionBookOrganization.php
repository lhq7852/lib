<?php

namespace chb_user\union;

use chb_lib\common\BaseSystem;
use chb_lib\common\RedisToolTrait;
use chb_lib\common\ServiceTrait;

class UnionBookOrganization extends UnionService
{

    use RedisToolTrait, ServiceTrait;

    /**
     * 获取联盟增量数据
     *
     * @param array $param
     * @return array|void
     */
    public function getUnionIncrementList($param = [])
    {
        $data = $this->getModel()->getUnionIncrementList($param);
        $config = (BaseSystem::_getObject("SystemConfig"))::_getKeyConfig("union_config");
        $data = $this->jsonEncrypt($data, $config['public_key'], request()->time());
        return $data;
    }

    /**
     * 更新联盟
     *
     * @param [type] $url 请求地址
     * @param [type] $param 请求参数
     * @return bool
     */
    public function updateData($url, $param)
    {
        $check = $this->getModel()->count();
        if (empty($check)) {
            $param['start_time'] = '';
        }
        $ret = $this->request($url, $param);
        if (empty($ret) || empty($ret['data'])) {
            return true;
        }
        $ret = $this->decryptData($ret['data']['data'], $ret['data']['time']);
        if (empty($ret['data']) || !is_array($ret['data'])) {
            return true;
        }
        $add = [];
        foreach ($ret['data'] as $v) {
            $info = $this->getModel()->getDataInfo(['organization_no' => $v['organization_no']]);
            $diff = array_diff($info, $v);
            if (!empty($diff)) {
                $this->getModel()->where("organization_no", $v['organization_no'])->update($v);
            }
            if (empty($info)) {
                $add[] = $v;
            }
        }
        if (!empty($add)) {
            $this->getModel()->insertAll($add);
        }
        echo "同步联盟机构实名成功" . PHP_EOL;
        return true;
    }

    /**
     * 数据加入联盟
     */
    public function addUnion($param = [])
    {
        $time = request()->time();
        $config = (BaseSystem::_getObject("SystemConfig"))::_getKeyConfig("union_config");
        $legal_person_name = empty($param['legal_person_name']) ? '' : $this->jsonEncrypt($param['legal_person_name'], $config['public_key'], $time);
        $legal_identity_card_no = empty($param['legal_identity_card_no']) ? '' : $this->jsonEncrypt($param['legal_identity_card_no'], $config['public_key'], $time);
        $organization_name = empty($param['organization_name']) ? '' : $this->jsonEncrypt($param['organization_name'], $config['public_key'], $time);
        $business_license_image = empty($param['business_license_image']) ? '' : $this->jsonEncrypt($param['business_license_image'], $config['public_key'], $time);
        $credit_type = empty($param['credit_type']) ? '' : $this->jsonEncrypt($param['credit_type'], $config['public_key'], $time);
        $credit_code = empty($param['credit_code']) ? '' : $this->jsonEncrypt($param['credit_code'], $config['public_key'], $time);
        $certificate_image = empty($param['certificate_image']) ? '' : $this->jsonEncrypt($param['certificate_image'], $config['public_key'], $time);
        $data = [
            'phone_account_no' => empty($param['phone']) ? '' : md5($param['phone']),
            'email_account_no' => empty($param['email']) ? '' : md5($param['email']),
            'user_no' => $param['user_no'],
            'legal_person_name' => $legal_person_name['data'] ?? '',
            'legal_identity_card_no' => $legal_identity_card_no['data'] ?? '',
            'organization_no' => $param['organization_no'],
            'organization_name' => $organization_name['data'] ?? '',
            'business_license_image' => $business_license_image['data'] ?? '',
            'credit_type' => $credit_type['data'] ?? '',
            'credit_code' => $credit_code['data'] ?? '',
            'certificate_image' => $certificate_image['data'] ?? '',
            'union_state' => 1,
            'create_time' => date("Y-m-d H:i:s", $time),
        ];
        $this->getModel()->addData($data);
        return true;
    }

    /**
     * 查询联盟数据
     */
    public function getUnion($param = [])
    {
        $list = $this->getDataList(['user_no' => $param['user_no']], false);
        if (empty($list)) {
            return [];
        }
        $config = (BaseSystem::_getObject("SystemConfig"))::_getKeyConfig("union_config");
        foreach ($list as &$info) {
            $info['legal_person_name'] = empty($param['legal_person_name']) ? '' : $this->jsonDecrypt($info['legal_person_name'], $config['public_key'], $info['create_time']);
            $info['legal_identity_card_no'] = empty($param['legal_identity_card_no']) ? '' : $this->jsonDecrypt($info['legal_identity_card_no'], $config['public_key'], $info['create_time']);
            $info['organization_name'] = empty($param['organization_name']) ? '' : $this->jsonDecrypt($info['organization_name'], $config['public_key'], $info['create_time']);
            $info['business_license_image'] = empty($param['business_license_image']) ? '' : $this->jsonDecrypt($info['business_license_image'], $config['public_key'], $info['create_time']);
            $info['credit_type'] = empty($param['credit_type']) ? '' : $this->jsonDecrypt($info['credit_type'], $config['public_key'], $info['create_time']);
            $info['credit_code'] = empty($param['credit_code']) ? '' : $this->jsonDecrypt($info['credit_code'], $config['public_key'], $info['create_time']);
            $info['certificate_image'] = empty($param['certificate_image']) ? '' : $this->jsonDecrypt($info['certificate_image'], $config['public_key'], $info['create_time']);
        }
        return $list;
    }
}
