<?php

namespace chb_user\union;

use chb_lib\common\BaseSystem;
use chb_lib\common\RedisToolTrait;
use chb_lib\common\ServiceTrait;

class UnionMember extends UnionService
{

    use RedisToolTrait, ServiceTrait;

    /**
     * 获取联盟增量数据
     *
     * @param array $param
     * @return array|void
     */
    public function getUnionMemberIncrementList($param = [])
    {
        $data = $this->getModel()->getUnionMemberIncrementList($param);
        $config = (BaseSystem::_getObject("SystemConfig"))::_getKeyConfig("union_config");
        $data = $this->jsonEncrypt($data, $config['public_key'], request()->time());
        return $data;
    }

    /**
     * 更新联盟成员
     *
     * @param [type] $url 请求地址
     * @param [type] $param 请求参数
     * @return bool
     */
    public function updateData($url, $param)
    {
        $check = $this->getModel()->count();
        if (empty($check)) {
            $param['start_time'] = '';
        }
        $ret = $this->request($url, $param);
        if (empty($ret) || empty($ret['data'])) {
            return true;
        }
        $ret = $this->decryptData($ret['data']['data'], $ret['data']['time']);
        if (empty($ret['data']) || !is_array($ret['data'])) {
            return true;
        }
        $add = [];
        foreach ($ret['data'] as $v) {
            $info = $this->getModel()->getDataInfo(['union_member_id' => $v['union_member_id']]);
            $diff = array_diff($info, $v);
            if (!empty($diff)) {
                $this->getModel()->where("union_member_id", $v['union_member_id'])->update($v);
            }
            if (empty($info)) {
                $add[] = $v;
            }
        }
        if (!empty($add)) {
            $this->getModel()->insertAll($add);
        }
        echo "同步联盟成员成功" . PHP_EOL;
        return true;
    }

}
