<?php

namespace chb_user\union;

use chb_lib\common\BaseSystem;
use chb_lib\common\RedisToolTrait;
use chb_lib\common\ServiceTrait;

class UnionBookIdentity extends UnionService
{

    use RedisToolTrait, ServiceTrait;

    /**
     * 获取联盟增量数据
     *
     * @param array $param
     * @return array|void
     */
    public function getUnionIncrementList($param = [])
    {
        $data = $this->getModel()->getUnionIncrementList($param);
        $config = (BaseSystem::_getObject("SystemConfig"))::_getKeyConfig("union_config");
        $data = $this->jsonEncrypt($data, $config['public_key'], request()->time());
        return $data;
    }

    /**
     * 更新联盟
     *
     * @param [type] $url 请求地址
     * @param [type] $param 请求参数
     * @return bool
     */
    public function updateData($url, $param)
    {
        $check = $this->getModel()->count();
        if (empty($check)) {
            $param['start_time'] = '';
        }
        $ret = $this->request($url, $param);
        if (empty($ret) || empty($ret['data'])) {
            return true;
        }
        $ret = $this->decryptData($ret['data']['data'], $ret['data']['time']);
        if (empty($ret['data']) || !is_array($ret['data'])) {
            return true;
        }
        $add = [];
        foreach ($ret['data'] as $v) {
            $info = $this->getModel()->getDataInfo(['user_no' => $v['user_no'], 'identity_pk' => $v['identity_pk']]);
            $diff = array_diff($info, $v);
            if (!empty($diff)) {
                $this->getModel()->where(['user_no' => $v['user_no'], 'identity_pk' => $v['identity_pk']])->update($v);
            }
            if (empty($info)) {
                $add[] = $v;
            }
        }
        if (!empty($add)) {
            $this->getModel()->insertAll($add);
        }
        echo "同步联盟用户身份成功" . PHP_EOL;
        return true;
    }

    /**
     * 数据加入联盟
     */
    public function addUnion($param = [])
    {
        $data = [
            'user_no' => $param['user_no'],
            'identity_pk' => $param['identity_pk'],
            'organization_no' => $param['organization_no'],
            'union_state' => 1,
        ];
        $this->getModel()->addData($data);
        return true;
    }

    /**
     * 查询联盟数据
     */
    public function getUnion($param = [])
    {
        $list = $this->getDataList(['user_no' => $param['user_no']], false);
        if (empty($list)) {
            return [];
        }
        return $list;
    }
}
