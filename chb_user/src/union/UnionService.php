<?php

namespace chb_user\union;

use chb_lib\common\BaseSystem;
use chb_lib\common\SingleTrait;
use chb_lib\common\UtilsTrait;
use chb_user\identity_config\IdentityConfig;
use chb_user\resource_client\ResourceClient;
use GuzzleHttp\Client;

class UnionService
{
    use UtilsTrait, SingleTrait;

    protected $syncConfig = [
        'is_union' => 'union_member_api', //联盟成员
        'is_client' => 'client_api', //应用
        'is_identity_config' => 'identity_config_api', //身份配置
        'is_user' => 'user_api', //用户
        'is_account' => 'account_api', //账号
        'is_person' => 'person_api', //个人实名
        'is_organization' => 'organization_api', //机构
        'is_identity' => 'identity_api', //身份
    ];

    protected $syncObject = [
        'is_union' => UnionMember::class,
        'is_client' => ResourceClient::class,
        'is_identity_config' => IdentityConfig::class,
        'is_user' => UnionBookUser::class,
        'is_account' => UnionBookAccount::class,
        'is_person' => UnionBookPerson::class,
        'is_organization' => UnionBookOrganization::class,
        'is_identity' => UnionBookIdentity::class,
    ];

    protected $cycle = 7; //默认周期 7天

    /**
     * 联盟数据-发起同步
     * 每次更新获取当前时间前7天增量数据
     *
     */
    public function startSync()
    {
        $list = UnionMember::_getUnionLeaderList();
        if (empty($list)) {
            return true;
        }
        $config = (BaseSystem::_getObject("SystemConfig"))::_getKeyConfig("union_config");
        if (empty($config) || empty($config['is_union'])) {
            return true;
        }
        $client_owner = (BaseSystem::_getObject("SystemConfig"))::_getKeyConfig("client_owner");
        $cycle = empty($config['cycle']) ? $this->cycle : $config['cycle'];
        $param = [
            'start_time' => date("Y-m-d", strtotime("-" . $cycle . " day")),
            'client_owner' => $client_owner,
        ];
        foreach ($list as $info) {
            $url = "http://" . $info['host'] . $info['url'];
            echo $url . PHP_EOL;
            foreach ($this->syncConfig as $k => $v) {
                if (empty($config[$k]) || empty($this->syncObject[$k])) {
                    continue;
                }
                echo $k . PHP_EOL;
                $this->syncObject[$k]::_updateData($url, array_merge($param, ['api' => $config[$v]]));
            }
        }
        return true;
    }

    /**
     * 发送请求
     *
     * @param [type] $url
     * @param [type] $param
     * @return void|array
     */
    protected function request($url, $param)
    {
        $apiKey = (BaseSystem::_getObject("SystemConfig"))::_getKeyConfig("admin_key");
        if (empty($apiKey)) {
            return [];
        }
        $param = array_merge($param, [
            'timeStamp' => time(),
            'nonceStr' => $this->getNonceStr(),
        ]);
        $signStr = "";
        ksort($param); //按拼音重新排序
        foreach ($param as $key => $val) {
            $signStr .= strtoupper($key) . "=$val" . "&";
        }
        $signStr = $signStr . $apiKey;
        $param["signKey"] = md5($signStr);

        $obj = new Client();
        $ret = $obj->request("post", $url, [
            'form_params' => $param, //post表单提交
        ]);
        $code = $ret->getStatusCode();
        if ($code != '200') {
            return [];
        }
        $body = (string) $ret->getBody();
        return json_decode($body, true);
    }

    /**
     * 解密字符串
     *
     * @param [type] $data
     * @return void|array
     */
    public function decryptData($data, $time)
    {
        if (empty($data)) {
            return [];
        }
        $config = (BaseSystem::_getObject("SystemConfig"))::_getKeyConfig("union_config");
        $data = $this->jsonDecrypt($data, $config['public_key'], $time);
        return $data;
    }
}
