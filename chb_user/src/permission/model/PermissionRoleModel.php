<?php

/**
 * 模型
 */

namespace chb_user\permission\model;

use chb_lib\common\BaseModel;
use think\Model;

class PermissionRoleModel extends BaseModel
{

    protected $name = 'permission_role';
    protected $pk = 'permission_role_id';
    protected $alias = "a";

    protected $likeList = ["keyword" => "permission_id"]; //设置模糊搜索映射的字段 alias|value

    /**
     * 获取菜单数据
     *
     * @param array $param
     * @return array
     */
    public function getPassMenuList($param = [])
    {
        $objWhere = $this->alias($this->alias)->leftJoin("permission m", "m.permission_id = a.permission_id")
            ->where("m.permission_type", "menu_id")
            ->field("a.permission_id,a.role_id,m.type_id as menu_id");
        if (!empty($param['arr_role_ids'])) {
            $objWhere = $objWhere->where([$this->alias . '.role_id' => $this->explodeData(",", $param['arr_role_ids'])]);
        }
        $objWhere = $objWhere->order($this->getOrderSort($param['order_type'] ?? 0));
        return $objWhere->select()->toArray();
    }

    /**
     * 获取按钮数据
     *
     * @param array $param
     * @return array
     */
    public function getPassMenuButtonList($param = [])
    {
        $objWhere = $this->alias($this->alias)->leftJoin("permission m", "m.permission_id = a.permission_id")
            ->where("m.permission_type", "button_id")
            ->field("a.permission_id,a.role_id,m.type_id as button_id");
        if (!empty($param['arr_role_ids'])) {
            $objWhere = $objWhere->where([$this->alias . '.role_id' => $this->explodeData(",", $param['arr_role_ids'])]);
        }
        $objWhere = $objWhere->order($this->getOrderSort($param['order_type'] ?? 0));
        return $objWhere->select()->toArray();
    }

    /**
     * 根据菜单id判断是否拥有权限
     */
    public function checkMenu($param = [])
    {
        $objWhere = $this->alias($this->alias)->leftJoin("permission m", "m.permission_id = a.permission_id")
            ->where("m.permission_type", "menu_id")
            ->field("a.permission_id,a.role_id,m.type_id as menu_id");
        if (!empty($param['menu_id'])) {
            $objWhere = $objWhere->where(['m.type_id' => $param['menu_id']]);
        }
        if (!empty($param['arr_role_ids'])) {
            $objWhere = $objWhere->where([$this->alias . '.role_id' => $this->explodeData(",", $param['arr_role_ids'])]);
        }
        return $objWhere->count();
    }

    /**
     * 根据菜单id判断是否拥有权限
     */
    public function checkButton($param = [])
    {
        $objWhere = $this->alias($this->alias)->leftJoin("permission m", "m.permission_id = a.permission_id")
            ->where("m.permission_type", "button_id")
            ->field("a.permission_id,a.role_id,m.type_id as button_id");
        if (!empty($param['button_id'])) {
            $objWhere = $objWhere->where(['m.type_id' => $param['button_id']]);
        }
        if (!empty($param['arr_role_ids'])) {
            $objWhere = $objWhere->where([$this->alias . '.role_id' => $this->explodeData(",", $param['arr_role_ids'])]);
        }
        return $objWhere->count();
    }
}
