<?php

/**
 * 模型
 */

namespace chb_user\permission\model;

use chb_lib\common\BaseModel;

class PermissionModel extends BaseModel
{

    protected $name = 'permission';
    protected $pk = 'permission_id';

    protected $likeList = ["keyword" => "permission_name"]; //设置模糊搜索映射的字段 alias|value

}
