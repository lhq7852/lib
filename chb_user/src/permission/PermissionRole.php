<?php

namespace chb_user\permission;

use chb_lib\common\exception\Err;
use chb_lib\common\RedisToolTrait;
use chb_lib\common\ServiceTrait;
use chb_user\resource_menu\ResourceButton;
use chb_user\resource_menu\ResourceMenu;

class PermissionRole
{

    use RedisToolTrait, ServiceTrait;

    protected $typeList = [
        'pass_menu' => 'getPassMenuList',
        'pass_button' => 'getPassMenuButtonList',
    ];

    /**
     * 权限菜单列表
     *
     * @param array $param
     * @return array
     */
    protected function getPassMenuList($param = [])
    {
        $list = $this->getModel()->getPassMenuList($param);
        $arrMenu = array_column($list, 'menu_id');
        $menu_ids = implode(",", $arrMenu);
        $data = ResourceMenu::_getDataList(['menu_id' => $this->explodeData(",", $menu_ids)]);
        return $data;
    }

    /**
     * 指定页面的菜单权限
     *
     * @param array $param
     * @return array
     */
    public function getPassMenuButtonList($param = [])
    {
        $list = $this->getModel()->getPassMenuButtonList($param);
        $arrButton = array_column($list, 'button_id');
        $button_ids = implode(",", $arrButton);
        $data = ResourceButton::_getDataList(['button_id' => $this->explodeData(",", $button_ids)]);
        return $data;
    }

    /**
     * 设置角色权限
     *
     * @return ture
     */
    public function setRolePermission($param = [])
    {
        if (empty($param['role_id']) || empty($param['permission_ids'])) {
            Err::errorMsg(800);
        }
        $permission_ids = $this->explodeData(",", $param['permission_ids']);
        $data = [];
        $organization_no = request()->user['organization_no'] ?? 0;
        foreach ($permission_ids as $permission_id) {
            $data[] = [
                'role_id' => $param['role_id'],
                'permission_id' => $permission_id,
                "organization_no" => $organization_no,
            ];
        }
        $this->getModel()->addData($data);
        return true;
    }

    /**
     * 设置角色权限
     *
     * @return ture
     */
    public function getRolePermission($param = [])
    {
        $list = $this->getDataList(['role_id' => $param['role_id'], 'field' => 'role_id,permission_id'], false);
        return [
            'role_id' => $param['role_id'],
            'permission_ids' => array_column($list, 'permission_id'),
        ];
    }
}
