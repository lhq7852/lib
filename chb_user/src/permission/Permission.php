<?php

namespace chb_user\permission;

use chb_lib\common\RedisToolTrait;
use chb_lib\common\ServiceTrait;

class Permission
{

    use RedisToolTrait, ServiceTrait;

    /**
     * 设置入库的参数,可重写
     *
     * @param [type] $param
     * @return object
     */
    protected function setFieldParam(&$param = [], $mode = "add")
    {
        if (!empty($param['button_id'])) {
            $param['type_id'] = $param['button_id'];
        }
        if (!empty($param['menu_id'])) {
            $param['type_id'] = $param['menu_id'];
        }
        return $this;
    }

}
