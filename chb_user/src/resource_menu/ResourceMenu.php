<?php

namespace chb_user\resource_menu;

use chb_lib\common\RedisToolTrait;
use chb_lib\common\ServiceTrait;
use chb_user\permission\Permission;

class ResourceMenu
{

    use RedisToolTrait, ServiceTrait;

    protected $typeList = ['manage' => 'getManageMenuList'];

    /**
     * 菜单管理列表
     *
     * @param array $param
     * @return array
     */
    protected function getManageMenuList($param = [])
    {
        $data = $this->getPageList($param);
        if (empty($data['data'])) {
            return $data;
        }
        $arr_menu_ids = array_column($data['data'], "menu_id");
        $arrButton = ResourceMenuButton::_getMenuButtonList(['arr_menu_ids' => $arr_menu_ids]);
        foreach ($data['data'] as &$v) {
            $v['children'] = $arrButton[$v['menu_id']] ?? [];
        }
        return $data;
    }

    /**
     * 监听菜单添加
     *
     * @return bool
     */
    public function listenAdd()
    {
        Permission::_addData([
            'permission_type' => 'menu_id',
            'permission_name' => $this->dataInfo['menu_name'],
            'type_id' => $this->dataInfo['menu_id'],
        ]);
        return true;
    }

    /**
     * 监听菜单删除
     *
     * @return bool
     */
    public function listenDelete()
    {
        Permission::_deleteData([
            'menu_id' => $this->dataInfo['menu_id'],
            'permission_type' => 'menu_id',
        ]);
        return true;
    }
}
