<?php

/**
 * 后台用户模型
 */

namespace chb_user\resource_menu\model;

use chb_lib\common\BaseModel;

class ResourceButtonModel extends BaseModel
{

    protected $name = 'resource_button';
    protected $pk = 'button_id';

    protected $likeList = ["keyword" => "button_name"]; //设置模糊搜索映射的字段 alias|value

}
