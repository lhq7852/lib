<?php

/**
 * 后台用户模型
 */

namespace chb_user\resource_menu\model;

use chb_lib\common\BaseModel;

class ResourceMenuButtonModel extends BaseModel
{

    protected $name = 'resource_menu_button';
    protected $pk = 'id';
    protected $alias = "a";

    protected $likeList = ["keyword" => "button_name"]; //设置模糊搜索映射的字段 alias|value

    /**
     * 菜单下的按钮列表
     *
     * @param array $param
     * @return array
     */
    public function getMenuButtonList($param = [])
    {
        $objWhere = $this->setWhereLike($param)->alias($this->alias)->leftJoin("resource_button b", "b.button_id = a.button_id");
        if (!empty($param['arr_menu_ids'])) {
            $objWhere = $objWhere->where([$this->alias . '.menu_id' => $this->explodeData(",", $param['arr_menu_ids'])]);
        }
        $objWhere = $objWhere->order($this->getOrderSort($param['order_type'] ?? 0));
        $data = $objWhere->select()->toArray();
        return $this->array_column_key($data, 'menu_id', false);
    }
}
