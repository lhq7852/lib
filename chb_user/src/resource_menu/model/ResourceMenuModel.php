<?php

/**
 * 后台用户模型
 */

namespace chb_user\resource_menu\model;

use chb_lib\common\BaseModel;

class ResourceMenuModel extends BaseModel
{

    protected $name = 'resource_menu';
    protected $pk = 'menu_id';

    protected $likeList = ["keyword" => "menu_name"]; //设置模糊搜索映射的字段 alias|value

}
