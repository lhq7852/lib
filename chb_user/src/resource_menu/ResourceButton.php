<?php

namespace chb_user\resource_menu;

use chb_lib\common\RedisToolTrait;
use chb_lib\common\ServiceTrait;
use chb_user\permission\Permission;
use chb_user\permission\PermissionButton;

class ResourceButton
{

    use RedisToolTrait, ServiceTrait;

    /**
     * 重写添加方法
     *
     * @return bool
     */
    public function addData($param = [])
    {
        $info = $this->getInfo(['button_pk' => $param['button_pk']], false);
        if (empty($info)) {
            $id = $this->validateData($param, __FUNCTION__)->setFieldParam($param)->getModel()->addData($param);
            $this->dataInfo = $this->getDataInfo($id);
        } else {
            $id = $info['button_id'];
        }
        $this->listenRedis(__FUNCTION__);
        return $id;
    }

    /**
     * 监听按钮添加
     *
     * @return bool
     */
    public function listenAdd()
    {
        $info = Permission::_getInfo(['type_id' => $this->param['menu_id'], 'permission_type' => 'menu_id'], false);

        Permission::_deleteData(['permission_type' => 'button_id', 'type_id' => $this->dataInfo['button_id']]);
    
        Permission::_addData([
            'parent_id' => $info['permission_id'] ?? 0,
            'permission_type' => 'button_id',
            'permission_name' => $this->dataInfo['button_name'],
            'type_id' => $this->dataInfo['button_id'],
        ]);
        ResourceMenuButton::_deleteData(['button_id' => $this->dataInfo['button_id'], 'menu_id' => $this->param['menu_id']]);
        ResourceMenuButton::_addData(['menu_id' => $this->param['menu_id'], 'button_id' => $this->dataInfo['button_id']]);
        return true;
    }

    /**
     * 监听按钮删除
     *
     * @return bool
     */
    public function listenDelete()
    {
        Permission::_deleteData([
            'button_id' => $this->dataInfo['button_id'],
            'permission_type' => 'button_id',
        ]);
        return true;
    }
}
