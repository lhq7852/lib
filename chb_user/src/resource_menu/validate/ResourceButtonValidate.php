<?php

/**
 * 验证
 */

namespace chb_user\resource_menu\validate;

use chb_lib\common\ValidateTrait;
use think\Validate;

class ResourceButtonValidate extends Validate
{
    use ValidateTrait;

    protected $rule = [
        'button_name' => 'require|max:200',
        'button_pk' => 'require|max:200',
        'sort' => 'number',
    ];
}
