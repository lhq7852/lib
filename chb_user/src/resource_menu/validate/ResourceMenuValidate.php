<?php

/**
 * 验证
 */

namespace chb_user\resource_menu\validate;

use chb_lib\common\ValidateTrait;
use think\Validate;

class ResourceMenuValidate extends Validate
{
    use ValidateTrait;

    protected $being = "menu_pk"; //唯一字段检查 menu_pk|menu_pk+menu_id

    protected $rule = [
        'menu_name' => 'require|max:200',
        'menu_pk' => 'require|max:200|checkBeing',
        'sort' => 'number',
    ];
}
