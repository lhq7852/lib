<?php

namespace chb_user\resource_menu;

use chb_lib\common\ServiceTrait;
use chb_lib\common\RedisToolTrait;

class ResourceMenuButton
{

    use RedisToolTrait, ServiceTrait;
}
