<?php

namespace chb_user\resource_client;

use chb_lib\common\RedisToolTrait;
use chb_lib\common\ServiceTrait;
use chb_user\union\UnionService;

class ResourceClient
{

    use RedisToolTrait, ServiceTrait;

    /**
     * 设置入库的参数,可重写
     *
     * @param [type] $param
     * @return object
     */
    protected function setFieldParam(&$param = [], $mode = "add")
    {
        if ($mode == 'add') {
            $param['app_id'] = $this->getNonceStr(12);
        }
        return $this;
    }

    /**
     * 检查app_id是否有效
     *
     * @param array $param
     * @return void|array
     */
    public function getAppIdByClientPk($param = [])
    {
        $info = $this->getInfo(['client_pk' => $param['client_pk'], 'client_user_no' => $param['client_owner'] ?? ''], false);
        return [
            'app_id' => $info['app_id'] ?? '',
            'client_user_no' => $info['client_user_no'] ?? '',
        ];
    }

    /**
     * 获取全部应用标识
     *
     * @param [type] $url
     * @param [type] $param
     * @return void
     */
    public function getAllClientPk($param = [])
    {
        $list = $this->getDataList();
        if (empty($list)) {
            return "";
        }
        return implode(",", array_column($list, 'client_pk'));
    }

    /**
     * 更新租户应用
     *
     * @param [type] $url 请求地址
     * @param [type] $param 请求参数
     * @return bool
     */
    public function updateData($url, $param)
    {
        $ret = UnionService::_request($url, $param);
        if (empty($ret) || empty($ret['data'])) {
            return true;
        }
        $ret = UnionService::_decryptData($ret['data']['data'], $ret['data']['time']);
        if (empty($ret['data']) || !is_array($ret['data'])) {
            return true;
        }
        $add = [];
        foreach ($ret['data'] as $v) {
            $info = $this->getModel()->getDataInfo(['app_id' => $v['app_id']]);
            $diff = array_diff($info, $v);
            if (!empty($diff)) {
                $this->getModel()->where("app_id", $v['app_id'])->update($v);
            }
            if (empty($info)) {
                $add[] = $v;
            }
        }
        if (!empty($add)) {
            $this->getModel()->insertAll($add);
        }
        echo "同步租户应用成功" . PHP_EOL;
        return true;
    }

}
