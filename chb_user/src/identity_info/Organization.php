<?php

namespace chb_user\identity_info;

use chb_lib\common\RedisToolTrait;
use chb_lib\common\ServiceTrait;
use chb_rpc\external_api\VerifyApiRpc;
use chb_user\department\Staff;
use chb_user\union\UnionBookOrganization;
use chb_user\user\Account;
use think\facade\Queue;

class Organization
{

    use RedisToolTrait, ServiceTrait;

    /**
     * 保存认证信息
     *
     * @param array $param
     * @return bool|Exception
     */
    public function saveInfo($param = [])
    {
        $data = [
            'account_no' => request()->user['account_no'] ?? '',
            'user_no' => request()->user['user_no'] ?? '',
            'legal_person_name' => base64_encode($param['legal_person_name']) ?? '',
            'legal_identity_card_no' => base64_encode($param['legal_identity_card_no']) ?? '',
            'organization_no' => md5($param['credit_code']),
            'organization_name' => base64_encode($param['organization_name']),
            'credit_code' => base64_encode($param['credit_code']) ?? '',
            'business_license_image' => $param['business_license_image'],
            'certificate_image' => $param['certificate_image'],
            'state' => 1,
        ];
        $id = $this->addData($data);
        Queue::push("app\job\IdentityAuthJob@authCompany", $data, 'identity:auth_company');
        return $id;
    }

    /**
     * 队列验证身份
     *
     * @param [type] $param
     * @return bool|Exception
     */
    public function startAuth($param)
    {
        $info = $this->getInfo(['credit_code' => $param['credit_code']]);
        if (!empty($info) && $info['state'] == 2) {
            return true;
        }
        $keyword = !empty($param['credit_code']) ? base64_decode($param['credit_code']) : (!empty($param['company_name']) ? base64_decode($param['company_name']) : '');
        if (empty($keyword)) {
            $this->editData([
                'state' => 3,
                'reason' => '统一信用代码不能为空',
            ], ['credit_code' => $param['credit_code']]);
        }
        $ret = VerifyApiRpc::_getCompanyInfo([
            'keyword' => $keyword,
        ]);
        if (!empty($ret['code']) && $ret['code'] == 200) {
            $this->editData([
                'state' => !empty($ret['code']) && $ret['code'] == 200 ? 2 : 3,
                'reason' => json_encode($ret, JSON_UNESCAPED_UNICODE),
                'examine_user_no' => '01',
                'examine_time' => date("Y-m-d H:i:s"),
            ], ['credit_code' => $param['credit_code']]);
            $account = Account::_getAccountInfo(['account_no' => $info['account_no']]);
            $info['phone'] = $account['phone'];
            $info['email'] = $account['email'];
            //组织成员
            Staff::_addStaff([
                'user_no' => $info['user_no'],
                'organization_no' => $info['organization_no'],
            ]);
            //加入联盟
            UnionBookOrganization::_addUnion($info);
        }
        return true;
    }

    /**
     * 同步联盟账号数据
     */
    public function syncUnion($param = [])
    {
        $list = UnionBookOrganization::_getUnion(['user_no' => $param['user_no']]);
        if (empty($list)) {
            return false;
        }
        $data = [];
        foreach ($list as $info) {
            $data = [
                'account_no' => !empty($info['phone_account_no']) ? $info['phone_account_no'] : $info['email_account_no'],
                'user_no' => $info['user_no'] ?? '',
                'legal_person_name' => $info['legal_person_name'] ?? '',
                'legal_identity_card_no' => $info['legal_identity_card_no'] ?? '',
                'organization_no' => $info['organization_no'] ?? '',
                'organization_name' => $info['organization_name'] ?? '',
                'business_license_image' => $info['business_license_image'] ?? '',
                'credit_type' => $info['credit_type'] ?? '',
                'credit_code' => $info['credit_code'] ?? '',
                'certificate_image' => $info['certificate_image'] ?? '',
                'mode' => "union",
                'state' => 2,
                'examine_user_no' => "01",
                'examine_time' => date("Y-m-d H:i:s"),
                'reason' => "联盟",
            ];
        }
        $this->getModel()->addData($data);
        return true;
    }
}
