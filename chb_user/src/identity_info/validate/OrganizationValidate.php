<?php

/**
 * 
 */

namespace chb_user\identity_info\validate;

use chb_lib\common\ValidateTrait;
use think\Validate;

class OrganizationValidate  extends Validate
{
    use ValidateTrait;

    protected $being = "credit_code"; //唯一字段检查

    protected $rule = [
        'credit_code' => 'require|max:200|checkBeing',
        'organization_name' => 'require|max:200',
    ];
}
