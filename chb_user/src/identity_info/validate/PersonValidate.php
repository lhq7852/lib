<?php

/**
 * 
 */

namespace chb_user\identity_info\validate;

use chb_lib\common\ValidateTrait;
use think\Validate;

class PersonValidate  extends Validate
{
    use ValidateTrait;

    protected $being = "card_no"; //唯一字段检查

    protected $rule = [
        'card_type' => 'require',
        'card_no' => 'require|max:200|checkBeing',
        'card_name' => 'require|max:200',
    ];
}
