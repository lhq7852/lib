<?php

namespace chb_user\identity_info;

use chb_lib\common\exception\Err;
use chb_lib\common\RedisToolTrait;
use chb_lib\common\RpcServerTrait;
use chb_lib\common\ServiceTrait;
use chb_rpc\external_api\VerifyApiRpc;
use chb_user\union\UnionBookPerson;
use chb_user\user\User;
use think\facade\Queue;

class Person
{

    use RedisToolTrait, ServiceTrait, RpcServerTrait;

    protected $validateData = [1 => "张张嘴", 2 => "眨眨眼"]; //1代表张嘴，2代表闭眼

    public function __construct()
    {
        $this->redisKey = "Person:";
    }

    /**
     * 实名审核列表
     */
    public function getExamineList($param = [])
    {
        if (method_exists($this, "getDataByRedisHook") && !isset($this->redisHook[$this->createHookKey(__FUNCTION__, func_get_args())])) {
            return $this->getDataByRedisHook(__FUNCTION__, func_get_args(), true);
        }
        $data = $this->getModel()->getExamineList($param);
        foreach ($data as $k => &$v) {
            $v['id_number'] = base64_decode($v['id_number']);
            $v['name'] = base64_decode($v['name']);
        }
        return $data;
    }

    /**
     * 获取实名信息
     *
     * @param array $param
     * @return void
     */
    public function getIdentityInfo($param = [])
    {
        $info = $this->getInfo(['account_no' => $param['account_no']], false);
        if (empty($info)) {
            Err::errorMsg(833);
        }
        $info['card_no'] = base64_decode($info['card_no']);
        $info['card_name'] = base64_decode($info['card_name']);
        return $info;
    }

    /**
     * 保存个人实名认证信息
     *
     * @param array $param
     * @return bool|Exception
     */
    public function saveInfo($param = [])
    {
        $data = [
            'card_type' => 'IDcard', //身份证
            'account_no' => request()->user['account_no'],
            'card_no' => base64_encode($param['card_no']),
            'card_name' => base64_encode($param['card_name']),
            'user_no' => request()->user['user_no'],
            'state' => 1,
            'facade_image' => $param['facade_image'] ?? '',
            'country_image' => $param['country_image'] ?? '',
        ];
        $id = $this->addData($data);
        if ($id && $data['card_type'] == 'IDcard') {
            Queue::push("app\job\IdentityAuthJob@authPerson", $data, 'identity:auth_person');
        }
        return $id;
    }

    /**
     * 队列验证身份
     *
     * @param [type] $param
     * @return bool|Exception
     */
    public function startAuth($param)
    {
        $info = $this->getInfo(['card_no' => $param['card_no']]);
        if (!empty($info) && $info['state'] == 2) {
            return true;
        }
        $ret = VerifyApiRpc::_getPersonIdCard([
            'idCard' => base64_decode($param['card_no']),
            'name' => base64_decode($param['card_name']),
        ]);

        if (!empty($ret['status']) && $ret['status'] == '01') {
            //注册用户
            $info['user_no'] = User::_regUser([
                'account_no' => $param['account_no'],
                'card_no' => base64_decode($param['card_no']),
                'card_name' => base64_decode($param['card_name']),
            ]);
            $this->editData([
                'state' => !empty($ret['status']) && $ret['status'] == '01' ? 2 : 3,
                'reason' => json_encode($ret, JSON_UNESCAPED_UNICODE),
                'examine_user_no' => '01',
                'examine_time' => date("Y-m-d H:i:s"),
                'user_no' => $info['user_no'],
            ], ['card_no' => $param['card_no']]);
            //加入联盟
            UnionBookPerson::_addUnion($info);
        }
        return true;
    }

    /**
     * 获取验证动作顺序
     *
     * @return array
     */
    public function getValidateData()
    {
        $validateData = array_keys($this->validateData);
        shuffle($validateData);
        $data = implode(",", $validateData);
        $this->setRedisKey($this->redisKey . "getValidateData:" . request()->user['account_id'])->setRedisValue($data);
        $txt = "先" . $this->validateData[$validateData[0]] . ",再" . $this->validateData[$validateData[1]];
        return [
            'validateData' => $txt,
        ];
    }

    /**
     * 活体验证
     */
    public function compareRequest($param = [])
    {
        $validateData = $this->setRedisKey($this->redisKey . "getValidateData:" . request()->user['account_id'])->getRedisString();
        if (empty($validateData)) {
            Err::errorMsg(840);
        }
        $info = $this->getIdentityInfo(['account_no' => request()->user['account_no']]);
        $param['validateData'] = $validateData;
        $param['livenessType'] = "ACTION";
        $param['imageUrl'] = $info['facade_image'];
        $ret = VerifyApiRpc::_compareRequest($param);
        $result = $ret['Description'] == "成功" ? "1" : "0";
        $this->setRedisKey($this->redisKey . "CompareResult:" . request()->user['account_id'])->setRedisValue($result);
        return [
            "sim" => $ret['Sim'],
            "description" => $ret['Description'],
        ];
    }

    /**
     * 获取活体验证结果
     *
     * @return void|string|int
     */
    public function getCompareResult()
    {
        return $this->setRedisKey($this->redisKey . "CompareResult:" . request()->user['account_id'])->getRedisString();
    }

    /**
     * 同步联盟账号数据
     */
    public function syncUnion($param = [])
    {
        $info = UnionBookPerson::_getUnion(['user_no' => $param['user_no']]);
        if (empty($info)) {
            return false;
        }
        $data = [
            'account_no' => !empty($info['phone_account_no']) ? $info['phone_account_no'] : $info['email_account_no'],
            'user_no' => $info['user_no'] ?? '',
            'card_type' => $info['card_type'] ?? '',
            'card_no' => $info['card_no'] ?? '',
            'card_name' => $info['card_name'] ?? '',
            'facade_image' => $info['facade_image'] ?? '',
            'country_image' => $info['country_image'] ?? '',
            'mode' => "union",
            'state' => 2,
            'examine_user_no' => "01",
            'examine_time' => date("Y-m-d H:i:s"),
            'reason' => "联盟",
        ];
        $this->getModel()->addData($data);
        return true;
    }
}
