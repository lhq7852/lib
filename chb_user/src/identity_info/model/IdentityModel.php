<?php

/**
 * 模型
 */

namespace chb_user\identity_info\model;

use chb_lib\common\BaseModel;
use chb_user\identity_config\model\IdentityConfigModel;

class IdentityModel extends BaseModel
{

    protected $name = 'identity';
    protected $pk = 'identity_id';

    protected $alias = "a";

    /**
     * 字段配置-1
     *
     * @return string
     */
    public static function field_1()
    {
        return "a.user_no,a.identity_pk,a.organization_no,a.mode,a.state,a.examine_user_no,a.examine_time,a.create_time";
    }

    /**
     * 字段配置-2
     *
     * @return string
     */
    public static function field_2()
    {
        return 'a.user_no, a.identity_pk, a.organization_no';
    }

    /**
     * 分页列表
     */
    public function getPageList($param = [])
    {
        $objWhere = $this->alias($this->alias)->leftJoin("identity_config ic", "ic.identity_pk = a.identity_pk")
            ->field(self::field_1() . "," . IdentityConfigModel::field_1());
        if (!empty($param['state'])) {
            $objWhere = $objWhere->where("a.state", $param['state']);
        }
        $objWhere = $objWhere->order($this->getOrderSort(empty($param['order_type']) ? 0 : $param['order_type']));

        if (!empty($this->param["first_page_time"])) {
            $objWhere = $objWhere->where("create_time", ">=", date("Y-m-d H:i:s", $this->param["first_page_time"]));
        }
        return $objWhere->paginate($param['pageSize'] ?? 30)->toArray();
    }
}
