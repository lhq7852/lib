<?php

/**
 * 模型
 */

namespace chb_user\identity_info\model;

use chb_lib\common\BaseModel;

class OrganizationModel extends BaseModel
{

    protected $name = 'organization';
    protected $pk = 'organization_id';

    protected $likeList = ["keyword" => "company_name"]; //设置模糊搜索映射的字段 alias|value

    /**
     * 根据用户编号获取身份证姓名
     *
     * @param array $param
     * @return void|array
     */
    public function getOrganizationNameList($param = [])
    {
        $data = $this->alias($this->alias)->where(['organization_no' => $param['organization_no']])->column("organization_name", "organization_no");
        if (empty($data)) {
            return $data;
        }
        foreach ($data as &$v) {
            $v = base64_decode($v);
        }
        return $data;
    }


}
