<?php

/**
 * 后台用户模型
 */

namespace chb_user\identity_info\model;

use chb_lib\common\BaseModel;
use chb_lib\common\SingleTrait;

class LoginIdentityLogModel extends BaseModel
{
    use SingleTrait;

    protected $name = 'login_identity_log';
    protected $pk = 'login_identity_log_id';
    protected $connection = 'log_database';

}
