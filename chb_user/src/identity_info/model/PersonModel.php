<?php

/**
 * 模型
 */

namespace chb_user\identity_info\model;

use chb_lib\common\BaseModel;
use think\facade\Db;

class PersonModel extends BaseModel
{

    protected $name = 'person';
    protected $pk = 'person_id';

    protected $likeList = ["keyword" => "card_name"]; //设置模糊搜索映射的字段 alias|value

    /**
     * 实名审核列表
     */
    public function getExamineList($param = [])
    {
        $sub_1 = Db::connect($this->connection)
            ->name((new OrganizationModel)->getName())
            ->field("credit_type as type,credit_code as id_number,organization_name as name,examine_time,examine_user_no,state")
            ->buildSql();
        $data = Db::connect($this->connection)
            ->name($this->name)
            ->field("card_type as type,card_no as id_number,card_name as name,examine_time,examine_user_no,state")
            ->unionAll($sub_1)
            ->page($param['page'] ?? 1)
            ->limit($param['pageSize'] ?? 30)
            ->select()
            ->toArray();
        return $data;
    }

    /**
     * 根据用户编号获取身份证姓名
     *
     * @param array $param
     * @return void|array
     */
    public function getPersonNameList($param = [])
    {
        $data = $this->alias($this->alias)->where(['user_no' => $param['user_no']])->column("card_name", "user_no");
        if (empty($data)) {
            return $data;
        }
        foreach ($data as &$v) {
            $v = base64_decode($v);
        }
        return $data;
    }
}
