<?php

/**
 * 日志
 *
 */

namespace chb_user\identity_info;

use chb_lib\common\RedisToolTrait;
use chb_lib\common\ServiceTrait;
use think\facade\Queue;

class LoginIdentityLog
{
    use ServiceTrait, RedisToolTrait;

    /**
     * 保存日志队列
     *
     * @return bool
     */
    public function saveLogQueue($param = [])
    {
        $data = [
            'login_identity_log_id' => $param['login_identity_log_id'],
            'update_time' => date("Y-m-d H:i:s"),
            'api' => request()->param("api", "") ?? '',
            'ip' => request()->ip(),
            'http_referer' => $_SERVER['HTTP_REFERER'] ?? ($_SERVER['HTTP_ORIGIN'] ?? ''),
            'user_agent' => $_SERVER['HTTP_USER_AGENT'] ?? '',
        ];
        Queue::push("app\job\LoginLogJob@identityLog", $data, 'login_log:identity');
        return true;
    }

    /**
     * 保存日志
     *
     * @param [type] $param
     * @return bool
     */
    public function saveLog($param)
    {
        $this->getModel()->editData($param);
        return true;
    }

    /**
     * 登录日志
     *
     * @param array $param
     * @return bool
     */
    public function addLoginLog($param = [])
    {
        $this->getModel()->addData([
            'account_no' => $param['account_no'],
            'user_no' => $param['user_no'],
            'identity_pk' => $param['identity_pk'],
            'organization_no' => $param['organization_no'],
            'api' => request()->param("api", "") ?? '',
            'ip' => request()->ip(),
            'http_referer' => $_SERVER['HTTP_REFERER'] ?? ($_SERVER['HTTP_ORIGIN'] ?? ''),
            'user_agent' => $_SERVER['HTTP_USER_AGENT'] ?? '',
        ]);
        return true;
    }
}
