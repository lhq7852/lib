<?php

/**
 * 用户身份
 *
 */

namespace chb_user\identity_info;

use chb_lib\common\exception\Err;
use chb_lib\common\RedisToolTrait;
use chb_lib\common\ServiceTrait;
use chb_user\identity_config\IdentityFlowConfig;
use chb_user\identity_info\model\IdentityModel;
use chb_user\identity_info\model\LoginIdentityLogModel;
use chb_user\union\UnionBookIdentity;
use chb_user\user\Account;
use chb_user\user\User;

class Identity
{
    use ServiceTrait, RedisToolTrait;

    public $account_id;
    public $account_no;
    public $user_no;

    /**
     * 权限控制
     *
     * @param [type] $account_id
     * @return array
     */
    public function pass($account_id)
    {
        if (method_exists($this, "getDataByRedisHook") && !isset($this->redisHook[$this->createHookKey(__FUNCTION__, func_get_args())])) {
            return $this->getDataByRedisHook(__FUNCTION__, func_get_args(), false);
        }
        $this->account_id = $account_id;
        $account = Account::_getAccountInfo(['account_id' => $account_id]);
        $this->account_no = $account['account_no'];
        $this->user_no = $account['user_no'];
        $identity = $this->getCurrentIdentity($account);
        $data = array_merge($account, $identity);
        return $data;
    }

    /**
     * 当前登录身份
     *
     * @return void
     */
    private function getCurrentIdentity()
    {
        if (empty($this->user_no)) {
            return [
                'identity_pk' => '',
                'organization_no' => '',
            ];
        }
        $last = LoginIdentityLogModel::_getInfo(['user_no' => $this->user_no], false);
        if (!empty($last) && !empty($last['organization_no'])) {
            LoginIdentityLog::_saveLogQueue(['login_identity_log_id' => $last['login_identity_log_id']]);
            return [
                'identity_pk' => $last['identity_pk'],
                'organization_no' => $last['organization_no'],
            ];
        }
        $identity = $this->getInfo(['user_no' => $this->user_no, 'field' => IdentityModel::field_2()]);
        if (empty($identity)) {
            return [
                'identity_pk' => '',
                'organization_no' => '',
            ];
        }
        LoginIdentityLog::_addLoginLog([
            'account_no' => $this->account_no,
            'user_no' => $this->user_no,
            'identity_pk' => $identity['identity_pk'],
            'organization_no' => $identity['organization_no'],
        ]);
        //只有一个身份,默认该身份进入系统
        return [
            'identity_pk' => $identity['identity_pk'],
            'organization_no' => $identity['organization_no'],
        ];
    }

    /**
     * 切换身份
     *
     * @param string $identity_id
     * @return bool
     */
    public function switchIdentity($identity_id = 0)
    {
        $info = $this->getDataInfo($identity_id);
        LoginIdentityLog::_addLoginLog([
            'account_no' => $this->account_no,
            'user_no' => $this->user_no,
            'identity_pk' => $info['identity_pk'],
            'organization_no' => $info['organization_no'],
        ]);
        return true;
    }

    /**
     * 提交身份审核
     *
     * @param array $param
     * @return bool|Exception|int|string
     */
    public function addData($param = [])
    {
        $flowConfig = IdentityFlowConfig::_getInfo(['identity_pk' => $param['identity_pk']], false);
        //实名检查
        if (!empty($flowConfig['is_real']) && empty(Person::_getInfo(['user_no' => request()->user['user_no']], false))) {
            Err::errorMsg(833);
        }
        //活体检查
        if (!empty($flowConfig['is_living']) && empty(Person::_getCompareResult())) {
            Err::errorMsg(842);
        }
        //机构检查
        if (!empty($flowConfig['is_organization']) && empty(Organization::_getInfo(['organization_no' => $param['organization_no']], false))) {
            Err::errorMsg(845);
        }
        //问卷检查
        if (!empty($flowConfig['is_questionnaire'])) {
            Err::errorMsg(847);
        }
        //检查是否已有相同身份
        $check = $this->getInfo(['user_no' => request()->user['user_no'], 'identity_pk' => $param['identity_pk']], false);
        if (!empty($check['state']) && $check['state'] == 1) {
            Err::errorMsg(851);
        }
        if (!empty($check['state']) && $check['state'] == 3) {
            return $this->getModel()->editData(['identity_id' => $check['identity_id'], 'state' => 1]);
        }
        if (!empty($check)) {
            Err::errorMsg(850);
        }
        $data = [
            'user_no' => request()->user['user_no'],
            'identity_pk' => $param['identity_pk'],
            'organization_no' => $param['organization_no'],
            'mode' => "auth",
            'state' => 2, //先默认跳过审核
            'examine_time' => date("Y-m-d H:i:s"),
        ];
        $this->getModel()->addData($data);

        if ($data['state']) {
            UnionBookIdentity::_addUnion($data); //加入联盟
        }
        return true;
    }

    /**
     * 分页列表
     */
    public function getPageList($param = [])
    {
        $data = $this->getModel()->getPageList($param);
        if (empty($data['data'])) {
            return $data;
        }
        $arrPersonNo = [];
        $arrOrganizationNo = [];
        foreach ($data['data'] as $v) {
            if ($v['attribute'] == 1) { //个人
                $arrPersonNo[] = $v['user_no'];
            }
            if ($v['attribute'] == 2) { //机构
                $arrOrganizationNo[] = $v['organization_no'];
            }
        }
        $arrPerson = Person::_getPersonNameList(['user_no' => $arrPersonNo]);
        $arrOrganization = Organization::_getOrganizationNameList(['organization_no' => $arrOrganizationNo]);
        $arrUser = User::_getUserNameList(array_column($data['data'], 'examine_user_no'));
        foreach ($data['data'] as &$v) {
            $v['name'] = $v['attribute'] == 1 ? ($arrPerson[$v['user_no']] ?? '') : '';
            $v['name'] = $v['attribute'] == 2 ? ($arrOrganization[$v['organization_no']] ?? $v['name']) : $v['name'];
            $v['examine_user_name'] = $arrUser[$v['examine_user_no']] ?? '';
        }
        return $data;
    }

    /**
     * 同步联盟账号数据
     */
    public function syncUnion($param = [])
    {
        $list = UnionBookIdentity::_getUnion(['user_no' => $param['user_no']]);
        if (empty($list)) {
            return false;
        }
        $data = [];
        foreach ($list as $info) {
            $data = [
                'user_no' => $info['user_no'] ?? '',
                'identity_pk' => $info['identity_pk'] ?? '',
                'organization_no' => $info['organization_no'] ?? '',
                'mode' => "union",
                'state' => 2,
            ];
        }
        $this->getModel()->addData($data);
        return true;
    }
}
