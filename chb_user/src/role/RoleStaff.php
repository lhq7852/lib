<?php

namespace chb_user\role;

use chb_lib\common\RedisToolTrait;
use chb_lib\common\ServiceTrait;

class RoleStaff
{

    use RedisToolTrait, ServiceTrait;

    /**
     * 添加角色员工
     *
     * @param array $param
     * @return void
     */
    public function addRoleStaff($param = [])
    {
        $this->deleteData(['staff_id' => $param['staff_id']]);
        if (!empty($param['role_ids'])) {
            $role_ids = is_array($param['role_ids']) ? $param['role_ids'] : explode(",", trim($param['role_ids']));
            $data = [];
            foreach ($role_ids as $role_id) {
                $data[] = [
                    'role_id' => $role_id,
                    'staff_id' => $param['staff_id'],
                ];
            }
            if (!empty($data)) {
                $this->getModel()->addData($data);
            }
        }
    }
}
