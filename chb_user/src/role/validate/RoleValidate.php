<?php

/**
 * 验证
 */

namespace chb_user\role\validate;

use chb_lib\common\ValidateTrait;
use think\Validate;

class RoleValidate extends Validate
{
    use ValidateTrait;

    protected $rule = [
        'role_name' => 'require|max:200',
    ];
}
