<?php

/**
 * 模型
 */

namespace chb_user\role\model;

use chb_lib\common\BaseModel;

class RoleModel extends BaseModel
{

    protected $name = 'role';
    protected $pk = 'role_id';

    protected $likeList = ["keyword" => "role_name"]; //设置模糊搜索映射的字段 alias|value

}
