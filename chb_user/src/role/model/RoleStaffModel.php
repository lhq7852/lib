<?php

/**
 * 模型
 */

namespace chb_user\role\model;

use chb_lib\common\BaseModel;

class RoleStaffModel extends BaseModel
{

    protected $name = 'role_staff';
    protected $pk = 'role_staff_id';

    protected $likeList = ["keyword" => "role_id"]; //设置模糊搜索映射的字段 alias|value

}
