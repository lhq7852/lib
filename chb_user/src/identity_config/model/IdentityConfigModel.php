<?php

/**
 * 模型
 */

namespace chb_user\identity_config\model;

use chb_lib\common\BaseModel;

class IdentityConfigModel extends BaseModel
{

    protected $name = 'identity_config';
    protected $pk = 'identity_config_id';

    protected $likeList = ["keyword" => "identity_name"]; //设置模糊搜索映射的字段 alias|value

    
    /**
     * 字段配置-1
     *
     * @return string
     */
    public static function field_1()
    {
        return "ic.identity_name,ic.attribute";
    }
}
