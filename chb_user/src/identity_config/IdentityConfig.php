<?php

namespace chb_user\identity_config;

use chb_lib\common\RedisToolTrait;
use chb_lib\common\ServiceTrait;
use chb_user\resource_client\ResourceClient;
use chb_user\union\UnionService;

class IdentityConfig
{

    use RedisToolTrait, ServiceTrait;

    protected $typeList = ['show' => 'getShowRoleList'];

    /**
     * 显示认证列表
     *
     * @param array $param
     * @return array
     */
    protected function getShowRoleList($param = [])
    {
        return $this->getPageList(['is_show' => 1, 'parent_id' => $param['parent_id'] ?? 0]);
    }

    /**
     * 更新租户-身份配置
     *
     * @param [type] $url 请求地址
     * @param [type] $param 请求参数
     * @return bool
     */
    public function updateData($url, $param)
    {
        $param['clientPk'] = ResourceClient::_getAllClientPk();
        if (empty($param['clientPk'])) {
            return true;
        }
        $ret = UnionService::_request($url, $param);
        if (empty($ret) || empty($ret['data'])) {
            return true;
        }
        $ret = UnionService::_decryptData($ret['data']['data'], $ret['data']['time']);
        if (empty($ret['data']) || !is_array($ret['data'])) {
            return true;
        }
        $add = [];
        foreach ($ret['data'] as $v) {
            $info = $this->getModel()->getDataInfo(['identity_pk' => $v['identity_pk']]);
            $diff = array_diff($info, $v);
            if (!empty($diff)) {
                $this->getModel()->where("identity_pk", $v['identity_pk'])->update($v);
            }
            if (empty($info)) {
                $add[] = $v;
            }
        }
        if (!empty($add)) {
            $this->getModel()->insertAll($add);
        }
        echo "同步租户身份配置成功" . PHP_EOL;
        return true;
    }

}
