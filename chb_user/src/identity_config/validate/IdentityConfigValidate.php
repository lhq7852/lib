<?php

/**
 * 系统配置参数检验
 */

namespace chb_user\identity_config\validate;

use chb_lib\common\ValidateTrait;
use think\Validate;

class IdentityConfigValidate  extends Validate
{
    use ValidateTrait;

    protected $being = "identity_type"; //唯一字段检查

    protected $rule = [
        'identity_type' => 'require|max:200|checkBeing',
        'identity_name' => 'require|max:200',
        'remark' => 'max:5000',
    ];
}
