<?php

namespace chb_user\resource_data;

use chb_lib\common\exception\Err;
use chb_lib\common\RedisToolTrait;
use chb_lib\common\ServiceTrait;

class ResourceData
{

    use RedisToolTrait, ServiceTrait;

    /**
     * 添加权限规则配置
     */
    public function addData($param = [])
    {
        if (empty($param['data']) && !is_array($param['data'])) {
            Err::errorMsg(800);
        }
        $data = [];
        foreach ($param['data'] as $v) {
            if (empty($v['permission_id'])) {
                continue;
            }
            if (!empty($v['see_pk'])) {
                $data[] = [
                    'client_pk' => $param['client_pk'] ?? '',
                    'permission_id' => $v['permission_id'] ?? '',
                    'see_pk' => $v['see_pk'] ?? '',
                    'manage_pk' => '',
                ];
            }
            if (!empty($v['manage_pk'])) {
                $manage_pk = $this->explodeData(",", $v['manage_pk']);
                foreach ($manage_pk as $p) {
                    if (empty($p)) {
                        continue;
                    }
                    $data[] = [
                        'client_pk' => $param['client_pk'] ?? '',
                        'permission_id' => $v['permission_id'] ?? '',
                        'see_pk' => '',
                        'manage_pk' => $p,
                    ];
                }
            }
        }
        if (!empty($data)) {
            $permission_ids = array_column($data, 'permission_id');
            $this->getModel()->deleteData(['permission_id' => $permission_ids]);
            $this->getModel()->addData($data);
        }
        return true;
    }
}
