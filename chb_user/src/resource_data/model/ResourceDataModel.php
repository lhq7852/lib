<?php

/**
 * 后台用户模型
 */

namespace chb_user\resource_data\model;

use chb_lib\common\BaseModel;

class ResourceDataModel extends BaseModel
{

    protected $name = 'resource_data';
    protected $pk = 'data_id';

    protected $likeList = ["keyword" => "data_name"]; //设置模糊搜索映射的字段 alias|value

}
