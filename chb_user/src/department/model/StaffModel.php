<?php

/**
 * 后台模型
 */

namespace chb_user\department\model;

use chb_lib\common\BaseModel;

class StaffModel extends BaseModel
{

    protected $name = 'organization_staff';
    protected $pk = 'staff_id';

    protected $likeList = ["keyword" => "user_no"]; //设置模糊搜索映射的字段 alias|value

    /**
     * 统计员工数量
     *
     * @param array $param
     * @return void|int|string
     */
    public function countStaff($param = [])
    {
        return $this->setIsDeleteTime()->where(['organization_no' => $param['organization_no']])->count();
    }
}
