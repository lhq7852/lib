<?php

/**
 * 后台模型
 */

namespace chb_user\department\model;

use chb_lib\common\BaseModel;

class DepartmentStaffModel extends BaseModel
{

    protected $name = 'organization_derpatment_staff';
    protected $pk = 'id';

    protected $likeList = ["keyword" => "department_id"]; //设置模糊搜索映射的字段 alias|value

}
