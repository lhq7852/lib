<?php

/**
 * 后台用户模型
 */

namespace chb_user\department\model;

use chb_lib\common\BaseModel;

class DepartmentModel extends BaseModel
{

    protected $name = 'organization_derpatment';
    protected $pk = 'department_id';

    protected $likeList = ["keyword" => "department_name"]; //设置模糊搜索映射的字段 alias|value

}
