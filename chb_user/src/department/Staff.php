<?php

namespace chb_user\department;

use chb_lib\common\exception\Err;
use chb_lib\common\RedisToolTrait;
use chb_lib\common\ServiceTrait;
use chb_user\role\RoleStaff;

class Staff
{

    use RedisToolTrait, ServiceTrait;

    public function __construct()
    {
        $this->redisKey = "Staff:";
    }

    /**
     * 创建邀请码
     *
     * @return string
     */
    public function createInviteToken()
    {
        if (empty(request()->user['organization_no'])) {
            Err::errorMsg(865);
        }
        $check = $this->getInfo(['user_no' => request()->user['user_no'], 'organization_no' => request()->user['organization_no']], false);
        if (empty($check)) {
            Err::errorMsg(865);
        }
        $token = $this->getNonceStr(16);
        $this->setRedisKey($this->redisKey . "InviteToken:" . $token)->setRedisTime(60 * 60)->setRedisValue(request()->user['organization_no']);
        return $token;
    }

    /**
     * 根据邀请码加入组织
     *
     * @return bool
     */
    public function inviteTokenJoin($inviteToken = '')
    {
        if (empty(request()->user['user_no'])) {
            Err::errorMsg(833);
        }
        $organization_no = $this->setRedisKey($this->redisKey . "InviteToken:" . $inviteToken)->getRedisString();
        if (empty($organization_no)) {
            Err::errorMsg(860);
        }
        $this->addStaff([
            'organization_no' => $organization_no,
            'user_no' => request()->user['user_no'],
        ]);
        return true;
    }

    /**
     * 添加成员
     *
     * @param [type] $param
     * @return bool
     */
    public function addStaff($param = [])
    {
        if (empty($param['user_no']) || empty($param['organization_no'])) {
            return false;
        }
        $check = $this->getInfo(['user_no' => $param['user_no'], 'organization_no' => $param['organization_no']], false);
        if (!empty($check)) {
            return true;
        }
        $job_number = $this->getModel()->countStaff(['organization_no' => $param['organization_no']]);
        $this->addData([
            'user_no' => $param['user_no'] ?? '',
            'organization_no' => $param['organization_no'] ?? '',
            'job_number' => $job_number + 1,
            'staff_type' => 'all',
            'parent_staff_id' => 0,
            'state' => 1,
        ]);
        return true;
    }

    /**
     * 修改员工
     *
     * @return bool
     */
    public function editData($param = [])
    {
        if (!isset($this->transHook[__FUNCTION__])) {
            return $this->transHook(__FUNCTION__, func_get_args());
        }
        $this->getModel()->editData($param);
        DepartmentStaff::_addDepartmentStaff(['staff_id' => $param['staff_id'], 'department_ids' => $param['department_ids']]);
        RoleStaff::_addRoleStaff(['staff_id' => $param['staff_id'], 'role_ids' => $param['role_ids']]);
        return true;
    }
}
