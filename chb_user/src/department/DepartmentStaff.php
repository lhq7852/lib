<?php

namespace chb_user\department;

use chb_lib\common\RedisToolTrait;
use chb_lib\common\ServiceTrait;

class DepartmentStaff
{

    use RedisToolTrait, ServiceTrait;

    /**
     * 添加部门员工
     *
     * @param array $param
     * @return void
     */
    public function addDepartmentStaff($param = [])
    {
        $this->deleteData(['staff_id' => $param['staff_id']]);
        if (!empty($param['department_ids'])) {
            $department_ids = is_array($param['department_ids']) ? $param['department_ids'] : explode(",", trim($param['department_ids']));
            $data = [];
            foreach ($department_ids as $department_id) {
                $data[] = [
                    'department_id' => $department_id,
                    'staff_id' => $param['staff_id'],
                ];
            }
            if (!empty($data)) {
                $this->getModel()->addData($data);
            }
        }
    }
}
