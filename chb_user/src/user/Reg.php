<?php

/**
 * 用户登录权限
 */

namespace chb_user\user;

use chb_lib\common\exception\Err;
use chb_lib\common\SingleTrait;
use chb_lib\common\UtilsTrait;
use chb_rpc\message\MessageRpc;

class Reg extends Passport
{
    use UtilsTrait, SingleTrait;

    /**
     * 手机注册
     *
     * @param array $param
     * @return array
     */
    public function mobileReg($param = [])
    {
        if (empty($param['account'])) {
            Err::errorMsg(510);
        }
        if (empty($param['mobile_code'])) {
            Err::errorMsg(512);
        }
        if (empty($param['password'])) {
            Err::errorMsg(511);
        }
        if (empty($param['confirm_password'])) {
            Err::errorMsg(513);
        }
        if ($param['confirm_password'] != $param['password']) {
            Err::errorMsg(514);
        }
        
        //检查短信验证码
        MessageRpc::_verifySmsCode(['mobile' => ($param['area_code'] ?? '86') . $param['account'], 'code' => $param['mobile_code']]);

        $info = Account::_getUserInfoByLogin($param);
        if (!empty($info)) {
            Err::errorMsg(522);
        }
        $account_id = Account::_regAccount([
            'type' => 'phone',
            'account' => $param['account'],
            'password' => $param['password'],
        ]);
        return $this->loginSuccess([
            'account_id' => $account_id,
            'account' => $param['account'],
        ]);
    }

    /**
     * 邮件注册
     */
    public function emailReg($param = [])
    {
        if (empty($param['account'])) {
            Err::errorMsg(510);
        }
        if (empty($param['email_code'])) {
            Err::errorMsg(512);
        }
        if (empty($param['password'])) {
            Err::errorMsg(511);
        }
        if (empty($param['confirm_password'])) {
            Err::errorMsg(513);
        }
        if ($param['confirm_password'] != $param['password']) {
            Err::errorMsg(514);
        }
        //检查验证码
        MessageRpc::_verifyEmailCode(['email' => $param['account'], 'code' => $param['email_code']]);

        $info = Account::_getUserInfoByLogin($param);
        if (!empty($info)) {
            Err::errorMsg(522);
        }
        $account_id = Account::_regAccount([
            'type' => 'email',
            'account' => $param['account'],
            'password' => $param['password'],
        ]);
        return $this->loginSuccess([
            'account_id' => $account_id,
            'account' => $param['account'],
        ]);
    }
}
