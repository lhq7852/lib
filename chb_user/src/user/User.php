<?php

namespace chb_user\user;

use chb_lib\common\RedisToolTrait;
use chb_lib\common\ServiceTrait;
use chb_user\union\UnionBookAccount;
use chb_user\union\UnionBookUser;
use chb_user\user\model\UserModel;

class User
{
    protected $account_no;

    use RedisToolTrait, ServiceTrait;

    /**
     * 用户详情
     *
     * @param array $param
     * @return array
     */
    public function getDetailInfo($param = [])
    {
        if (method_exists($this, "getDataByRedisHook") && !isset($this->redisHook[$this->createHookKey(__FUNCTION__, func_get_args())])) {
            return $this->getDataByRedisHook(__FUNCTION__, func_get_args(), false);
        }
        $info = User::_getUserInfo([
            'user_no' => request()->user['user_no'],
        ], false);
        if (empty($info)) {
            return [];
        }
        $info['phone'] = base64_decode($info['phone']);
        $info['email'] = base64_decode($info['email']);
        return $info;
    }

    /**
     * 设置模糊搜索条件
     *
     * @param array $param
     * @param [type] $method
     * @return object
     */
    protected function setFilterWhere(&$param = [], $method = null)
    {
        if (!empty($param['keyword'])) {
            $param['keyword'] = base64_encode($param['keyword']);
        }
        return $this;
    }

    /**
     * 身份证验证成功-生成用户
     *
     * @return bool
     */
    public function regUser($param = [])
    {
        $this->account_no = $param['account_no'];
        $data = [
            'user_name' => base64_encode($param['card_name']),
            'user_no' => md5($param['card_no']),
            'avatar' => '',
            'sex' => 0,
            'state' => 1,
            'mode' => 'auth',
        ];
        $this->addData($data);
        return $data['user_no'];
    }

    /**
     * 添加监听
     *
     * @return bool
     */
    protected function listenAdd()
    {
        if (!empty($this->dataInfo['user_no'])) {
            Account::_editData([
                'user_no' => $this->dataInfo['user_no'],
            ], ['account_no' => $this->account_no]);

            //数据加入联盟
            UnionBookUser::_addUnion($this->dataInfo);
            $account = Account::_getInfo(['account_no' => $this->account_no], false);
            $account['user_no'] = $this->dataInfo['user_no'];
            UnionBookAccount::_addUnion($account);
        }
    }

    /**
     * 获取用户姓名列表
     */
    public function getUserNameList($userNo = [])
    {
        if (empty($userNo)) {
            return [];
        }
        if (method_exists($this, "getDataByRedisHook") && !isset($this->redisHook[$this->createHookKey(__FUNCTION__, func_get_args())])) {
            return $this->getDataByRedisHook(__FUNCTION__, func_get_args(), false);
        }
        $data = $this->getDataList(['user_no' => $userNo, "state" => '1', "field" => UserModel::field_2()], false);
        if (empty($data)) {
            return $data;
        }
        $list = [];
        foreach ($data as $v) {
            $list[$v['user_no']] = base64_decode($v['user_name']);
        }
        return $list;
    }

    /**
     * 同步联盟账号数据
     */
    public function syncUnion($param = [])
    {
        $info = UnionBookUser::_getUnion(['user_no' => $param['user_no']]);
        if (empty($info)) {
            return false;
        }
        $data = [
            'user_name' => $info['user_name'],
            'user_no' => $info['user_no'],
            'avatar' => $info['avatar'],
            'sex' => 0,
            'state' => 1,
            'mode' => 'union',
        ];
        $this->getModel()->addData($data);
        return true;
    }
}
