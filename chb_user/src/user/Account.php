<?php

namespace chb_user\user;

use chb_lib\common\exception\Err;
use chb_lib\common\RedisToolTrait;
use chb_lib\common\ServiceTrait;
use chb_lib\common\UtilsTrait;
use chb_user\user\model\AccountModel;

class Account
{

    use RedisToolTrait, ServiceTrait, UtilsTrait;

    /**
     * 监听用户登录
     *
     * @param [type] $user_id
     * @return void
     */
    public function listenLogin($param = [])
    {
        if (empty($param['account_id'])) {
            return false;
        }
        $this->editData([
            'account_id' => $param['account_id'],
            'last_login_time' => date("Y-m-d H:i:s"),
            'last_login_ip' => request()->ip(),
        ]);
        LoginAccountLog::_saveLogQueue([
            'account_id' => $param['account_id'],
            'type' => $param['type'] ?? 'login',
            'login_mode' => $param['login_mode'],
        ]);
        return true;
    }

    /**
     * 获取账号信息
     *
     * @param array $param
     * @return void|array
     */
    public function getAccountInfo($param = [])
    {
        if (!empty($param['account_id'])) {
            $info = $this->getInfo(['account_id' => $param['account_id'], 'field' => AccountModel::field_2()], false);
        }
        if (!empty($param['account_no'])) {
            $info = $this->getInfo(['account_no' => $param['account_no'], 'field' => AccountModel::field_2()], false);
        }
        if (empty($info)) {
            Err::errorMsg(830);
        }
        $info['type_info'] = base64_decode($info['type_info']);
        $info['phone'] = $info['type'] == 'phone' ? $info['type_info'] : '';
        $info['email'] = $info['type'] == 'email' ? $info['type_info'] : '';
        $info['phone_txt'] = $this->mobileRegular($info['phone']);
        $info['email_txt'] = $this->emailRegular($info['email']);
        return $info;
    }

    /**
     * 根据登录账号获取信息
     *
     * @param array $param
     * @return array
     */
    public function getUserInfoByLogin($param = [])
    {
        return $this->getInfo([
            'type_info' => base64_encode($param['account']),
        ], false);
    }

    /**
     * 注册账号
     *
     * @return void
     */
    public function regAccount($param = [])
    {
        if (empty($param['type'])) {
            Err::errorMsg(710);
        }
        $account_id = $this->addData([
            'type' => $param['type'] ?? '',
            'account_no' => md5($param['account']),
            'password' => empty($param['password']) ? '' : md5(md5($param['password'])),
            'type_info' => base64_encode($param['account']),
            'wechat_open_id' => $param['wechat_open_id'] ?? '',
            'wechat_union_id' => $param['wechat_union_id'] ?? '',
            'headimgurl' => $param['headimgurl'] ?? '',
            'person_no' => $param['person_no'] ?? '',
            'area_code' => $param['type'] != 'phone' ? "" : ($param['area_code'] ?? '86'),
            'user_no' => '',
            'last_login_time' => date("Y-m-d H:i:s"),
            'last_login_ip' => request()->ip(),
            'state' => 1,
        ]);
        return $account_id;
    }

    /**
     * 同步联盟账号数据
     */
    public function syncUnion($param = [])
    {
        $data = [
            'account_no' => !empty($param['phone']) ? $param['phone_account_no'] : $param['email_account_no'],
            'user_no' => $param['user_no'],
            'password' => $param['password'],
            'phone' => empty($param['phone']) ? null : base64_encode($param['phone']),
            'email' => empty($param['email']) ? null : base64_encode($param['email']),
            'wechat_union_id' => $param['wechat_union_id'] ?? '',
            'state' => 1,
        ];
        $account_id = $this->getModel()->addData($data);
        return $account_id;
    }

}
