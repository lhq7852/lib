<?php

namespace chb_user\user;

use chb_lib\common\exception\Err;
use chb_lib\common\RedisToolTrait;
use chb_lib\common\ServiceTrait;

class UserConfig
{

    use RedisToolTrait, ServiceTrait;

    /**
     * 详情
     */
    public function getInfo($param = [])
    {
        if (empty(request()->user['user_no'])) {
            Err::errorMsg(831);
        }
        $info = $this->getModel()->getInfo(['config_pk' => $param['config_pk'], 'user_no' => request()->user['user_no'] ?? '']);
        $value = json_decode($info['value'], true);
        return [
            'config_pk' => $param['config_pk'],
            'value' => is_array($value) ? $value : $info['value'],
        ];
    }

    /**
     * 保存密码
     */
    public function savePassword($param = [])
    {
        if (empty($param['password'])) {
            Err::errorMsg(511);
        }
        if (mb_strlen($param['password']) > 6) {
            Err::errorMsg(523);
        }
        $info = $this->getInfo(['config_pk' => 'suopingmima', 'account_no' => request()->user['account_no']]);
        if (empty($info)) {
            return $this->addData([
                'user_no' => request()->user['user_no'] ?? '',
                'account_no' => request()->user['account_no'] ?? '',
                'config_pk' => "suopingmima",
                'type_no' => "suopingmima",
                'name' => "锁屏密码",
                'value' => md5($param['password']),
                'state' => 1,
                'remark' => "锁屏密码",
            ]);
        } else {
            return $this->editData([
                "user_config_id" => $info['user_config_id'],
                'value' => md5($param['password']),
            ]);
        }
    }

    /**
     * 检查密码
     */
    public function checkPassword($param = [])
    {
        if (empty($param['password'])) {
            Err::errorMsg(511);
        }
        if (mb_strlen($param['password']) > 6) {
            Err::errorMsg(523);
        }
        $info = $this->getInfo(['config_pk' => 'suopingmima', 'account_no' => request()->user['account_no']], false);
        if (empty($info)) {
            Err::errorMsg(524);
        }
        return [
            'check' => $info['value'] != md5($param['password']) ? 0 : 1,
        ];
    }

    /**
     * 保存联盟配置
     */
    public function saveUnionConfig($param = [])
    {
        if (!isset($param['value'])) {
            Err::errorMsg(800);
        }
        $info = $this->getInfo(['config_pk' => $param['config_pk'], 'user_no' => request()->user['user_no']], false);
        if (empty($info)) {
            return $this->addData([
                'user_no' => request()->user['user_no'],
                'account_no' => request()->user['account_no'] ?? '',
                'config_pk' => $param['config_pk'],
                'type_no' => "union_state",
                'name' => "用户联盟配置",
                'value' => $param['value'],
                'state' => 1,
                'remark' => "用户联盟配置",
            ]);
        } else {
            return $this->editData([
                "user_config_id" => $info['user_config_id'],
                'value' => $param['value'],
            ]);
        }
    }

}
