<?php

/**
 * 日志
 *
 */

namespace chb_user\user;

use chb_lib\common\RedisToolTrait;
use chb_lib\common\ServiceTrait;
use think\facade\Queue;

class LoginAccountLog
{
    use ServiceTrait, RedisToolTrait;

    /**
     * 保存日志队列
     *
     * @return bool
     */
    public function saveLogQueue($param = [])
    {
        $data = [
            'account_id' => $param['account_id'],
            'type' => $param['type'] ?? 'login',
            'login_mode' => base64_encode($param['login_mode']),
            'ip' => request()->ip(),
            'http_referer' => $_SERVER['HTTP_REFERER'] ?? ($_SERVER['HTTP_ORIGIN'] ?? ''),
            'user_agent' => $_SERVER['HTTP_USER_AGENT'] ?? '',
        ];
        Queue::push("app\job\LoginLogJob@accountLog", $data, 'login_log:account');
        return true;
    }

    /**
     * 保存日志
     *
     * @param [type] $param
     * @return bool
     */
    public function saveLog($param)
    {
        $this->getModel()->addData($param);
        return true;
    }

}
