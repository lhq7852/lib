<?php

/**
 * 添加用户验证
 */

namespace chb_user\user\validate;

use chb_lib\common\ValidateTrait;
use think\Validate;

class AccountValidate extends Validate
{
    use ValidateTrait;

    protected $being = "account_no"; //唯一字段检查

    protected $rule = [
        'account_no' => 'require|max:200|checkBeing',
    ];

}
