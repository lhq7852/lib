<?php

/**
 * 添加用户验证
 */

namespace chb_user\user\validate;

use chb_lib\common\ValidateTrait;
use think\Validate;

class UserValidate extends Validate
{
    use ValidateTrait;

    protected $being = "user_no"; //唯一字段检查

    protected $rule = [
        'user_name' => 'require|max:200',
        'user_no' => 'require|max:200|checkBeing',
    ];

}
