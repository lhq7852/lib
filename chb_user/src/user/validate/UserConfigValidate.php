<?php

/**
 * 添加用户验证
 */

namespace chb_user\user\validate;

use chb_lib\common\ValidateTrait;
use think\Validate;

class UserConfigValidate extends Validate
{
    use ValidateTrait;

    protected $being = "config_pk+account_no"; //唯一字段检查

    protected $rule = [
        'config_pk' => 'require|max:200|checkBeing',
        'value' => 'require|max:5000',
    ];

}
