<?php

/**
 * 用户登录权限
 */

namespace chb_user\user;

use chb_lib\common\auth_code\UserToken;
use chb_lib\common\BaseSystem;
use chb_lib\common\exception\Err;
use chb_lib\common\RedisToolTrait;
use chb_lib\common\SingleTrait;
use chb_lib\common\system\SystemApi;
use chb_lib\common\UtilsTrait;
use chb_rpc\external_api\WechatRpc;
use chb_rpc\message\MessageRpc;
use chb_user\identity_info\Identity;
use chb_user\identity_info\Organization;
use chb_user\identity_info\Person;
use chb_user\union\UnionBookAccount;
use chb_user\user\Account;
use Fastknife\Service\BlockPuzzleCaptchaService;
use think\facade\Validate;

class Passport
{

    use RedisToolTrait, SingleTrait, UtilsTrait;

    public $expire_time = 60 * 60;

    public function __construct()
    {
        $this->expire_time = (BaseSystem::_getObject("SystemConfig"))::_getKeyConfig("token_expire_time");
        UserToken::setSignKeyType('token_key');
        UserToken::setPrivateKey("token_private_key");
    }

    /**
     * 用户[密码]登录
     *
     * @param array $param
     * @return array
     */
    public function login($param = [])
    {
        if (empty($param['account'])) {
            Err::errorMsg(510);
        }
        if (empty($param['password'])) {
            Err::errorMsg(511);
        }
        //拼图验证码 仅pc端用
        $this->checkErrLogin();

        $info = Account::_getUserInfoByLogin($param);
        if (empty($info)) {
            $info = $this->checkUnion($param);
            if (empty($info)) {
                Err::errorMsg(515);
            }
        }
        if ($info['password'] != md5(md5($param['password']))) {
            Err::errorMsg(516);
        }
        return $this->loginSuccess([
            'account_id' => $info['account_id'],
            'account' => $param['account'],
        ]);
    }

    /**
     * 检查联盟
     *
     * @param [type] $param
     * @return void
     */
    protected function checkUnion($param)
    {
        $unionInfo = UnionBookAccount::_getUnion(['account' => $param['account']]);
        if (empty($unionInfo)) {
            return [];
        }
        //同步联盟数据
        $unionInfo['account_id'] = Account::_syncUnion($unionInfo);
        User::_syncUnion($unionInfo);
        Person::_syncUnion($unionInfo);
        Organization::_syncUnion($unionInfo);
        Identity::_syncUnion($unionInfo);
        return $unionInfo;
    }

    /**
     * 登录成功返回+记录+清除
     *
     * @param array $param
     * @return array
     */
    protected function loginSuccess($param = [])
    {
        $account = Account::_getAccountInfo(['account_id' => $param['account_id']]);
        $token = UserToken::createAppToken($param['account_id'], $this->getSessionId());
        $this->setRedisKey($this->getRedisKayName() . request()->param("platform", '') . $param['account_id'])->setRedisValue($token);
        Account::_listenLogin([
            'account_id' => $param['account_id'],
            'login_mode' => $param['account'],
        ]);
        $this->clearErrLogin(); //清除错误次数缓存
        return [
            'token' => $token,
            'user_no' => $account['user_no'],
        ];
    }

    /**
     * 生成一个短时间使用的登录态
     *
     * @param array $param
     * @return void
     */
    public function createShortToken($param = [])
    {
        $this->expire_time = 60 * 30; //30分钟有效
        $token = UserToken::createAppToken($param['account_id'], $this->getSessionId());
        $this->setRedisKey($this->getRedisKayName() . ($param['platform'] ?? '') . $param['account_id'])->setRedisValue($token);
        return $token;
    }

    /**
     * 用户[短信]登录
     *
     * @param array $param
     * @return array
     */
    public function mobileLogin($param = [])
    {
        if (empty($param['account'])) {
            Err::errorMsg(510);
        }
        if (empty($param['mobile_code'])) {
            Err::errorMsg(512);
        }
        //检查短信验证码
        MessageRpc::_verifySmsCode(['mobile' => ($param['area_code'] ?? '86') . $param['account'], 'code' => $param['mobile_code']]);

        $info = Account::_getUserInfoByLogin($param);
        if (empty($info)) {
            $info = $this->checkUnion($param);
            if (empty($info)) {
                Err::errorMsg(515);
            }
        }
        return $this->loginSuccess([
            'account_id' => $info['account_id'],
            'account' => $param['account'],
        ]);
    }

    /**
     * 用户邮件登录
     */
    public function emailLogin($param = [])
    {
        if (empty($param['account'])) {
            Err::errorMsg(510);
        }
        if (empty($param['email_code'])) {
            Err::errorMsg(512);
        }
        //检查邮件验证码
        MessageRpc::_verifyEmailCode(['email' => $param['account'], 'code' => $param['email_code']]);

        $info = Account::_getUserInfoByLogin($param);
        if (empty($info)) {
            $info = $this->checkUnion($param);
            if (empty($info)) {
                Err::errorMsg(515);
            }
        }
        return $this->loginSuccess([
            'account_id' => $info['account_id'],
            'account' => $param['account'],
        ]);
    }

    /**
     * 微信登录-缓存
     * state : 1 发起中 2 授权成功 可以直接登录 3 授权成功 需要绑定
     * @param array $param
     * @return void|string
     */
    public function setWechatLogin($state = 1, $session_id = '')
    {
        $session_id = empty($session_id) ? $this->getSessionId() : $session_id;
        $this->setRedisKey($this->getRedisKayName() . "wechat:" . $session_id)->setRedisTime(60 * 15)->setRedisValue($state);
        return $session_id;
    }

    /**
     * 微信登录
     */
    public function wechatLogin($param = [])
    {
        $redisKey = $this->getRedisKayName();
        $redisName = $redisKey . "wechatBack:" . $param['session_id'];
        $redisName2 = $redisKey . "wechat:" . $param['session_id'];
        $state = $this->setRedisKey($redisName2)->getRedisArray();
        if (empty($state)) {
            Err::errorMsg(525);
        }
        if ($state == 1) {
            Err::errorMsg(526);
        }
        $data = $this->setRedisKey($redisName)->getRedisArray();
        if (empty($data)) {
            Err::errorMsg(525);
        }
        $info = Account::_getInfo(['wechat_open_id' => $data['openid']], false);
        return $this->loginSuccess([
            'account_id' => $info['account_id'],
            'account' => $data['openid'],
        ]);
    }

    /**
     * 微信登录-回调
     *
     * @param array $param
     * @return true
     */
    public function wechatBack($param = [])
    {
        $client_pk = (BaseSystem::_getObject("SystemInfo"))::_getKeyConfig("system_pk"); //系统标识
        //获取微信信息
        if (empty($param['openid'])) {
            $this->setWechatLogin(1, $param['session_id']);
            return true;
        }
        $userInfo = WechatRpc::_getUserBasicInfo(['client_pk' => $client_pk, 'openid' => $param['openid']]);

        $redisKey = $this->getRedisKayName();
        $redisName = $redisKey . "wechatBack:" . $param['session_id'];
        $redisName2 = $redisKey . "wechat:" . $param['session_id'];
        trace("wechatLogin", "chb_error");
        trace(["userInfo" => $userInfo], "chb_error");
        $this->setRedisKey($redisName)->setRedisTime(60 * 15)->setRedisValue([
            'userInfo' => $userInfo,
            'openid' => $param['openid'],
        ]);

        //检查是否已绑定微信,如果绑定,-登录,如果没绑定,提示注册
        $info = Account::_getInfo(['wechat_open_id' => $param['openid']], false);
        //存在,可以直接登录
        if (!empty($info)) {
            $this->setRedisKey($redisName2)->setRedisTime(60 * 15)->setRedisValue(2);
            return true;
        }
        if (!empty($userInfo['unionid'])) {
            $info = Account::_getInfo(['wechat_union_id' => $userInfo['unionid']], false);
        }
        //不存在openid 添加账号
        Account::_regAccount([
            'type' => 'wechat_gzh',
            'account' => empty($userInfo['nickname']) ? $param['openid'] : $userInfo['nickname'],
            'password' => "",
            'wechat_union_id' => $userInfo['unionid'] ?? '',
            'wechat_open_id' => $param['openid'],
            'headimgurl' => $userInfo['headimgurl'] ?? '',
            'user_no' => $info['user_no'] ?? "",
        ]);
        //存在,可以直接登录
        if (!empty($info)) {
            $this->setRedisKey($redisName2)->setRedisTime(60 * 15)->setRedisValue(2);
            return true;
        }
        //不存在,需要个人实名验证
        $this->setRedisKey($redisName2)->setRedisTime(60 * 15)->setRedisValue(3);
        return true;
    }

    /**
     * 检查错误次数
     *
     * @return bool|exception
     */
    public function checkErrLogin($param = [])
    {
        if (request()->param("platform", '') != 'pc') {
            return true;
        }
        $redisName = $this->getRedisKayName() . md5($_SERVER['HTTP_USER_AGENT']); //UA
        $redisName2 = $this->getRedisKayName() . md5(request()->ip()); //ip
        $err_num = $this->setRedisKey($redisName)->getRedisString() ?? 0;
        $err_num2 = $this->setRedisKey($redisName2)->getRedisString() ?? 0;
        $err_num = $err_num2 > $err_num ? $err_num2 : $err_num;
        $config = (BaseSystem::_getObject("SystemConfig"))::_getKeyConfig("login_error_frozen");
        if ($err_num >= $config['frozen_num']) {
            $h = (int) $config['frozen_hours'];
            $this->forceRedisTime = 60 * 60 * $h;
            $msg = $this->content_replace(lang(517), ['n' => $h], "{", "}");
            Err::errorMsg(517, $msg);
        }
        if ($err_num >= 3) { //必须要拼图验证码
            if (empty($param['token']) || empty($param['pointJson'])) {
                Err::errorMsg(520);
            }
            $this->verification($param);
            return $this->verification($param);
        }
        return true;
    }

    /**
     * 二次验证-行为验证码
     */
    public function verification($param = [])
    {
        try {
            $rules = [
                'token' => ['require'],
                'pointJson' => ['require'],
            ];
            $validate = Validate::rule($rules)->failException(true);
            $validate->check($param);
            $config = config('captcha');
            $service = new BlockPuzzleCaptchaService($config);
            $service->verification($param['token'], $param['pointJson']);
        } catch (\Exception $e) {
            Err::errorMsg(520);
        }
        return true;
    }

    /**
     * 清除错误测试
     *
     * @return bool
     */
    public function clearErrLogin()
    {
        $redisName = $this->getRedisKayName() . md5($_SERVER['HTTP_USER_AGENT']); //UA
        $redisName2 = $this->getRedisKayName() . md5(request()->ip()); //ip
        $this->delRedis($redisName);
        $this->delRedis($redisName2);
        return true;
    }

    /**
     * 登录错误次数
     *
     * @param [type] $msg
     * @return array
     */
    public function setErrLogin($code = '')
    {
        $redisName = $this->getRedisKayName() . md5($_SERVER['HTTP_USER_AGENT']); //UA
        $redisName2 = $this->getRedisKayName() . md5(request()->ip()); //ip
        $err_num = $this->setRedisKey($redisName)->getRedisString() ?? 0;
        $err_num2 = $this->setRedisKey($redisName2)->getRedisString() ?? 0;
        $err_num = $err_num2 > $err_num ? $err_num2 : $err_num;
        $is_verify_image = false;
        $msg = lang($code);
        $config = (BaseSystem::_getObject("SystemConfig"))::_getKeyConfig("login_error_frozen");
        if ($err_num >= $config['verify_image_num']) {
            $is_verify_image = true;
        }
        if ($err_num >= $config['frozen_num']) {
            $h = (int) $config['frozen_hours'];
            $this->forceRedisTime = 60 * 60 * $h;
            $msg = $this->content_replace(lang(517), ['n' => $h], "{", "}");
        } else if ($err_num >= $config['error_tips']) {
            $msg .= $this->content_replace(lang(518), ['n' => $config['frozen_num'] - $err_num], "{", "}");
        }
        $err_num += 1;
        $this->setRedisKey($redisName)->setRedisValue($err_num);
        $this->setRedisKey($redisName2)->setRedisValue($err_num);
        if ($is_verify_image) {
            Err::errorMsg(520, $msg); // 520 显示图像验证码标识
        }
        Err::errorMsg($code, $msg ?? '');
    }

    /**
     * 检查单点登录
     *
     * @param [type] $account_id
     * @param [type] $userToken
     * @return void
     */
    public function checkUserPass($account_id, $userToken)
    {
        $check = $this->setRedisKey($this->getRedisKayName() . request()->param("platform", '') . $account_id)->getRedisString();
        if (!empty($check) && $check != $userToken) {
            Err::errorMsg(401);
        }
        return true;
    }

    /**
     * 登录检查
     *
     * urlencode(base64_encode(base64_encode($user['identity_id'])))
     */
    public function checkLogin($userToken = '', $param = [])
    {
        $data = UserToken::verifyAppToken($userToken, $this->getSessionId());
        if (request()->time() - $data['login_time'] > $this->expire_time) {
            Err::errorMsg(702);
        }
        if (!empty($data['account_id'])) {
            $check = Identity::_pass($data['account_id']);
            // $this->checkUserPass($data['account_id'], $userToken); //检查单点登录
            if (empty($check)) {
                Err::errorMsg(515);
            }
            if ($check['state'] == 2) {
                Err::errorMsg(515);
            }
            SystemApi::_addCollectApi(); //手机接口api
        }
        return $check;
    }

    /**
     * 刷新token
     *
     * @param [type] $userToken
     * @return string
     */
    public function refreshToken($userToken)
    {
        $data = UserToken::verifyAppToken($userToken, $this->getSessionId());
        if (empty($data['account_id'])) {
            Err::errorMsg(402);
        }
        $token = UserToken::createAppToken($data['account_id'], $this->getSessionId());
        $this->setRedisKey($this->getRedisKayName() . $data['account_id'])->setRedisValue($token);
        return $token;
    }

    /**
     * 重置密码-找回密码
     */
    public function resetPassword($param = [])
    {
        if (empty($param['password'])) {
            Err::errorMsg(511);
        }
        if (empty($param['confirm_password'])) {
            Err::errorMsg(513);
        }
        if ($param['confirm_password'] != $param['password']) {
            Err::errorMsg(514);
        }
        $check = false;
        if (!empty($param['email']) && !empty($param['email_code'])) {
            //检查邮件验证码
            MessageRpc::_verifyEmailCode(['email' => $param['email'], 'code' => $param['email_code']]);
            $check = true;
        }
        if (!empty($param['mobile']) && !empty($param['mobile_code'])) {
            //检查短信验证码
            MessageRpc::_verifySmsCode(['mobile' => ($param['area_code'] ?? '86') . $param['phone'], 'code' => $param['mobile_code']]);
            $check = true;
        }
        if (!$check) {
            Err::errorMsg(530);
        }
        $info = Account::_getUserInfoByLogin([
            'account' => empty($param['mobile']) ? $param['email'] : $param['mobile'],
        ]);
        if (!$info) {
            Err::errorMsg(515);
        }
        Account::_editData([
            'account_id' => $info['account_id'],
            'password' => md5(md5($param['password'])),
        ]);
        return true;
    }
}
