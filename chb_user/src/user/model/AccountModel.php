<?php

/**
 * 后台用户模型
 */

namespace chb_user\user\model;

use chb_lib\common\BaseModel;

class AccountModel extends BaseModel
{

    protected $name = "account";
    protected $pk = 'account_id';

    protected $alias = 'a';

    /**
     * 获取指定字段-方法1
     *
     * @return string
     */
    public static function field_1()
    {
        return "a.account_no,a.account_id,a.type_info,a.type,a.user_no,a.wechat_open_id,a.wechat_union_id,a.headimgurl,a.person_no,a.state,a.last_login_time,a.last_login_ip,a.update_user_no,a.update_time";
    }

    /**
     * 获取指定字段-方法2
     *
     * @return string
     */
    public static function field_2()
    {
        return 'account_no,account_id,type,type_info,wechat_union_id,user_no,state';
    }
}
