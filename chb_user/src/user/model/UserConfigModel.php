<?php

/**
 * 模型
 */

namespace chb_user\user\model;

use chb_lib\common\BaseModel;

class UserConfigModel extends BaseModel
{

    protected $name = "user_config";
    protected $pk = 'user_config_id';
}
