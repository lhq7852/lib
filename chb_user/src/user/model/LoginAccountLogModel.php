<?php

/**
 * 后台用户模型
 */

namespace chb_user\user\model;

use chb_lib\common\BaseModel;

class LoginAccountLogModel extends BaseModel
{

    protected $name = 'login_account_log';
    protected $pk = 'login_account_log_id';
    protected $connection = 'log_database';

}
