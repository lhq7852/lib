<?php

/**
 * 后台用户模型
 */

namespace chb_user\user\model;

use chb_lib\common\BaseModel;

class UserModel extends BaseModel
{

    protected $name = 'user';
    protected $pk = 'user_id';

    protected $likeList = ["keyword" => "user_name"]; //设置模糊搜索映射的字段 alias|value

    protected $alias = 'u';

    /**
     * 获取指定字段-方法1
     *
     * @return void
     */
    public static function field_1()
    {
        return "u.user_id,u.user_no,u.user_name,u.avatar,u.state";
    }

    /**
     * 获取指定字段-方法1
     *
     * @return void
     */
    public static function field_2()
    {
        return "u.user_no,u.user_name";
    }


    /**
     * 获取用户信息
     *
     * @param array $param
     * @return array
     */
    public function getUserInfo($param = [])
    {
        $objWhere = $this->alias("u")->leftJoin("account a", "a.user_no = u.user_no");
        if (!empty($param['user_no'])) {
            $objWhere = $objWhere->where("u.user_no", $param['user_no']);
        }
        if (!empty($param['user_id'])) {
            $objWhere = $objWhere->where("u.user_id", $param['user_id']);
        }
        return $objWhere->field(self::field_1() . "," . AccountModel::field_1())->findOrEmpty();
    }
}
