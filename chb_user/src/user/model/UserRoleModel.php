<?php

/**
 * 模型
 */

namespace chb_user\user\model;

use chb_lib\common\BaseModel;

class UserRoleModel extends BaseModel
{

    protected $name = 'user_role';
    protected $pk = 'user_role_id';

}
