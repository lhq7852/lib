<?php
/**
 * 短信-资源接口
 */
namespace chb_rpc\external_api;

use chb_lib\common\RedisToolTrait;
use chb_lib\common\RpcServerTrait;
use chb_lib\common\SingleTrait;

class SmsRpc
{

    use RedisToolTrait, RpcServerTrait, SingleTrait;

    /**
     * 发送-短信
     * @return array
     */
    public function sendSms($param = [])
    {
        return $this->rpcExternalResult(array_merge(['rpc' => 'external_api.SmsApi.sendMsg'], $param));
    }

    /**
     * 发送-邮件
     * @return array
     */
    public function sendEmail($param = [])
    {
        return $this->rpcExternalResult(array_merge(['rpc' => 'external_api.SmsApi.sendEmail'], $param));
    }

    /**
     * 消息-添加签名
     * @return array
     */
    public function addSmsSign($param = [])
    {
        return $this->rpcExternalResult(array_merge(['rpc' => 'external_api.SmsApi.addSmsSign'], $param));
    }

    /**
     * 消息-查询签名
     * @return array
     */
    public function querySmsSign($param = [])
    {
        return $this->rpcExternalResult(array_merge(['rpc' => 'external_api.SmsApi.querySmsSign'], $param));
    }

    /**
     * 消息-删除签名
     * @return array
     */
    public function deleteSmsSign($param = [])
    {
        return $this->rpcExternalResult(array_merge(['rpc' => 'external_api.SmsApi.deleteSmsSign'], $param));
    }

    /**
     * 消息-编辑签名
     * @return array
     */
    public function modifySmsSign($param = [])
    {
        return $this->rpcExternalResult(array_merge(['rpc' => 'external_api.SmsApi.modifySmsSign'], $param));
    }

    /**
     * 消息-添加消息模板
     * @return array
     */
    public function addSmsTemplate($param = [])
    {
        return $this->rpcExternalResult(array_merge(['rpc' => 'external_api.SmsApi.addSmsTemplate'], $param));
    }

    /**
     * 消息-查询消息模板
     * @return array
     */
    public function querySmsTemplate($param = [])
    {
        return $this->rpcExternalResult(array_merge(['rpc' => 'external_api.SmsApi.querySmsTemplate'], $param));
    }

    /**
     * 消息-删除消息模板
     * @return array
     */
    public function deleteSmsTemplate($param = [])
    {
        return $this->rpcExternalResult(array_merge(['rpc' => 'external_api.SmsApi.deleteSmsTemplate'], $param));
    }

    /**
     * 消息-编辑消息模板
     * @return array
     */
    public function modifySmsTemplate($param = [])
    {
        return $this->rpcExternalResult(array_merge(['rpc' => 'external_api.SmsApi.modifySmsTemplate'], $param));
    }

}
