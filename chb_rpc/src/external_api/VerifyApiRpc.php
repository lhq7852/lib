<?php
/**
 * 验证类-资源接口
 */
namespace chb_rpc\external_api;

use chb_lib\common\RedisToolTrait;
use chb_lib\common\RpcServerTrait;
use chb_lib\common\SingleTrait;

class VerifyApiRpc
{

    use RedisToolTrait, RpcServerTrait, SingleTrait;

    /**
     * 个人身份验证
     * @return array
     */
    public function getPersonIdCard($param = [])
    {
        return $this->rpcExternalResult(array_merge(['rpc' => 'external_api.AlicloudApi.getPersonIdCard'], $param));
    }

    /**
     * 机构验证
     * @return array
     */
    public function getCompanyInfo($param = [])
    {
        return $this->rpcExternalResult(array_merge(['rpc' => 'external_api.AlicloudApi.getCompanyInfo'], $param));
    }

    /**
     * 识别身份证图片
     * @return array
     */
    public function identityCard($param = [])
    {
        return $this->rpcExternalResult(array_merge(['rpc' => 'external_api.AlicloudApi.identityCard'], $param));
    }

    /**
     * 识别营业执照图片
     * @return array
     */
    public function businessLicense($param = [])
    {
        return $this->rpcExternalResult(array_merge(['rpc' => 'external_api.AlicloudApi.businessLicense'], $param));
    }

    /**
     * 公章识别
     * @return array
     */
    public function ocrOfficialSeal($param = [])
    {
        return $this->rpcExternalResult(array_merge(['rpc' => 'external_api.AlicloudApi.ocrOfficialSeal'], $param));
    }

    /**
     * 活体比较-识别
     * @return array
     */
    public function compareRequest($param = [])
    {
        return $this->rpcExternalResult(array_merge(['rpc' => 'external_api.AlicloudApi.compareRequest'], $param));
    }
}
