<?php
/**
 * 宝塔-资源接口
 */
namespace chb_rpc\external_api;

use chb_lib\common\RedisToolTrait;
use chb_lib\common\RpcServerTrait;
use chb_lib\common\SingleTrait;

class WechatRpc
{

    use RedisToolTrait, RpcServerTrait, SingleTrait;

    /**
     * pc授权发起页-租户授权给平台
     *
     * @param array $param
     * @return array|Exception
     */
    public function getPcPass($param = [])
    {
        return $this->rpcExternalResult(array_merge(['rpc' => 'external_api.WechatApi.getPcPass'], $param));
    }

    /**
     * 用户-微信登录授权发起
     *
     * @param array $param
     * @return array|Exception
     */
    public function getLoginPass($param = [])
    {
        return $this->rpcExternalResult(array_merge(['rpc' => 'external_api.WechatApi.getLoginPass'], $param));
    }

    /**
     * 用code 换 access_token
     * code
     * appid
     * @return array
     */
    public function getAccessToken($param = [])
    {
        return $this->rpcExternalResult(array_merge(['rpc' => 'external_api.WechatApi.getAccessToken'], $param));
    }

    /**
     * 获取用户信息
     * access_token
     * openid
     * @return array
     */
    public function getUserInfo($param = [])
    {
        return $this->rpcExternalResult(array_merge(['rpc' => 'external_api.WechatApi.getUserInfo'], $param));
    }

    /**
     * 场景二维码
     * access_token
     * openid
     * @return array
     */
    public function getSceneQrcode($param = [])
    {
        return $this->rpcExternalResult(array_merge(['rpc' => 'external_api.WechatApi.getSceneQrcode'], $param));
    }

    /**
     * 获取用户基础信息
     * access_token
     * openid
     * @return array
     */
    public function getUserBasicInfo($param = [])
    {
        return $this->rpcExternalResult(array_merge(['rpc' => 'external_api.WechatApi.getUserBasicInfo'], $param));
    }

}
