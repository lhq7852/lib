<?php
/**
 * 宝塔-资源接口
 */
namespace chb_rpc\external_api;

use chb_lib\common\RedisToolTrait;
use chb_lib\common\RpcServerTrait;
use chb_lib\common\SingleTrait;

class BtRpc
{

    use RedisToolTrait, RpcServerTrait, SingleTrait;

    /**
     * 添加网站
     *
     * @param array $param
     * @return array|Exception
     */
    public function addWebsite($param = [])
    {
        return $this->rpcExternalResult(array_merge(['rpc' => 'external_api.BtApi.addWebsite'], $param));
    }

    /**
     * 网站nginx配置
     *
     * @param array $param
     * @return array|Exception
     */
    public function saveNginxFileBody($param = [])
    {
        return $this->rpcExternalResult(array_merge(['rpc' => 'external_api.BtApi.saveNginxFileBody'], $param));
    }

    /**
     * 保存网站根目录
     *
     * @param array $param
     * @return array|Exception
     */
    public function setWebsitePath($param = [])
    {
        return $this->rpcExternalResult(array_merge(['rpc' => 'external_api.BtApi.setWebsitePath'], $param));
    }

    /**
     * 删除网站
     *
     * @param array $param
     * @return array|Exception
     */
    public function deleteSite($param = [])
    {
        return $this->rpcExternalResult(array_merge(['rpc' => 'external_api.BtApi.deleteSite'], $param));
    }

    /**
     * 上传文件
     *
     * @param array $param
     * @return array|Exception
     */
    public function uploadFile($param = [])
    {
        return $this->rpcExternalResult(array_merge(['rpc' => 'external_api.BtApi.uploadFile'], $param));
    }

    /**
     * 解压文件tar
     *
     * @param array $param
     * @return array|Exception
     */
    public function unzip($param = [])
    {
        return $this->rpcExternalResult(array_merge(['rpc' => 'external_api.BtApi.unzip'], $param));
    }

    /**
     * 删除文件
     *
     * @param array $param
     * @return array|Exception
     */
    public function deleteFile($param = [])
    {
        return $this->rpcExternalResult(array_merge(['rpc' => 'external_api.BtApi.deleteFile'], $param));
    }

    /**
     * 通用调用方法
     *
     * @param array $param
     * @return array|Exception
     */
    public function common($param = [])
    {
        return $this->rpcExternalResult(array_merge(['rpc' => 'external_api.BtApi.common'], $param));
    }

}
