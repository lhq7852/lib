<?php

namespace chb_rpc\message;

use chb_lib\common\RedisToolTrait;
use chb_lib\common\RpcServerTrait;
use chb_lib\common\SingleTrait;

class MessageRpc
{

    use RedisToolTrait, RpcServerTrait, SingleTrait;

    /**
     * 短信验证
     * @return array
     */
    public function verifySmsCode($param = [])
    {
        return $this->rpcMessageResult(array_merge(['rpc' => 'sms.SmsMessage.verifyCode'], $param));
    }

    /**
     * 邮箱验证
     * @return array
     */
    public function verifyEmailCode($param = [])
    {
        return $this->rpcMessageResult(array_merge(['rpc' => 'sms.SmsMessage.verifyEmailCode'], $param));
    }

}
