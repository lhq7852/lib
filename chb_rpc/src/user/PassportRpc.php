<?php

namespace chb_rpc\user;

use chb_lib\common\RedisToolTrait;
use chb_lib\common\RpcServerTrait;
use chb_lib\common\SingleTrait;

class PassportRpc
{
    use RpcServerTrait, SingleTrait, RedisToolTrait;
    /**
     * 登录检查
     *
     * @param array $param
     * @return array
     */
    public function checkLogin($userToken = '', $param = [])
    {
        if (!isset($this->redisHook[$this->createHookKey(__FUNCTION__, func_get_args())])) {
            return $this->getDataByRedisHook(__FUNCTION__, func_get_args(), false); // func_get_args() == [$param]
        }
        return $this->rpcUserResult(['rpc' => 'user.user.checkLogin', 'userToken' => $userToken]);
    }
}
