<?php

namespace chb_rpc\resource_client;

use chb_lib\common\BaseSystem;
use chb_lib\common\RedisToolTrait;
use chb_lib\common\RpcServerTrait;
use chb_lib\common\SingleTrait;

class ResourceClientRpc
{

    use RedisToolTrait, RpcServerTrait, SingleTrait;

    /**
     * 获取我的应用-app_id
     * @return array
     */
    public function getAppIdByClientPk()
    {
        $client_owner = (BaseSystem::_getObject("SystemInfo"))::_getKeyConfig("owner");
        return $this->rpcUserResult(['rpc' => 'resource_client.ResourceClient.getAppIdByClientPk', 'client_pk' => config("chb.system"), 'client_owner' => $client_owner]);
    }

    /**
     * 获取平台应用-app_id
     * @return array
     */
    public function getPlatformByAppId($param = [])
    {
        return $this->rpcPlatformResult(['rpc' => 'resource_client.ResourceClient.getPlatformByAppId', 'app_id' => $param['app_id'] ?? '']);
    }

}
