<?php

namespace chb_rpc\system;

use chb_lib\common\SingleTrait;
use chb_lib\common\RedisToolTrait;
use chb_lib\common\RpcServerTrait;

class SystemRedisRpc
{

    use RpcServerTrait, SingleTrait;

    /**
     * 键值对获取
     * 示例
     * @param [type] $redis_key
     * @return array
     */
    public function getRedisConfig($redis_key)
    {
        return $this->rpcUserResult(['rpc' => 'system.SystemRedis.getRedisConfig', 'redis_key' => $redis_key]);
    }
}
