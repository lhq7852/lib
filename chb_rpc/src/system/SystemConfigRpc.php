<?php

namespace chb_rpc\system;

use chb_lib\common\SingleTrait;
use chb_lib\common\RedisToolTrait;
use chb_lib\common\RpcServerTrait;

class SystemConfigRpc
{

    use RedisToolTrait, RpcServerTrait, SingleTrait;

    /**
     * 键值对获取
     * 示例
     * @param [type] $alias
     * @return array
     */
    public function getKeyConfig($alias)
    {
        return $this->rpcUserResult(['rpc' => 'system.SystemConfig.getKeyConfig', 'alias' => $alias]);
    }
}
