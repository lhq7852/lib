@echo off
echo Stopping nginx...
cd "C:\nginx"
nginx.exe -s stop
::taskkill /F /IM nginx.exe > nul
echo Stopping PHP FastCGI...
taskkill /F /IM php-cgi.exe > nul

echo Start Nginx... 
cd "C:\nginx"
start nginx
echo Start PHP7.1.8 FastCGI... 
cd "C:\php"
php-cgi.exe -b 127.0.0.1:9000