<?php

/**
 * 验证
 */

namespace chb_union_center\resource_client\validate;

use chb_lib\common\ValidateTrait;
use think\Validate;

class ResourceClientValidate extends Validate
{
    use ValidateTrait;

    protected $rule = [
        'client_name' => 'require|max:200',
        'sort' => 'number',
    ];
}
