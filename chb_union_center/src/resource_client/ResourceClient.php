<?php

namespace chb_union_center\resource_client;

use chb_lib\common\BaseSystem;
use chb_lib\common\RedisToolTrait;
use chb_lib\common\ServiceTrait;

class ResourceClient
{

    use RedisToolTrait, ServiceTrait;

    /**
     * 设置入库的参数,可重写
     *
     * @param [type] $param
     * @return object
     */
    protected function setFieldParam(&$param = [], $mode = "add")
    {
        if ($mode == 'add') {
            $param['app_id'] = $this->getNonceStr(12);
            $param['app_secret'] = $this->getNonceStr(32);
        }
        return $this;
    }

    /**
     * 根据应用标识获取应用的app_id
     *
     * @param array $param
     * @return void|string
     */
    public function getPlatformByAppId($param = [])
    {
        $info = $this->getInfo(['app_id' => $param['app_id']], false);
        return $info['client_user_no'] ?? "";
    }

    /**
     * 根据应用所有者,获取对应的应用列表
     *
     * @param array $param
     * @return void|string
     */
    public function getClientList($param = [])
    {
        $data = $this->getDataList(['client_user_no' => $param['client_owner'] ?? ''], false);
        $config = (BaseSystem::_getObject("SystemConfig"))::_getKeyConfig("union_config");
        return $this->jsonEncrypt($data, $config['public_key'], request()->time());
    }

}
