<?php

/**
 * 后台用户模型
 */

namespace chb_union_center\union\model;

use chb_lib\common\BaseModel;

class UnionMemberModel extends BaseModel
{

    protected $name = 'union_member';
    protected $pk = 'union_member_id';

    protected $hidden = [];
    protected $likeList = ["keyword" => "user_no"]; //设置模糊搜索映射的字段 alias|value

    /**
     * 获取联盟账号-盟主
     */
    public function getUnionLeaderList()
    {
        return $this->where("is_leader", 1)->select()->toArray();
    }

    /**
     * 获取联盟账号-非盟主
     */
    public function getUnionMemberList()
    {
        return $this->where("is_leader", 0)->select()->toArray();
    }

    /**
     * 获取联盟增量数据
     *
     * @param array $param
     * @return void
     */
    public function getUnionMemberIncrementList($param = [])
    {
        if (empty($param['start_time'])) {
            return $this->setIsDeleteTime()->select()->toArray();
        }
        return $this->setIsDeleteTime()->where('update_time', ">", $param['start_time'])->select()->toArray();
    }

}
