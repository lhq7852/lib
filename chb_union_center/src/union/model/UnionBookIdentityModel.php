<?php

/**
 * 后台模型
 */

namespace chb_union_center\union\model;

use chb_lib\common\BaseModel;

class UnionBookIdentityModel extends BaseModel
{

    protected $name = 'union_book_identity';
    protected $pk = 'union_book_identity_id';

    protected $hidden = [];
    protected $likeList = ["keyword" => "user_no"]; //设置模糊搜索映射的字段 alias|value

    /**
     * 获取联盟增量数据
     *
     * @param array $param
     * @return void
     */
    public function getUnionIncrementList()
    {
        return $this->setIsDeleteTime()->where('union_state', 1)->select()->toArray();
    }

}
