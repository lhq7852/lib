<?php

namespace chb_union_center\identity_config;

use chb_lib\common\RedisToolTrait;
use chb_lib\common\ServiceTrait;

class IdentityClient
{

    use RedisToolTrait, ServiceTrait;

    protected $typeList = ['show' => 'getShowList'];

    /**
     * 重写- 添加
     *
     * @return bool
     */
    public function addData($param = [])
    {
        $this->validateData($param, __FUNCTION__);
        $arrClientPk = explode(",", $param['client_pk']);
        foreach ($arrClientPk as $client_pk) {
            $data[] = [
                'identity_pk' => $param['identity_pk'],
                'client_pk' => $client_pk,
            ];
        }
        $this->getModel()->deleteData(['identity_pk' => $param['identity_pk']]);
        $this->getModel()->insertAll($data);
        return true;
    }

    /**
     * 显示列表
     *
     * @param array $param
     * @return array
     */
    protected function getShowList($param = [])
    {
        return $this->getPageList(['field' => "a.identity_type,r.*"]);
    }
}
