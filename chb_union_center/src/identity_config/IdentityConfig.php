<?php

namespace chb_union_center\identity_config;

use chb_lib\common\BaseSystem;
use chb_lib\common\RedisToolTrait;
use chb_lib\common\ServiceTrait;

class IdentityConfig
{

    use RedisToolTrait, ServiceTrait;

    protected $typeList = ['show' => 'getShowRoleList'];

    /**
     * 显示认证列表
     *
     * @param array $param
     * @return array
     */
    protected function getShowRoleList($param = [])
    {
        return $this->getPageList(['is_show' => 1, 'parent_id' => $param['parent_id'] ?? 0]);
    }

    /**
     * 获取租户的身份配置
     *
     * @param array $param
     * @return void|array
     */
    public function getIdentityConfigList($param = [])
    {
        $identityClient = IdentityClient::_getDataList(['client_pk' => explode(",", $param['clientPk'])], false);
        if (empty($identityClient)) {
            return [];
        }
        $identityPk = array_column($identityClient, 'identity_pk');
        $identityPk = array_diff($identityPk, ['system_user']);
        if (empty($identityPk)) {
            return [];
        }
        $data = $this->getDataList(['identity_pk' => $identityPk], false);
        $config = (BaseSystem::_getObject("SystemConfig"))::_getKeyConfig("union_config");
        return $this->jsonEncrypt($data, $config['public_key'], request()->time());
    }
    
}
