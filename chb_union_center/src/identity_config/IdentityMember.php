<?php

namespace chb_union_center\identity_config;

use chb_lib\common\ServiceTrait;
use chb_lib\common\RedisToolTrait;

class IdentityMember
{

    use RedisToolTrait, ServiceTrait;

    protected $typeList = ['show' => 'getShowList'];

    /**
     * 显示列表
     *
     * @param array $param
     * @return array
     */
    protected function getShowList($param = [])
    {
        return $this->getPageList($param);
    }
}
