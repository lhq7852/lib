<?php

/**
 * 模型
 */

namespace chb_union_center\identity_config\model;

use chb_lib\common\BaseModel;

class IdentityClientModel extends BaseModel
{

    protected $name = 'identity_client';
    protected $pk = 'identity_client_id';

    protected $hidden = ['delete_time'];
    protected $likeList = ["keyword" => "identity_type"]; //设置模糊搜索映射的字段 alias|value

    protected $alias = 'a';
    /**
     * 链表
     *
     * @param array $param
     * @return array
     */
    public function getPageList($param = [])
    {
        $objWhere = $this->alias($this->alias)->leftJoin("resource_client r", "r.client_pk={$this->alias}.client_pk");
        $this->setWhereLike($param, $objWhere);
        if (!empty($param)) {
            $where = $this->filterSetWhere($param);
        }
        if (!empty($where)) {
            $objWhere = $objWhere->where($where);
        }
        $objWhere = $objWhere->order($this->getOrderSort(empty($param['order_type']) ? 0 : $param['order_type']));
        if (!empty($param['field'])) {
            $objWhere = $objWhere->field($param['field']);
        }
        if (!empty($this->param["first_page_time"])) {
            $objWhere = $objWhere->where("create_time", ">=", date("Y-m-d H:i:s", $this->param["first_page_time"]));
        }
        return $objWhere->paginate($param['pageSize'] ?? 30);
    }
}
