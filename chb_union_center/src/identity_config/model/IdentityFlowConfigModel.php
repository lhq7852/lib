<?php

/**
 * 模型
 */

namespace chb_union_center\identity_config\model;

use chb_lib\common\BaseModel;

class IdentityFlowConfigModel extends BaseModel
{

    protected $name = 'identity_flow_config';
    protected $pk = 'identity_flow_config_id';

    protected $likeList = ["keyword" => "is_real"]; //设置模糊搜索映射的字段 alias|value

}
