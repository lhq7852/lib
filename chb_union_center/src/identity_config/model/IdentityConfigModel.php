<?php

/**
 * 模型
 */

namespace chb_union_center\identity_config\model;

use chb_lib\common\BaseModel;
use think\Model;

class IdentityConfigModel extends BaseModel
{

    protected $name = 'identity_config';
    protected $pk = 'identity_config_id';

    protected $hidden = ['delete_time'];
    protected $likeList = ["keyword" => "identity_name"]; //设置模糊搜索映射的字段 alias|value

}
