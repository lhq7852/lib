<?php

/**
 * 模型
 */

namespace chb_union_center\identity_config\model;

use chb_lib\common\BaseModel;
use think\Model;

class IdentityMemberModel extends BaseModel
{

    protected $name = 'identity_member';
    protected $pk = 'identity_member_id';

    protected $hidden = ['delete_time'];
    protected $likeList = ["keyword" => "member_name"]; //设置模糊搜索映射的字段 alias|value

}
