<?php

/**
 * 系统配置参数检验
 */

namespace chb_union_center\identity_config\validate;

use chb_lib\common\ValidateTrait;
use think\Validate;

class IdentityClientValidate  extends Validate
{
    use ValidateTrait;

    protected $rule = [
        'identity_type' => 'require|max:200',
        'client_pk' => 'require',
    ];
}
