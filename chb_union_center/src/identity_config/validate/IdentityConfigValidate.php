<?php

/**
 * 系统配置参数检验
 */

namespace chb_union_center\identity_config\validate;

use chb_lib\common\ValidateTrait;
use think\Validate;

class IdentityConfigValidate  extends Validate
{
    use ValidateTrait;

    protected $being = "identity_pk"; //唯一字段检查

    protected $rule = [
        'identity_pk' => 'require|max:200|checkBeing',
        'identity_name' => 'require|max:200',
        'remark' => 'max:5000',
    ];
}
