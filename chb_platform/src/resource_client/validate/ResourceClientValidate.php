<?php

/**
 * 验证
 */

namespace chb_platform\resource_client\validate;

use chb_lib\common\ValidateTrait;
use think\Validate;

class ResourceClientValidate extends Validate
{
    use ValidateTrait;
    protected $being = "client_alias|app_id"; //唯一字段检查

    protected $rule = [
        'client_name' => 'require|max:200',
        'sort' => 'number',
        'app_id' => 'checkBeing',
    ];
}
