<?php

namespace chb_platform\resource_client;

use chb_lib\common\RedisToolTrait;
use chb_lib\common\ServiceTrait;

class ResourceClient
{

    use RedisToolTrait, ServiceTrait;

    /**
     * 设置入库的参数,可重写
     *
     * @param [type] $param
     * @return object
     */
    protected function setFieldParam(&$param = [], $mode = "add")
    {
        if ($mode == 'add') {
            $param['app_id'] = $this->getNonceStr(12);
        }
        return $this;
    }

    /**
     * 检查app_id是否有效
     *
     * @param array $param
     * @return void|array
     */
    public function getAppIdByClientPk($param = [])
    {
        $info = $this->getInfo(['client_pk' => $param['client_pk'], 'client_user_no' => $param['client_owner'] ?? ''], false);
        return [
            'app_id' => $info['app_id'] ?? '',
            'client_user_no' => $info['client_user_no'] ?? '',
        ];
    }

    /**
     * 获取全部应用标识
     *
     * @param [type] $url
     * @param [type] $param
     * @return void
     */
    public function getAllClientPk($param = [])
    {
        $list = $this->getDataList();
        if (empty($list)) {
            return "";
        }
        return implode(",", array_column($list, 'client_pk'));
    }

}
