<?php

/**
 * 平台
 */

namespace chb_platform\resource_client\model;

use chb_lib\common\BaseModel;

class ResourceClientModel extends BaseModel
{

    protected $name = 'resource_client';
    protected $pk = 'client_id';

    protected $likeList = ["keyword" => "client_name"]; //设置模糊搜索映射的字段 alias|value

}
